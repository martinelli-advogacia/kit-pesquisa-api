CREATE TABLE solicitacao_pesquisa (
    ID bigint NOT NULL AUTO_INCREMENT,
    SOLICITANTE varchar(255),
    DATA_CRIACAO datetime,
    DATA_ATUALIZACAO datetime,
	PESQUISADOR_RESPONSAVEL varchar(255),
	OBSERVACAO varchar(1000),
	URGENTE boolean DEFAULT FALSE,
	CONCLUIDA boolean DEFAULT FALSE,
	EMPRESA_ID bigint NOT NULL,
    PRIMARY KEY(ID),
    CONSTRAINT FK_EMPRESA_CNPJ_SOLICITACAO FOREIGN KEY (EMPRESA_ID) REFERENCES  empresa_cnpj (id)
	);

CREATE TABLE empresa_cnpj (
    ID bigint NOT NULL AUTO_INCREMENT,
    CNPJ varchar(255),
    NOME_EMPRESA varchar(255),
    UF varchar(2),
    PRIMARY KEY(ID)
);

CREATE TABLE arquivo (
	id bigint NOT NULL AUTO_INCREMENT,
	nome_arquivo boolean,
	EMPRESA_ID bigint,
	anexo blob,
	DATA_ATUALIZACAO datetime,
	ATUALIZADO_POR varchar(255),
	PRIMARY KEY(ID),
	CONSTRAINT FK_EMPRESA_CNPJ_ARQUIVO FOREIGN KEY (EMPRESA_ID) REFERENCES  empresa_cnpj (id)
);


CREATE TABLE item_pesquisa (
    id bigint,
    sem_resultado boolean DEFAULT FALSE,
    concluido boolean DEFAULT FALSE,
    ARQUIVO_ID bigint,
    PESQUISA_ID bigint,
    DATA_ATUALIZACAO datetime,
    ATUALIZADO_POR varchar(255),
    PRIMARY KEY(ID),
    CONSTRAINT FK_SOLICITACAO_PESQUISA_ITEM FOREIGN KEY (PESQUISA_ID) REFERENCES  solicitacao_pesquisa (id),
    CONSTRAINT FK_ARQUIVO_ITEM FOREIGN KEY (ARQUIVO_ID) REFERENCES arquivo (id)
);