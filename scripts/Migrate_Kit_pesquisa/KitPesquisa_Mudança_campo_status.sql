UPDATE KIT_PESQUISA.dbo.solicitacao_pesquisa
SET status='Aberto'
WHERE concluida=0 AND pesquisador_responsavel IS null;

UPDATE KIT_PESQUISA.dbo.solicitacao_pesquisa
SET status='Em Atendimento'
WHERE concluida=0 AND pesquisador_responsavel IS NOT null;

UPDATE KIT_PESQUISA.dbo.solicitacao_pesquisa
SET status='Concluida'
WHERE concluida=1;

UPDATE KIT_PESQUISA.dbo.item_pesquisa
SET concluido=1
WHERE pesquisa_id IN (SELECT id FROM KIT_PESQUISA.dbo.solicitacao_pesquisa WHERE concluida=1)
AND arquivo_id IS not null;

UPDATE KIT_PESQUISA.dbo.item_pesquisa
SET concluido=1, sem_resultado = 1
WHERE pesquisa_id IN (SELECT id FROM KIT_PESQUISA.dbo.solicitacao_pesquisa WHERE concluida=1)
AND arquivo_id IS null;