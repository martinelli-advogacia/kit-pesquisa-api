-- DROP SCHEMA dbo;

CREATE SCHEMA dbo;
-- KIT_PESQUISA_HOMOL.dbo.empresa_cnpj definition

-- Drop table

-- DROP TABLE KIT_PESQUISA_HOMOL.dbo.empresa_cnpj GO

CREATE TABLE KIT_PESQUISA_HOMOL.dbo.empresa_cnpj (
	id bigint IDENTITY(1,1) NOT NULL,
	cnpj varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	nome_empresa varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	uf varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK__empresa___3213E83F46A79DC3 PRIMARY KEY (id)
) GO;


-- KIT_PESQUISA_HOMOL.dbo.tipo_item_pesquisa definition

-- Drop table

-- DROP TABLE KIT_PESQUISA_HOMOL.dbo.tipo_item_pesquisa GO

CREATE TABLE KIT_PESQUISA_HOMOL.dbo.tipo_item_pesquisa (
	id bigint IDENTITY(1,1) NOT NULL,
	titulo varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	url varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK__tipo_ite__3213E83F188F4E08 PRIMARY KEY (id)
) GO;


-- KIT_PESQUISA_HOMOL.dbo.usuario definition

-- Drop table

-- DROP TABLE KIT_PESQUISA_HOMOL.dbo.usuario GO

CREATE TABLE KIT_PESQUISA_HOMOL.dbo.usuario (
	login varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	email varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	nome varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	telefone varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK__usuario__7838F27354616539 PRIMARY KEY (login)
) GO;


-- KIT_PESQUISA_HOMOL.dbo.arquivo definition

-- Drop table

-- DROP TABLE KIT_PESQUISA_HOMOL.dbo.arquivo GO

CREATE TABLE KIT_PESQUISA_HOMOL.dbo.arquivo (
	id bigint IDENTITY(1,1) NOT NULL,
	anexo varbinary(MAX) NOT NULL,
	data_atualizacao datetime2(7) NULL,
	nome_arquivo varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	tipo_conteudo varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	empresa_id bigint NOT NULL,
	atualizado_por varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK__arquivo__3213E83F9483753C PRIMARY KEY (id),
	CONSTRAINT FK_ARQUIVO_USUARIO FOREIGN KEY (atualizado_por) REFERENCES KIT_PESQUISA_HOMOL.dbo.usuario(login),
	CONSTRAINT FK_EMPRESA_CNPJ_ARQUIVO FOREIGN KEY (empresa_id) REFERENCES KIT_PESQUISA_HOMOL.dbo.empresa_cnpj(id)
) GO;


-- KIT_PESQUISA_HOMOL.dbo.permissionamento definition

-- Drop table

-- DROP TABLE KIT_PESQUISA_HOMOL.dbo.permissionamento GO

CREATE TABLE KIT_PESQUISA_HOMOL.dbo.permissionamento (
	id bigint IDENTITY(1,1) NOT NULL,
	role_alowed varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	login varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT INDEX_LOGIN UNIQUE (login),
	CONSTRAINT PK__permissi__3213E83F49E9CC4C PRIMARY KEY (id),
	CONSTRAINT FK_PERMIS_USUARIO FOREIGN KEY (login) REFERENCES KIT_PESQUISA_HOMOL.dbo.usuario(login)
) GO
CREATE UNIQUE NONCLUSTERED INDEX INDEX_LOGIN ON KIT_PESQUISA_HOMOL.dbo.permissionamento (login) GO;


-- KIT_PESQUISA_HOMOL.dbo.solicitacao_pesquisa definition

-- Drop table

-- DROP TABLE KIT_PESQUISA_HOMOL.dbo.solicitacao_pesquisa GO

CREATE TABLE KIT_PESQUISA_HOMOL.dbo.solicitacao_pesquisa (
	id bigint IDENTITY(1,1) NOT NULL,
	concluida bit NOT NULL,
	data_atualizacao datetime2(7) NULL,
	data_criacao datetime2(7) NULL,
	observacao varchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	urgente bit NOT NULL,
	empresa_id bigint NULL,
	pesquisador_responsavel varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	solicitante varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	CONSTRAINT PK__solicita__3213E83FDF1A6768 PRIMARY KEY (id),
	CONSTRAINT FK_EMPRESA_CNPJ_SOLICITACAO FOREIGN KEY (empresa_id) REFERENCES KIT_PESQUISA_HOMOL.dbo.empresa_cnpj(id),
	CONSTRAINT FK_PESQ_RESPONSAVEL FOREIGN KEY (pesquisador_responsavel) REFERENCES KIT_PESQUISA_HOMOL.dbo.usuario(login),
	CONSTRAINT FK_PESQ_SOLICITANTE FOREIGN KEY (solicitante) REFERENCES KIT_PESQUISA_HOMOL.dbo.usuario(login)
) GO;


-- KIT_PESQUISA_HOMOL.dbo.item_pesquisa definition

-- Drop table

-- DROP TABLE KIT_PESQUISA_HOMOL.dbo.item_pesquisa GO

CREATE TABLE KIT_PESQUISA_HOMOL.dbo.item_pesquisa (
	id bigint IDENTITY(1,1) NOT NULL,
	concluido bit NOT NULL,
	data_atualizacao datetime2(7) NULL,
	sem_resultado bit NULL,
	arquivo_id bigint NULL,
	pesquisa_id bigint NOT NULL,
	tipo_item_id bigint NULL,
	atualizado_por varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK__item_pes__3213E83F6ABC6A03 PRIMARY KEY (id),
	CONSTRAINT FK_ARQUIVO_ITEM FOREIGN KEY (arquivo_id) REFERENCES KIT_PESQUISA_HOMOL.dbo.arquivo(id),
	CONSTRAINT FK_ITEM_USUARIO FOREIGN KEY (atualizado_por) REFERENCES KIT_PESQUISA_HOMOL.dbo.usuario(login),
	CONSTRAINT FK_SOLICITACAO_PESQUISA_ITEM FOREIGN KEY (pesquisa_id) REFERENCES KIT_PESQUISA_HOMOL.dbo.solicitacao_pesquisa(id),
	CONSTRAINT FK_TIPO_ITEM FOREIGN KEY (tipo_item_id) REFERENCES KIT_PESQUISA_HOMOL.dbo.tipo_item_pesquisa(id)
) GO;;
