package com.martinelli.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapContext;

import com.martinelli.Application;
import com.martinelli.dto.UsuarioAutenticadoDTO;
import com.martinelli.utils.exceptions.ldap.BadArgumentException;

public class LDAPFindUser {

	static final String ARGUMENTO_NULO = "Argumento nulo";
	static final String ARGUMENTO_VAZIO = "Argumento vazio";

	public static void main(String[] args) throws NamingException, BadArgumentException, IOException {
		
		LdapContext ctx = LDAPConnector.openConnection(Application.getLdapDomainProperty() + "\\doliveira",
				"H&pt@2020", Application.getLdapUrlProperty());
		File myObj = new File("D:\\usuarios_kit.txt");
		FileWriter myWriter = new FileWriter("D:\\lista_login.txt");
	      Scanner myReader = new Scanner(myObj);
	      UsuarioAutenticadoDTO usuario = null;
	      while (myReader.hasNextLine()) {
	        String data = myReader.nextLine();
	        usuario = LDAPFindUser.getUserByName(ctx, data, Application.getLdapSearchBaseProperty());
	        if(usuario != null && usuario.getLogin() != null)
	        	myWriter.write(usuario.toString() +"\n");
	        else
	        	myWriter.write(data +"\n");
	        System.out.println(usuario != null?usuario.toString(): data);
	      }
	      myReader.close();
	      myWriter.close();
		
	}

	public static UsuarioAutenticadoDTO getUserByName(LdapContext ctx, String accountName, String ldapSearchBase)
			throws NamingException, BadArgumentException {

		UsuarioAutenticadoDTO funcionario = new UsuarioAutenticadoDTO();
		String searchFilter = "(&(objectClass=user)(displayName=" + accountName + "))";
		NamingEnumeration<SearchResult> results = busca(ctx, ldapSearchBase, null, searchFilter);

		SearchResult searchResult = null;
		if (results.hasMoreElements()) {
			searchResult = results.nextElement();

			Attributes attributes = searchResult.getAttributes();
			funcionario.setLogin(safeAttribute(attributes, "sAMAccountName"))
					.setNome(safeAttribute(attributes, "displayName")).setEmail(safeAttribute(attributes, "mail"))
					.setTelefone(safeAttribute(attributes, "mobile"));

			if (results.hasMoreElements()) {
				return null;
			}
		}

		return funcionario;
	}

	private static String safeAttribute(Attributes attr, String atributo) throws NamingException {
		if (attr.get(atributo) != null)
			return attr.get(atributo).get().toString();
		else
			return "";
	}

	public static NamingEnumeration<SearchResult> busca(LdapContext ctx, String ldapSearchBase,
			String[] returningAttributes, String searchFilter) throws NamingException, BadArgumentException {
		if (ctx == null || searchFilter == null || ldapSearchBase == null)
			throw new BadArgumentException(ARGUMENTO_NULO);
		if (searchFilter.isEmpty() || ldapSearchBase.isEmpty())
			throw new BadArgumentException(ARGUMENTO_VAZIO);

		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		if (returningAttributes != null && returningAttributes.length > 0)
			searchControls.setReturningAttributes(returningAttributes);
		return ctx.search(ldapSearchBase, searchFilter, searchControls);
	}

}
