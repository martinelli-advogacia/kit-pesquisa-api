package com.martinelli.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.martinelli.dto.EmpresaCNPJDTO;

@Entity
@Table(name = "empresa_cnpj")
@NamedQuery(name = "NQ_EMPRESA_POR_NOME_CNPJ", query = "SELECT e FROM EmpresaCNPJ e "
		+ "WHERE e.cnpj LIKE CONCAT('%', :busca,'%') "
		+ "OR UPPER(e.nomeEmpresa) LIKE CONCAT('%', :busca,'%') ")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmpresaCNPJ {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	long id;

	@NotNull
	@Column(name = "cnpj", unique=true)
	String cnpj;

	@Column(name = "nome_empresa")
	String nomeEmpresa;

	String uf;

	public EmpresaCNPJ() {
		super();
	}

	public EmpresaCNPJ(long id, @NotNull String cnpj, String nomeEmpresa, String uf) {
		super();
		this.id = id;
		this.cnpj = cnpj;
		this.nomeEmpresa = nomeEmpresa;
		this.uf = uf;
	}

	public EmpresaCNPJ(EmpresaCNPJDTO empresa) {
		super();
		this.id = empresa.getId();
		this.cnpj = empresa.getCnpj();
		this.nomeEmpresa = empresa.getNomeEmpresa();
		this.uf = empresa.getUf();
	}

	public long getId() {
		return id;
	}

	public EmpresaCNPJ setId(long id) {
		this.id = id;
		return this;
	}

	public String getCnpj() {
		return cnpj;
	}

	public EmpresaCNPJ setCnpj(String cnpj) {
		this.cnpj = cnpj;
		return this;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public EmpresaCNPJ setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
		return this;
	}

	public String getUf() {
		return uf;
	}

	public EmpresaCNPJ setUf(String uf) {
		this.uf = uf;
		return this;
	}

	@Override
	public String toString() {
		return "EmpresaCNPJ{"
				+ "id="
				+ id
				+ ", cnpj='"
				+ cnpj
				+ '\''
				+ ", nomeEmpresa='"
				+ nomeEmpresa
				+ '\''
				+ ", uf='"
				+ uf
				+ '\''
				+ '}';
	}
}
