package com.martinelli.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "solicitacao_pesquisa")
@NamedQuery(name = "NQ_BUSCA_PESQUISAS", query = "SELECT new com.martinelli.dto.SolicitacaoPesquisaConsultaDTO(sp) "
		+ "FROM SolicitacaoPesquisa sp ")
@NamedQuery(name = "NQ_FIND_PESQUISA_BY_ID", query = "SELECT sp FROM SolicitacaoPesquisa sp "
		+ "JOIN FETCH sp.empresa "
		+ "JOIN FETCH sp.itens "
		+ "WHERE sp.id = :id")
@NamedQuery(name = "NQ_BUSCA_PESQUISAS_NAO_CONCLUIDAS", query = "SELECT new com.martinelli.dto.SolicitacaoPesquisaConsultaDTO(sp) "
		+ "FROM SolicitacaoPesquisa sp "
		+ "WHERE sp.concluida = false "
		+ "ORDER BY sp.dataCriacao DESC")
@NamedQuery(name = "NQ_DELETAR_SOLICITACAO", query = "DELETE FROM SolicitacaoPesquisa sp WHERE sp.id = :id")
@NamedQuery(name = "NQ_BUSCA_PESQUISAS_CONCLUIDAS", query = "SELECT sp FROM SolicitacaoPesquisa sp "
		+ "JOIN FETCH sp.solicitante "
		+ "WHERE sp.concluida = true ")

public class SolicitacaoPesquisa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "solicitante", foreignKey = @ForeignKey(name = "FK_PESQ_SOLICITANTE"))
	Usuario solicitante;

	@ManyToOne
	@JoinColumn(name = "pesquisador_responsavel", foreignKey = @ForeignKey(name = "FK_PESQ_RESPONSAVEL"))
	Usuario pesquisadorResponsavel;

	@Length(max = 1000, message = "Tamanho máximo da observação é de 1000 caracteres")
	String observacao;

	boolean urgente;

	boolean concluida;

	@Column(name = "data_criacao")
	LocalDateTime dataCriacao;

	@Column(name = "data_atualizacao")
	LocalDateTime dataAtualizacao;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.REMOVE })
	@JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "FK_EMPRESA_CNPJ_SOLICITACAO"))
	EmpresaCNPJ empresa;

	@Column(name = "status")
	String status;

	@OneToMany(mappedBy = "solicitacaoPesquisa", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST,
			CascadeType.DETACH, CascadeType.REMOVE })
	List<ItemPesquisa> itens;

	public long getId() {
		return id;
	}

	public SolicitacaoPesquisa setId(long id) {
		this.id = id;
		return this;
	}

	public Usuario getSolicitante() {
		return solicitante;
	}

	public SolicitacaoPesquisa setSolicitante(@NotNull Usuario solicitante) {
		this.solicitante = solicitante;
		return this;
	}

	public Usuario getPesquisadorResponsavel() {
		return pesquisadorResponsavel;
	}

	public SolicitacaoPesquisa setPesquisadorResponsavel(Usuario pesquisadorResponsavel) {
		this.pesquisadorResponsavel = pesquisadorResponsavel;
		return this;
	}

	public String getObservacao() {
		return observacao;
	}

	public SolicitacaoPesquisa setObservacao(String observacao) {
		this.observacao = observacao;
		return this;
	}

	public boolean getUrgente() {
		return urgente;
	}

	public SolicitacaoPesquisa setUrgente(boolean urgente) {
		this.urgente = urgente;
		return this;
	}

	public boolean getConcluida() {
		return concluida;
	}

	public SolicitacaoPesquisa setConcluida(boolean concluida) {
		this.concluida = concluida;
		return this;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public SolicitacaoPesquisa setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
		return this;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public SolicitacaoPesquisa setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
		return this;
	}

	public EmpresaCNPJ getEmpresa() {
		return empresa;
	}

	public SolicitacaoPesquisa setEmpresa(EmpresaCNPJ empresa) {
		this.empresa = empresa;
		return this;
	}

	public List<ItemPesquisa> getItens() {
		return itens;
	}

	public SolicitacaoPesquisa setItens(List<ItemPesquisa> itens) {
		this.itens = itens;
		return this;
	}

	public String getStatus() {
		return status;
	}

	public SolicitacaoPesquisa setStatus(String status) {
		this.status = status;
		return this;
	}

	@Override
	public String toString() {
		return "SolicitacaoPesquisa{"
				+ "id="
				+ id
				+ ", solicitante='"
				+ solicitante
				+ '\''
				+ ", pesquisadorResponsavel='"
				+ pesquisadorResponsavel
				+ '\''
				+ ", observacao='"
				+ observacao
				+ '\''
				+ ", urgente="
				+ urgente
				+ ", concluida="
				+ concluida
				+ ", dataCriacao="
				+ dataCriacao
				+ ", dataAtualizacao="
				+ dataAtualizacao
				+ ", empresa="
				+ empresa
				+ '}';
	}
}
