package com.martinelli.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "arquivo")
@NamedQuery(name = "NQ_BUSCAR_POR_SOLICITACAO", query = "SELECT distinct a FROM Arquivo a "
		+ " INNER JOIN FETCH a.itemPesquisa ip "
		+ " INNER JOIN ip.solicitacaoPesquisa sp "
		+ " WHERE sp.id = :idSolicitacao")
public class Arquivo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;

	@Column(name = "nome_arquivo")
	String nomeArquivo;

	@Lob
	@Column(name = "anexo", columnDefinition = "varbinary(max)")
	@NotNull
	@JsonIgnore
	byte[] anexo;

	@Column(name = "tipo_conteudo")
	String tipoConteudo;

	@Column(name = "data_atualizacao")
	LocalDateTime dataAtualizacao;

	@ManyToOne
	@JoinColumn(name = "atualizado_por", foreignKey = @ForeignKey(name = "FK_ARQUIVO_USUARIO"))
	Usuario ultimoAtualizador;

	@OneToMany(mappedBy = "arquivo", fetch = FetchType.LAZY)
	List<ItemPesquisa> itemPesquisa;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "FK_EMPRESA_CNPJ_ARQUIVO"))
	@NotNull
	EmpresaCNPJ empresaCNPJ;

	public Arquivo() {

	}

	public Arquivo(Arquivo arquivo) {
		this.id = arquivo.getId();
		this.nomeArquivo = arquivo.getNomeArquivo();
		this.anexo = arquivo.getAnexo();
		this.tipoConteudo = arquivo.getTipoConteudo();
		this.dataAtualizacao = arquivo.getDataAtualizacao();
		this.ultimoAtualizador = arquivo.getUltimoAtualizador();
		this.itemPesquisa = arquivo.getItemPesquisa();
		this.empresaCNPJ = arquivo.getEmpresaCNPJ();
	}

	public Arquivo(@NotNull long id, String nomeArquivo, @NotNull byte[] anexo, LocalDateTime dataAtualizacao,
			Usuario ultimoAtualizador, List<ItemPesquisa> itemPesquisa, @NotNull EmpresaCNPJ empresaCNPJ) {
		this.id = id;
		this.nomeArquivo = nomeArquivo;
		this.anexo = anexo;
		this.dataAtualizacao = dataAtualizacao;
		this.ultimoAtualizador = ultimoAtualizador;
		this.itemPesquisa = itemPesquisa;
		this.empresaCNPJ = empresaCNPJ;
	}

	public String getTipoConteudo() {
		return tipoConteudo;
	}

	public Arquivo setTipoConteudo(String tipoConteudo) {
		this.tipoConteudo = tipoConteudo;
		return this;
	}

	public long getId() {
		return id;
	}

	public Arquivo setId(long id) {
		this.id = id;
		return this;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public Arquivo setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
		return this;
	}

	public byte[] getAnexo() {
		return anexo;
	}

	public Arquivo setAnexo(byte[] anexo) {
		this.anexo = anexo;
		return this;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public Arquivo setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
		return this;
	}

	public Usuario getUltimoAtualizador() {
		return ultimoAtualizador;
	}

	public Arquivo setUltimoAtualizador(Usuario ultimoAtualizador) {
		this.ultimoAtualizador = ultimoAtualizador;
		return this;
	}

	public List<ItemPesquisa> getItemPesquisa() {
		return itemPesquisa;
	}

	public Arquivo setItemPesquisa(List<ItemPesquisa> itemPesquisa) {
		this.itemPesquisa = itemPesquisa;
		return this;
	}

	public EmpresaCNPJ getEmpresaCNPJ() {
		return empresaCNPJ;
	}

	public Arquivo setEmpresaCNPJ(EmpresaCNPJ empresaCNPJ) {
		this.empresaCNPJ = empresaCNPJ;
		return this;
	}

	@Override
	public String toString() {
		return "Arquivo{" + "id=" + id + ", nomeArquivo='" + nomeArquivo + '\'' +
		// ", anexo=" + Arrays.toString(anexo) +
				", dataAtualizacao="
				+ dataAtualizacao
				+ ", ultimoAtualizador='"
				+ ultimoAtualizador
				+ '\''
				+ ", itemPesquisa="
				+ itemPesquisa
				+ ", empresaCNPJ="
				+ empresaCNPJ
				+ '}';
	}
}
