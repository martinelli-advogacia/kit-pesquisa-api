package com.martinelli.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.martinelli.dto.TipoItemPesquisaDTO;

import java.util.Objects;

@Entity
@Table(name = "tipo_item_pesquisa")
@NamedQuery(name = "NQ_BUSCAR_SEM_ARQUIVO", query = "SELECT t FROM SolicitacaoPesquisa sp "
		+ " LEFT JOIN sp.itens i "
		+ " LEFT JOIN i.tipo t "
		+ " WHERE sp.id = :idSolicitacao "
		+ " AND i.semResultado = TRUE")
@JsonInclude(Include.NON_NULL)
public class TipoItemPesquisa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;

	String titulo;

	String url;

	String descricao;

	public TipoItemPesquisa() {
		super();
	}

	public TipoItemPesquisa(@NotNull long id, String titulo, String url, String descricao) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.url = url;
		this.descricao = descricao;
	}

	public TipoItemPesquisa(TipoItemPesquisaDTO tipo) {
		super();
		this.id = tipo.getId();
		this.titulo = tipo.getTitulo();
		this.url = tipo.getUrl();
		this.descricao = tipo.getDescricao();
	}

	public long getId() {
		return id;
	}

	public TipoItemPesquisa setId(long id) {
		this.id = id;
		return this;
	}

	public String getTitulo() {
		return titulo;
	}

	public TipoItemPesquisa setTitulo(String titulo) {
		this.titulo = titulo;
		return this;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "TipoItemPesquisa{" + "id=" + id + ", titulo='" + titulo + '\'' + '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		TipoItemPesquisa that = (TipoItemPesquisa) o;
		return Objects.equals(id, that.id) && Objects.equals(titulo, that.titulo);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, titulo);
	}

	public String getDescricao() {
		return descricao;
	}

	public TipoItemPesquisa setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}
}
