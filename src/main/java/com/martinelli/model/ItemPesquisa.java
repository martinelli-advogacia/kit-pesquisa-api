package com.martinelli.model;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.martinelli.dto.TipoItemPesquisaDTO;

//TODO adicionar hibernate envers a todas as tabelas
@Entity
@Table(name = "item_pesquisa")
@NamedQuery(name = "NQ_DELETE_ITEM_PELA_SOLICITACAO", query="DELETE FROM ItemPesquisa ip WHERE ip.solicitacaoPesquisa.id = :idSolicitacao")
public class ItemPesquisa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;

	@OneToOne
	@JoinColumn(name = "tipo_item_id", foreignKey = @ForeignKey(name = "FK_TIPO_ITEM"))
	TipoItemPesquisa tipo;

	@Column(name = "sem_resultado")
	boolean semResultado;

	boolean concluido;

	@Column(name = "data_atualizacao")
	LocalDateTime dataAtualizacao;

	@ManyToOne
	@JoinColumn(name = "atualizado_por", foreignKey = @ForeignKey(name = "FK_ITEM_USUARIO"))
	Usuario ultimoAtualizador;

	@ManyToOne
	@JoinColumn(name = "arquivo_id", foreignKey = @ForeignKey(name = "FK_ARQUIVO_ITEM"))
	Arquivo arquivo;

	@ManyToOne
	@JoinColumn(name = "pesquisa_id", foreignKey = @ForeignKey(name = "FK_SOLICITACAO_PESQUISA_ITEM"))
	@NotNull
	@JsonBackReference
	SolicitacaoPesquisa solicitacaoPesquisa;

	public ItemPesquisa() {
		super();
	}

	public ItemPesquisa(TipoItemPesquisaDTO tipo, boolean semResultado, boolean concluido,
			LocalDateTime dataAtualizacao, Usuario ultimoAtualizador, Arquivo arquivo) {
		if (tipo != null)
			this.tipo = new TipoItemPesquisa(tipo);
		this.semResultado = semResultado;
		this.concluido = concluido;
		this.dataAtualizacao = dataAtualizacao;
		this.ultimoAtualizador = ultimoAtualizador;
		this.arquivo = arquivo;
	}

	public long getId() {
		return id;
	}

	public ItemPesquisa setId(long id) {
		this.id = id;
		return this;
	}

	public TipoItemPesquisa getTipo() {
		return tipo;
	}

	public ItemPesquisa setTipo(TipoItemPesquisa tipo) {
		this.tipo = tipo;
		return this;
	}

	public boolean getSemResultado() {
		return semResultado;
	}

	public ItemPesquisa setSemResultado(boolean semResultado) {
		this.semResultado = semResultado;
		return this;
	}

	public boolean getConcluido() {
		return concluido;
	}

	public ItemPesquisa setConcluido(boolean concluido) {
		this.concluido = concluido;
		return this;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public ItemPesquisa setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
		return this;
	}

	public Usuario getUltimoAtualizador() {
		return ultimoAtualizador;
	}

	public ItemPesquisa setUltimoAtualizador(Usuario ultimoAtualizador) {
		this.ultimoAtualizador = ultimoAtualizador;
		return this;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public ItemPesquisa setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
		return this;
	}

	public SolicitacaoPesquisa getSolicitacaoPesquisa() {
		return solicitacaoPesquisa;
	}

	public ItemPesquisa setSolicitacaoPesquisa(SolicitacaoPesquisa solicitacaoPesquisa) {
		this.solicitacaoPesquisa = solicitacaoPesquisa;
		return this;
	}

	@Override
	public String toString() {
		return "ItemPesquisa{"
				+ "id="
				+ id
				+ ", tipo="
				+ tipo
				+ ", semResultado="
				+ semResultado
				+ ", concluido="
				+ concluido
				+ ", dataAtualizacao="
				+ dataAtualizacao
				+ ", ultimoAtualizador='"
				+ ultimoAtualizador
				+ '\''
				+ ", arquivo="
				+ arquivo
				+ ", solicitacaoPesquisa="
				+ solicitacaoPesquisa
				+ '}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ItemPesquisa that = (ItemPesquisa) o;
		return id == that.id && Objects.equals(tipo, that.tipo) && Objects.equals(semResultado, that.semResultado)
				&& Objects.equals(concluido, that.concluido) && Objects.equals(dataAtualizacao, that.dataAtualizacao)
				&& Objects.equals(ultimoAtualizador, that.ultimoAtualizador) && Objects.equals(arquivo, that.arquivo)
				&& Objects.equals(solicitacaoPesquisa, that.solicitacaoPesquisa);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, tipo, semResultado, concluido, dataAtualizacao, ultimoAtualizador, arquivo,
				solicitacaoPesquisa);
	}
}
