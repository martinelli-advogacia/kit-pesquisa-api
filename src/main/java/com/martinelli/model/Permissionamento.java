package com.martinelli.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "permissionamento", uniqueConstraints = @UniqueConstraint(columnNames = "login", name = "INDEX_LOGIN"))
@NamedQuery(name = "NQ_BUSCAR_POR_LOGIN", query = "SELECT p FROM Permissionamento p LEFT JOIN p.login l WHERE l.login = :login")
public class Permissionamento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "login", foreignKey = @ForeignKey(name = "FK_PERMIS_USUARIO"))
	Usuario login;

	@Column(name = "role_alowed")
	@NotNull
	String roleAlowed;

	@Column(name = "selecionar_itens")
	boolean selecionarItens = false;

	public long getId() {
		return id;
	}

	public Permissionamento setId(long id) {
		this.id = id;
		return this;
	}

	public @NotNull Usuario getLogin() {
		return login;
	}

	public Permissionamento setLogin(@NotNull Usuario login) {
		this.login = login;
		return this;
	}

	public String getRoleAlowed() {
		return roleAlowed;
	}

	public Permissionamento setRoleAlowed(String roleAlowed) {
		this.roleAlowed = roleAlowed;
		return this;
	}

	public boolean getSelecionarItens() {
		return selecionarItens;
	}

	public Permissionamento setSelecionarItens(boolean selecionarItens) {
		this.selecionarItens = selecionarItens;
		return this;
	}

}
