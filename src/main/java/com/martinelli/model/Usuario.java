package com.martinelli.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.martinelli.dto.UsuarioDTO;

@Entity
@Table(name = "usuario")
@NamedQuery(name = "NQ_USUARIO_POR_LOGIN", query = "SELECT u FROM Usuario u WHERE u.login = :login")
@NamedQuery(name = "NQ_USUARIO_POR_LOGIN_PERFIL", query = "SELECT new com.martinelli.dto.UsuarioDTO( u, p ) "
		+ "FROM Usuario u "
		+ "LEFT JOIN Permissionamento p ON p.login = u.login "
		+ "WHERE u.login = :login")
@NamedQuery(name = "NQ_USUARIO_PERMISSAO", query = "SELECT new com.martinelli.dto.UsuarioDTO( u, p ) "
		+ "FROM Usuario u "
		+ "LEFT JOIN Permissionamento p ON p.login = u.login")
@NamedQuery(name = "NQ_USUARIO_BUSCA_SOLICITANTE", query = "SELECT DISTINCT new com.martinelli.dto.UsuarioDTO(u) "
		+ "FROM Usuario u "
		+ "INNER JOIN SolicitacaoPesquisa sp ON u.login = sp.solicitante.login "
		+ "WHERE UPPER(u.login) LIKE UPPER(CONCAT('%',:busca,'%')) "
		+ "OR UPPER(u.nome) LIKE UPPER(CONCAT('%',:busca,'%'))")
@NamedQuery(name = "NQ_USUARIO_BUSCA_ATENDENTE", query = "SELECT DISTINCT new com.martinelli.dto.UsuarioDTO(u) "
		+ "FROM Usuario u "
		+ "INNER JOIN SolicitacaoPesquisa sp ON u.login = sp.pesquisadorResponsavel.login ")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Usuario {

	@Id
	String login;

	String nome;

	String email;

	String telefone;

	String uf;

	LocalDateTime ultimoAcesso;

	boolean vip = false;

	public Usuario() {
		super();
	}

	public Usuario(String login, String nome, String email, String telefone, String uf, LocalDateTime ultimoacesso) {
		super();
		this.login = login;
		this.nome = nome;
		this.email = email;
		this.telefone = telefone;
		this.uf = uf;
		this.ultimoAcesso = ultimoacesso;
	}

	public Usuario(UsuarioDTO atualizador) {
		super();
		this.login = atualizador.getLogin();
		this.nome = atualizador.getNome();
		this.email = atualizador.getEmail();
		this.telefone = atualizador.getTelefone();
		this.uf = atualizador.getUf();
		this.ultimoAcesso = atualizador.getUltimoAcesso();
		this.vip = atualizador.isVip();
	}

	public String getLogin() {
		return login;
	}

	public Usuario setLogin(String login) {
		this.login = login;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public Usuario setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Usuario setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public Usuario setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public String getUf() {
		return uf;
	}

	public Usuario setUf(String uf) {
		this.uf = uf;
		return this;
	}

	public LocalDateTime getUltimoAcesso() {
		return ultimoAcesso;
	}

	public Usuario setUltimoAcesso(LocalDateTime ultimoAcesso) {
		this.ultimoAcesso = ultimoAcesso;
		return this;
	}

	public boolean isVip() {
		return vip;
	}

	public Usuario setVip(boolean vip) {
		this.vip = vip;
		return this;
	}

}
