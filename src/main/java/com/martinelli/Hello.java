package com.martinelli;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class Hello extends HttpServlet {


    /**
	 * 
	 */
	static final long serialVersionUID = -5382990706107125157L;

	/**
     * Respond to a GET request for the content produced by
     * this servlet.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are producing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
      throws IOException, ServletException {

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();        
        writer.println("<html>");
        writer.println("<head>");
        writer.println("<title>HELLO KIT PESQUISA</title>");
        writer.println("</head>");
        writer.println("<body bgcolor=greenyellow align=center valign=middle>");

        writer.println("<h1>HELLO KIT PESQUISA</h1>");

        writer.println("Pagina de teste da aplicação");
        writer.println("HELLO WORLD!!");

        writer.println("</body>");
        writer.println("</html>");
    }
}
