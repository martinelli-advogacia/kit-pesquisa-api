package com.martinelli;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Application {
	
	static Logger logger = Logger.getLogger(Application.class.getName());

    // HashMap contendo as propriedades definidas
    public static final Properties mapProperties = loadProperties();

    /**
     * ----------------------------------------
     * Obtem um mapa das propriedades definidas
     * ----------------------------------------
     */
    public static Properties loadProperties() {
        try {
            InputStream inputStream = Application.class.getClassLoader().getResourceAsStream("config.properties");
            Properties properties = new Properties();
            properties.load(inputStream);

            return properties;
        } catch (IOException e) {
            e.printStackTrace();
            logger.log(Level.SEVERE ,"Não foi possível carregar o arquivo de propriedades");
        }

        return null;
    }

	public static String getStringAppProp(String prop){
    	return (String) mapProperties.get(prop);
	}

    /**
     * --------------------------------------
     * Obtem profile de execucao da aplicacao
     * --------------------------------------
     */
    public static String getApplicationEnvProperty() {
        return getStringAppProp("application.env");
    }

    /**
     * --------------------------
     * Obtem dominio da aplicacao
     * --------------------------
     */
    public static String getApplicationDomainProperty() {
        return getStringAppProp("application.domain");
    }

    /**
     * -----------------------------------------
     * Obtem o contexto de execucao da aplicacao
     * -----------------------------------------
     */
    public static String getApplicationContextPathProperty() {
        return getStringAppProp("application.contextPath");
    }

    /**
     * -----------------------------------------------------------
     * Obtem o protocolo web utilizado pela aplicacao (HTTP/HTTPS)
     * -----------------------------------------------------------
     */
    public static String getApplicationHttpProtocoloProperty() {
        return getStringAppProp("application.http.protocolo");
    }
    
    /**
     * -----------------------------------------------------------
     * Obtem o dominio web utilizado pela Front-End
     * -----------------------------------------------------------
     */
    public static String getApplicationDomainFrontProperty() {
        return getStringAppProp("application.domain.front");
    }
    
    public static String getApplicationDomainFrontVersion() {
        return getStringAppProp("application.domain.front.version");
    }
    
    
    /**
     * -----------------------------------------------------------
     * Obtem o token utilizado pela API
     * -----------------------------------------------------------
     */
    public static String getApplicationTokenApiProperty() {
        return getStringAppProp("application.token.api");
    }

    /**
     * ---------------------------------------------------------
     * Obtem o nome da unidade de persistencia (persistence.xml)
     * ---------------------------------------------------------
     */
    public static String getDbPersistenceUnitProperty() {
        return getStringAppProp("db.persistenceUnit");
    }

    /**
     * --------------------------------------------------------------------------
     * Obtem o driver de conexao a ser utilizado pela aplicacao (persistence.xml)
     * --------------------------------------------------------------------------
     *
     * @return string
     */
    public static String getDbDriverClassNameProperty() {
        return getStringAppProp("db.driverClassName");
    }

    /**
     * ------------------------------------------------------------
     * Obtem a url de conexao com a base de dados (persistence.xml)
     * ------------------------------------------------------------
     */
    public static String getDbUrlProperty() {
        return getStringAppProp("db.url");
    }

    /**
     * ---------------------------------------------------
     * Obtem o username da base de dados (persistence.xml)
     * ---------------------------------------------------
     */
    public static String getDbUsernameProperty() {
        return getStringAppProp("db.username");
    }

    /**
     * ---------------------------------------------------
     * Obtem o password da base de dados (persistence.xml)
     * ---------------------------------------------------
     */
    public static String getDbPasswordProperty() {
        return getStringAppProp("db.password");
    }

    /**
     * ----------------------------------------------------------
     * Obtem o dialeto utilizado pelo hibernate (persistence.xml)
     * ----------------------------------------------------------
     * https://docs.jboss.org/hibernate/orm/5.0/manual/en-US/html/ch03.html
     */
    public static String getHibernateDialectProperty() {
        return getStringAppProp("hibernate.dialect");
    }

    /**
     * -----------------------------------------------------------------------
     * Define se as queries de banco de dados serao exibidas (persistence.xml)
     * -----------------------------------------------------------------------
     * https://docs.jboss.org/hibernate/orm/5.0/manual/en-US/html/ch03.html
     */
    public static String getHibernateShowSqlProperty() {
        return getStringAppProp("hibernate.show_sql");
    }

    /**
     * --------------------------------------------------------------------------------
     * Define se as queries de banco de dados terao a saida formatada (persistence.xml)
     * --------------------------------------------------------------------------------
     * https://docs.jboss.org/hibernate/orm/5.0/manual/en-US/html/ch03.html
     */
    public static String getHibernateFormatSqlProperty() {
        return getStringAppProp("hibernate.format_sql");
    }

    /**
     * --------------------------------------------------------------------------------
     * Define a estrategia de geracao da base de dados pelo hibernate (persistence.xml)
     * --------------------------------------------------------------------------------
     * https://docs.jboss.org/hibernate/orm/5.0/manual/en-US/html/ch03.html
     */
    public static String getHibernateHbm2ddlAutoProperty() {
        return getStringAppProp("hibernate.hbm2ddl.auto");
    }
    
    /**
     * --------------------------------------------------------------------------------
     * Define o tempo de conexão
     * --------------------------------------------------------------------------------
     */
    public static String getHibernateHikariConnectionTimeoutProperty() {
        return getStringAppProp("hibernate.hikari.connectionTimeout");
    }
    
    /**
     * --------------------------------------------------------------------------------
     * Define o tempo minimo de conexão ociosa
     * --------------------------------------------------------------------------------
     */
    public static String getHibernateHikariMinimumIdleProperty() {
    	return getStringAppProp("hibernate.hikari.minimumIdle");
    }
    
    /**
     * --------------------------------------------------------------------------------
     * Define o tempo minimo de conexão ociosa
     * --------------------------------------------------------------------------------
     */
    public static String getHibernateHikariMaximumPoolSizeProperty() {
    	return getStringAppProp("hibernate.hikari.maximumPoolSize");
    }
    
    /**
     * --------------------------------------------------------------------------------
     * Define o tempo minimo de conexão ociosa
     * --------------------------------------------------------------------------------
     */
    public static String getHibernateHikariIdleTimeoutProperty() {
    	return getStringAppProp("hibernate.hikari.idleTimeout");
    }
    
    /**
     * -------------------------------------------------------------------------------
     * Url do Active directory
     * -------------------------------------------------------------------------------
     */
    
    public static String getLdapUrlProperty() {
    	return getStringAppProp("ldap.url");
    }
    
    /**
     * -------------------------------------------------------------------------------
     * Diretório base do Active directory
     * -------------------------------------------------------------------------------
     */
    
    public static String getLdapSearchBaseProperty() {
    	return getStringAppProp("ldap.searchBase");
    }
    
    /**
     * -------------------------------------------------------------------------------
     * Dominio do Active directory
     * -------------------------------------------------------------------------------
     */
    
    public static String getLdapDomainProperty() {
    	return getStringAppProp("ldap.domain");
    }
    
    /**
     * -------------------------------------------------------------------------------
     * Usuario do Active directory
     * -------------------------------------------------------------------------------
     */
    
    public static String getLdapUserProperty() {
    	return getStringAppProp("ldap.user");
    }
    
    /**
     * -------------------------------------------------------------------------------
     * Senha do Active directory
     * -------------------------------------------------------------------------------
     */
    
    public static String getLdapPasswordProperty() {
    	return getStringAppProp("ldap.password");
    }

    /**
     * ------------------
     * Obtém dados da JWT
     * ------------------
     */
    public static String getJwtFraseSegredo() {
        return getStringAppProp("jwt.frase.segredo");
    }

    public static int getJwtMinutosToken() {
        return Integer.parseInt(getStringAppProp("jwt.minutos.token").trim());
    }
    /**
     * SMTP
     * 
     */
    public static String getMailSmtpHost() {
        return getStringAppProp("mail.smtp.host");
    }
    public static String getMailSmtpPort() {
    	return getStringAppProp("mail.smtp.port");
    }
    public static String getMailUser() {
    	return getStringAppProp("mail.user");
    }
    public static String getMailPassword() {
    	return getStringAppProp("mail.password");
    }
    
}
