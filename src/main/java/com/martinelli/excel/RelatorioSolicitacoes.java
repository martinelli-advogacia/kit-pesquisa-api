package com.martinelli.excel;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.martinelli.dao.SolicitacaoPesquisaDAO;
import com.martinelli.dto.RelatorioFilterDTO;
import com.martinelli.dto.ResultadoPaginado;
import com.martinelli.dto.SolicitacaoPesquisaConsultaDTO;
import com.martinelli.utils.HibernateUtils;
import com.martinelli.utils.exceptions.TipoNaoImplementadoException;
import com.martinelli.utils.exceptions.XLSXNenhumaSolicitacaoException;

public class RelatorioSolicitacoes {
	private static final Logger LOG = Logger.getLogger(RelatorioSolicitacoes.class.getName());


	String textoCabecalho[] = { "Data de conclusão", "Profissional", "Quem fez", "Unid.", "Empresa", "UF", "Mês" };

	public static void main(String[] args) {
		HibernateUtils.getEntityManager();
		SolicitacaoPesquisaDAO dao = new SolicitacaoPesquisaDAO();
		RelatorioSolicitacoes p = new RelatorioSolicitacoes();
		try {
			RelatorioFilterDTO filtros = new RelatorioFilterDTO();
			filtros	.setDataFim(LocalDateTime.now())
					.setDataInicio(LocalDateTime.now().minusYears(1))
					.setPage(0)
					.setQtdItens(0);
			ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> solicitacoesDTO = dao.buscarPesquisasSolicitante(
					filtros);

			IOUtils.copy(p.gerarXLSX(solicitacoesDTO.getResult()), new FileOutputStream("teste.xlsx"));
		} catch (TipoNaoImplementadoException e) {
			LOG.severe(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			LOG.severe(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			LOG.severe(e.getMessage());
			e.printStackTrace();
		}
	}

	public ByteArrayInputStream gerarXLSX(List<SolicitacaoPesquisaConsultaDTO> solicitacoes)
			throws IOException, TipoNaoImplementadoException, XLSXNenhumaSolicitacaoException, URISyntaxException {
		InputStream excelFile = getClass().getClassLoader().getResourceAsStream("modelos_xlsx/modelo_pesquisas.xlsx");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		XSSFWorkbook workbook = new XSSFWorkbook(excelFile);
		workbook.setMissingCellPolicy(Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
		XSSFSheet sheet = workbook.getSheetAt(0);
		if (solicitacoes.isEmpty()) {
			workbook.close();
			throw new XLSXNenhumaSolicitacaoException();
		}
		for (int i = 0; i < solicitacoes.size(); i++) {
			XSSFRow row = sheet.createRow(i + 1);
			SolicitacaoPesquisaConsultaDTO sol = solicitacoes.get(i);

			LocalDateTime dataCriacao = sol.getDataAtualizacao();
			String mes = dataCriacao.getMonth().getDisplayName(TextStyle.FULL, Locale.ROOT);
			setCell(row, 0, dataCriacao.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			setCell(row, 1, sol.getEmAtendimentoPor().getNome());
			setCell(row, 2, sol.getSolicitadoPor().getNome());
			// setCell(row, 3, sol.getPesquisadorResponsavel().getUf());
			setCell(row, 3, "");
			setCell(row, 4, sol.getEmpresa());
			setCell(row, 5, mes);
		}
		excelFile.close();
		workbook.write(baos);
		workbook.close();
		return new ByteArrayInputStream(baos.toByteArray());
	}

	public void setCell(XSSFRow row, int nCell, Object valor) throws TipoNaoImplementadoException {
		XSSFCell cell = row.createCell(nCell);
		if (valor == null)
			cell.setCellValue("NULL");
		else if (valor instanceof Double)
			cell.setCellValue((Double) valor);
		else if (valor instanceof Integer)
			cell.setCellValue((Integer) valor);
		else if (valor instanceof String)
			cell.setCellValue((String) valor);
		else if (valor instanceof LocalDate)
			cell.setCellValue(valor.toString());
		else if (valor instanceof LocalDateTime)
			cell.setCellValue(valor.toString());
		else
			throw new TipoNaoImplementadoException();
	}
}