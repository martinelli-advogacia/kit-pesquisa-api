package com.martinelli.service;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.pdfbox.io.IOUtils;

import com.martinelli.dao.ArquivoDAO;
import com.martinelli.dao.ItemPesquisaDAO;
import com.martinelli.dao.SolicitacaoPesquisaDAO;
import com.martinelli.dto.ArquivoComAnexoDTO;
import com.martinelli.dto.ArquivoDTO;
import com.martinelli.model.Arquivo;
import com.martinelli.model.ItemPesquisa;
import com.martinelli.model.SolicitacaoPesquisa;
import com.martinelli.model.Usuario;
import com.martinelli.utils.exceptions.ArquivoInexistenteException;
import com.martinelli.utils.exceptions.ArquivoOrfaoException;
import com.martinelli.utils.exceptions.SemPermissaoException;
import com.martinelli.utils.exceptions.SolicitacaoConcluidaException;
import com.martinelli.utils.exceptions.SolicitacaoInexistenteException;

public class ArquivoService {

	static final Logger LOGGER = Logger.getLogger(ArquivoService.class.getName());

	static final SolicitacaoPesquisaDAO SOLICITACAO_PESQUISA_DAO = new SolicitacaoPesquisaDAO();

	static final ItemPesquisaDAO ITEM_PESQUISA_DAO = new ItemPesquisaDAO();

	static final ArquivoDAO ARQUIVO_DAO = new ArquivoDAO();

	public ArquivoDTO salvar(Long idSolicitacao, ArquivoDTO info, InputStream arquivo) throws Exception {
		SolicitacaoPesquisa solicitacao = null;
		Arquivo newArquivo = new Arquivo();
		ArquivoDTO arquivoSalvo = null;
		List<ItemPesquisa> itens = new ArrayList<>();
		byte[] byteArquivo = null;
		if (info.getItens() == null)
			throw new ArquivoOrfaoException();
		
		solicitacao = SOLICITACAO_PESQUISA_DAO.find(idSolicitacao);
		byteArquivo = (arquivo == null) ? null : IOUtils.toByteArray(arquivo);

		newArquivo	.setAnexo(byteArquivo)
					.setNomeArquivo(info.getFileName())
					.setTipoConteudo(info.getContentType())
					.setUltimoAtualizador(new Usuario(info.getAtualizador()))
					.setDataAtualizacao(LocalDateTime.now())
					.setEmpresaCNPJ(solicitacao.getEmpresa());

		info.getItens().stream().forEach(item -> {
			try {
				itens.add(ITEM_PESQUISA_DAO.find(item.getId()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		arquivoSalvo = ARQUIVO_DAO.saveComItens(newArquivo, itens);
		return arquivoSalvo;

	}

	public void deletar(Long idArquivo, long idSolicitacao, String atualizador) throws Exception {
		if (ARQUIVO_DAO.find(idArquivo) == null)
			throw new ArquivoInexistenteException();
		SolicitacaoPesquisa sol = SOLICITACAO_PESQUISA_DAO.find(idSolicitacao);
		if(sol == null)
			throw new SolicitacaoInexistenteException();
		if(sol.getConcluida())
			throw new SolicitacaoConcluidaException();

		ARQUIVO_DAO.delete(idArquivo, atualizador);
	}

	public ArquivoDTO find(Long idArquivo) throws Exception {
		Arquivo arq = ARQUIVO_DAO.find(idArquivo);
		if (ARQUIVO_DAO.find(idArquivo) == null)
			throw new ArquivoInexistenteException();

		return new ArquivoDTO(arq);
	}

	public List<ArquivoDTO> findTodos(Long idSolicitacao, String solicitante, boolean isPesquisador) throws Exception {
		SolicitacaoPesquisa solicitacao = SOLICITACAO_PESQUISA_DAO.find(idSolicitacao);

		if (solicitacao == null)
			throw new SolicitacaoInexistenteException();
		if (!isPesquisador && !solicitacao.getSolicitante().getLogin().equals(solicitante))
			throw new SemPermissaoException();

		List<Arquivo> arquivosEnt = ARQUIVO_DAO.findTodosSolicitacao(idSolicitacao);
		List<ArquivoDTO> arquivos = new ArrayList<>();
		for (Arquivo arqEnt : arquivosEnt) {
			arquivos.add(new ArquivoDTO(arqEnt));
		}
		return arquivos;
	}

	public ArquivoComAnexoDTO getFile(Long idSolicitacao, Long idArquivo, String solicitante, boolean isPesquisador)
			throws Exception {
		Arquivo arq = ARQUIVO_DAO.find(idArquivo);
		SolicitacaoPesquisa solicitacao = SOLICITACAO_PESQUISA_DAO.find(idSolicitacao);

		if (arq == null)
			throw new ArquivoInexistenteException();
		if (solicitacao == null)
			throw new SolicitacaoInexistenteException();
		if (!isPesquisador && !solicitacao.getSolicitante().getLogin().equals(solicitante))
			throw new SemPermissaoException();

		return new ArquivoComAnexoDTO(arq);
	}

	public List<ArquivoComAnexoDTO> getAllFiles(Long idSolicitacao, String solicitante, boolean isPesquisador)
			throws Exception {
		SolicitacaoPesquisa solicitacao = SOLICITACAO_PESQUISA_DAO.find(idSolicitacao);

		if (solicitacao == null)
			throw new SolicitacaoInexistenteException();
		if (!isPesquisador && !solicitacao.getSolicitante().getLogin().equals(solicitante))
			throw new SemPermissaoException();

		List<Arquivo> arquivosEnt = ARQUIVO_DAO.findTodosSolicitacao(idSolicitacao);
		List<ArquivoComAnexoDTO> arquivos = new ArrayList<>();
		for (Arquivo arqEnt : arquivosEnt) {
			arquivos.add(new ArquivoComAnexoDTO(arqEnt));
		}
		return arquivos;
	}

}
