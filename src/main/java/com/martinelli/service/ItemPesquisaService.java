package com.martinelli.service;

import java.util.List;
import java.util.logging.Logger;

import com.martinelli.dao.ItemPesquisaDAO;
import com.martinelli.model.ItemPesquisa;

public class ItemPesquisaService {
	
	static final Logger LOGGER = Logger.getLogger(ItemPesquisaService.class.getName());
	
	static final ItemPesquisaDAO ITEM_PESQUISA_DAO = new ItemPesquisaDAO();
	
	public boolean setSemResultado(List<ItemPesquisa> itens, Boolean flag, String atualizador) {
		return ITEM_PESQUISA_DAO.setSemResultado(itens, flag, atualizador);
	}

}
