package com.martinelli.service;

import java.util.List;
import java.util.logging.Logger;

import com.martinelli.dao.EmpresaCNPJDAO;
import com.martinelli.model.EmpresaCNPJ;

public class EmpresaCNPJService {
	
	static final Logger LOGGER = Logger.getLogger(EmpresaCNPJService.class.getName());
	
	static final EmpresaCNPJDAO EMPRESA_CNPJDAO = new EmpresaCNPJDAO();
	
	public List<EmpresaCNPJ> buscaEmpresa(String busca){
		return EMPRESA_CNPJDAO.findByNomeOrCNPJ(busca);
	}

}
