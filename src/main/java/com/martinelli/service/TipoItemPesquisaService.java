package com.martinelli.service;

import java.util.ArrayList;
import java.util.List;

import com.martinelli.dao.SolicitacaoPesquisaDAO;
import com.martinelli.dao.TipoItemPesquisaDAO;
import com.martinelli.dto.TipoItemPesquisaDTO;
import com.martinelli.model.SolicitacaoPesquisa;
import com.martinelli.model.TipoItemPesquisa;
import com.martinelli.utils.exceptions.SemPermissaoException;
import com.martinelli.utils.exceptions.SolicitacaoInexistenteException;

public class TipoItemPesquisaService {
	static final TipoItemPesquisaDAO DAO = new TipoItemPesquisaDAO();
	static final SolicitacaoPesquisaDAO SOLICITACAO_PESQUISA_DAO = new SolicitacaoPesquisaDAO();

	public List<TipoItemPesquisaDTO> getAllTipoItens() throws Exception {
		List<TipoItemPesquisa> itens =  DAO.getAll();
		List<TipoItemPesquisaDTO> res = new ArrayList<>();
		for (TipoItemPesquisa ent : itens) {
			res.add(new TipoItemPesquisaDTO(ent));
		}
		return res;
	}

	public List<TipoItemPesquisaDTO> buscarSemArquivo(Long idSolicitacao, String solicitante) throws Exception {
		SolicitacaoPesquisa solicitacao = SOLICITACAO_PESQUISA_DAO.find(idSolicitacao);
		if (solicitacao == null)
			throw new SolicitacaoInexistenteException();
		if (!solicitacao.getSolicitante().getLogin().equals(solicitante))
			throw new SemPermissaoException();

		return DAO.findSemArquivo(idSolicitacao);
	}
}
