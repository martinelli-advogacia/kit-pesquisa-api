package com.martinelli.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

import com.martinelli.Application;
import com.martinelli.dao.PermissionamentoDAO;
import com.martinelli.dao.UsuarioDAO;
import com.martinelli.dto.UsuarioDTO;
import com.martinelli.model.Permissionamento;
import com.martinelli.model.Usuario;
import com.martinelli.utils.ErroLDAP;
import com.martinelli.utils.LDAPConnector;
import com.martinelli.utils.LDAPService;
import com.martinelli.utils.exceptions.LdapException;
import com.martinelli.utils.exceptions.UsuarioInvalidoException;
import com.martinelli.utils.exceptions.ldap.BadArgumentException;

public class UsuarioService {

	static final Logger LOGGER = Logger.getLogger(UsuarioService.class.getName());

	static final UsuarioDAO USUARIO_DAO = new UsuarioDAO();
	static final PermissionamentoDAO PERMISSIONAMENTO_DAO = new PermissionamentoDAO();

	public List<UsuarioDTO> getAll() {
		try {
			return USUARIO_DAO.getAllComPerfil();
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
			return new ArrayList<>();
		}

	}

	public UsuarioDTO updateUsuario(UsuarioDTO usuario) {
		Usuario newUsuario = new Usuario(usuario);
		Permissionamento perfil = null;
		try {
			USUARIO_DAO.update(newUsuario);
			perfil = PERMISSIONAMENTO_DAO.buscarPorLogin(newUsuario.getLogin());
			if (perfil != null) {
				perfil.setRoleAlowed(usuario.getPerfil()).setSelecionarItens(usuario.getSelecionarItens());
				PERMISSIONAMENTO_DAO.update(perfil);
			} else {
				perfil = new Permissionamento();
				perfil	.setLogin(newUsuario)
						.setRoleAlowed(usuario.getPerfil())
						.setSelecionarItens(usuario.getSelecionarItens());
				PERMISSIONAMENTO_DAO.save(perfil);
			}
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}

		return new UsuarioDTO(newUsuario, perfil);

	}

	public UsuarioDTO updateUfUsuario(UsuarioDTO usuario) {
		Usuario user = USUARIO_DAO.find(usuario.getLogin());
		user.setUf(usuario.getUf());
		try {
			USUARIO_DAO.update(user);
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
			e.printStackTrace();
		}
		return new UsuarioDTO(user);
	}

	public UsuarioDTO findByLoginComPerfil(String login) {
		UsuarioDTO usuario = USUARIO_DAO.findComPerfil(login);
		return usuario;
	}

	public List<UsuarioDTO> buscaSolicitante(String busca) {
		return USUARIO_DAO.buscaSolicitante(busca);
	}

	public List<UsuarioDTO> buscaAtendente() {
		return USUARIO_DAO.buscaAtendente();
	}

	public List<UsuarioDTO> buscaUsuarioAD(String nome)
			throws LdapException, UsuarioInvalidoException, NamingException, BadArgumentException {
		LdapContext ctx = null;

		try {
			ctx = LDAPConnector.openConnection(
					Application.getLdapDomainProperty() + "\\" + Application.getLdapUserProperty(),
					Application.getLdapPasswordProperty(), Application.getLdapUrlProperty());
		} catch (NamingException e) {
			e.printStackTrace();
			LOGGER.severe(e.getMessage());
			String erroLdap = ErroLDAP.descobrirErroNamingException(e.getMessage());
			LOGGER.info(erroLdap);
			throw new LdapException(erroLdap);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.severe(e.getMessage());
			throw new UsuarioInvalidoException();
		}

		return LDAPService.getUserByName(ctx, nome, Application.getLdapSearchBaseProperty());
	}

}
