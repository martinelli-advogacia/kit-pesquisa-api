package com.martinelli.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.martinelli.dao.EmpresaCNPJDAO;
import com.martinelli.dao.SolicitacaoPesquisaDAO;
import com.martinelli.dao.UsuarioDAO;
import com.martinelli.dto.EmpresaCNPJDTO;
import com.martinelli.dto.RelatorioFilterDTO;
import com.martinelli.dto.ResultadoPaginado;
import com.martinelli.dto.SolicitacaoPesquisaConsultaDTO;
import com.martinelli.dto.SolicitacaoPesquisaDTO;
import com.martinelli.dto.SolicitacaoPesquisaFilterDTO;
import com.martinelli.dto.SolicitacaoPorVariasEmpresasDTO;
import com.martinelli.dto.TipoItemPesquisaDTO;
import com.martinelli.dto.UsuarioDTO;
import com.martinelli.model.EmpresaCNPJ;
import com.martinelli.model.ItemPesquisa;
import com.martinelli.model.SolicitacaoPesquisa;
import com.martinelli.model.Usuario;
import com.martinelli.utils.EmailUtil;
import com.martinelli.utils.exceptions.SemPermissaoException;
import com.martinelli.utils.exceptions.SolicitacaoInexistenteException;
import com.martinelli.utils.exceptions.SolicitacaoPesquisaInvalidaException;

public class SolicitacaoPesquisaService {
	static final SolicitacaoPesquisaDAO SOLICITACAO_DAO = new SolicitacaoPesquisaDAO();
	static final UsuarioDAO USUARIO_DAO = new UsuarioDAO();
	static final EmpresaCNPJDAO EMPRESA_DAO = new EmpresaCNPJDAO();

	private List<ItemPesquisa> criaItens(List<TipoItemPesquisaDTO> list, String solicitante) {
		LocalDateTime agora = LocalDateTime.now();
		Usuario solicitanteUsuario = USUARIO_DAO.find(solicitante);
		return list	.stream()
					.map(tipo -> new ItemPesquisa(tipo, false, false, agora, solicitanteUsuario, null))
					.collect(Collectors.toList());
	}

	private EmpresaCNPJ buscaPorCNPJ(EmpresaCNPJ empresaCNPJ) {
		return EMPRESA_DAO.findByCNPJ(empresaCNPJ.getCnpj()).orElse(empresaCNPJ);
	}

	// Cria uma solicitacao a partir de um CNPJ
	public List<SolicitacaoPesquisaDTO> criaSolicitacao(SolicitacaoPorVariasEmpresasDTO solicitacoes,
			String solicitante) throws Exception {

		// Caso o objeto não seja válido
		if (!solicitacoes.isValid())
			throw new SolicitacaoPesquisaInvalidaException();

		// cria a solicitacao de pesquisa com os itens
		List<ItemPesquisa> itensPesquisa = criaItens(solicitacoes.getListaItems(), solicitante);
		List<SolicitacaoPesquisa> solicitacoesSalvas = new ArrayList<>();

		for (EmpresaCNPJDTO empresa : solicitacoes.getEmpresasCNPJ()) {
			SolicitacaoPesquisa novaSolicitacaoPesquisa = new SolicitacaoPesquisa();
			// verificar se a empresa ja existe no banco, se não existe será criada junto a
			// solicitacao
			EmpresaCNPJ empresaEncontrada = buscaPorCNPJ(new EmpresaCNPJ(empresa));
			// se o nome da empresa encontrada está vazio, sobrepor com o nome da empresa da
			// nova solicitação
			// se a nova solicitação também está com o nome em branco, sobrepomos com um
			// nome em branco
			if (empresaEncontrada.getNomeEmpresa().trim().isEmpty()) {
				empresaEncontrada.setNomeEmpresa(empresa.getNomeEmpresa());
				empresaEncontrada = EMPRESA_DAO.update(empresaEncontrada);
			}
			Usuario solicitanteUsuario = USUARIO_DAO.find(solicitante);

			// inicializa a solicitacao nova que sera salva
			novaSolicitacaoPesquisa	.setSolicitante(solicitanteUsuario)
									.setEmpresa(empresaEncontrada)
									.setConcluida(false)
									.setUrgente(solicitacoes.getUrgente())
									.setObservacao(solicitacoes.getObservacao())
									.setDataCriacao(LocalDateTime.now())
									.setDataAtualizacao(LocalDateTime.now());
			solicitacoesSalvas.add(SOLICITACAO_DAO.saveComItens(novaSolicitacaoPesquisa, itensPesquisa));
		}

		List<SolicitacaoPesquisaDTO> resultado = new ArrayList<>();
		for (SolicitacaoPesquisa ent : solicitacoesSalvas) {
			resultado.add(new SolicitacaoPesquisaDTO(ent));
		}
		return resultado;
	}

	public SolicitacaoPesquisa buscaSolicitacao(long id, String solicitante, boolean isPesquisador) throws Exception {
		SolicitacaoPesquisa solicitacao = SOLICITACAO_DAO.find(id);
		if (solicitacao == null)
			throw new SolicitacaoInexistenteException();

		if (!isPesquisador && !solicitacao.getSolicitante().getLogin().equals(solicitante))
			throw new SemPermissaoException();

		return solicitacao;

	}

	public List<SolicitacaoPesquisaConsultaDTO> buscaListaPesquisasTodas() throws Exception {
		return SOLICITACAO_DAO.buscarPesquisasTodas();
	}

	public List<SolicitacaoPesquisaConsultaDTO> buscaListaPesquisasNaoConcluidas() throws Exception {
		return SOLICITACAO_DAO.buscarPesquisasNaoConcluidas();
	}

	public ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> buscaListaPesquisasConcluidas(
			SolicitacaoPesquisaFilterDTO filtros, long qtdItens, long page) throws Exception {
		return SOLICITACAO_DAO.buscarPesquisasConcluidas(filtros, qtdItens, page);
	}

	public ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> buscaListaPesquisasSolicitante(
			SolicitacaoPesquisaFilterDTO filtros, long qtdItens, long page) throws Exception {
		return SOLICITACAO_DAO.buscarPesquisasSolicitante(filtros, qtdItens, page);
	}

	public SolicitacaoPesquisaConsultaDTO atualizarAtendente(long id, String atendente) throws Exception {
		SolicitacaoPesquisaConsultaDTO sol = SOLICITACAO_DAO.atualizaAtendente(id, atendente);
		return sol;
	}

	public SolicitacaoPesquisaConsultaDTO concuirSolicitacao(long id, String atendente) throws Exception {
		SolicitacaoPesquisaConsultaDTO sol = SOLICITACAO_DAO.concluirSolicitacao(id, atendente);

		String destinatario = sol.getSolicitadoPor().getEmail();
		String assunto = "Solicitação de Pesquisa";
		String corpoEmail = "A solicitação de pesquisa do cnpj "
				+ sol.getCnpj()
				+ ", empresa "
				+ sol.getEmpresa()
				+ " foi concluída e está disponível no portal do NIN.";
		EmailUtil.enviarEmailHTML("kit-pesquisa-nin-no-reply@martinelli.adv.br", destinatario, assunto, corpoEmail);
		return sol;
	}

	public Set<UsuarioDTO> buscarSolicitantes() {
		return SOLICITACAO_DAO.buscarSolicitantes();
	}

	public ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> buscaListaPesquisas(RelatorioFilterDTO filtros) {
		return SOLICITACAO_DAO.buscarPesquisasSolicitante(filtros);
	}

	public List<SolicitacaoPesquisaConsultaDTO> buscaListaPesquisasConcluidasRelatorio(RelatorioFilterDTO filtros) {
		return SOLICITACAO_DAO.buscaListaPesquisasConcluidasRelatorio(filtros);
	}

	public SolicitacaoPesquisaConsultaDTO reverterSolicitacao(long id) throws Exception {
		return SOLICITACAO_DAO.reverterSolicitacao(id);
	}

	public boolean excluiSolicitacaoPropria(Long id, String login) throws Exception {
		SolicitacaoPesquisa solicitacao = SOLICITACAO_DAO.find(id);
		if (solicitacao == null)
			throw new SolicitacaoInexistenteException();
		if(!solicitacao.getSolicitante().getLogin().equals(login))
			throw new SemPermissaoException();
		SOLICITACAO_DAO.delete(id);
		return true;
	}
	
	public boolean excluiSolicitacao(long id) throws Exception{
		SolicitacaoPesquisa solicitacao = SOLICITACAO_DAO.find(id);
		if (solicitacao == null)
			throw new SolicitacaoInexistenteException();
		SOLICITACAO_DAO.delete(id);
		return true;
	}

}
