package com.martinelli.service;

import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.jpa.QueryHints;

import com.martinelli.Application;
import com.martinelli.dto.HealthCheckDTO;
import com.martinelli.dto.HealthCheckJsonDTO;
import com.martinelli.model.TipoItemPesquisa;
import com.martinelli.utils.ErroLDAP;
import com.martinelli.utils.HibernateUtils;
import com.martinelli.utils.LDAPConnector;
import com.martinelli.utils.exceptions.LdapException;
import com.martinelli.utils.exceptions.UsuarioInvalidoException;

public class HealthCheckService {

	private static final Logger LOGGER = Logger.getLogger(HealthCheckService.class.getName());

	private boolean checkLDAPConnection() throws LdapException, UsuarioInvalidoException {

		LdapContext ctx = null;

		try {
			ctx = LDAPConnector.openConnection(
					Application.getLdapDomainProperty() + "\\" + Application.getLdapUserProperty(),
					Application.getLdapPasswordProperty(), Application.getLdapUrlProperty());
		} catch (NamingException e) {
			e.printStackTrace();
			LOGGER.severe(e.getMessage());
			String erroLdap = ErroLDAP.descobrirErroNamingException(e.getMessage());
			LOGGER.info(erroLdap);
			throw new LdapException(erroLdap);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.severe(e.getMessage());
			throw new UsuarioInvalidoException();
		}

		return ctx != null;
	}

	private boolean checkDBConnection() {
		EntityManager em = HibernateUtils.getEntityManager();

		TypedQuery<TipoItemPesquisa> query = em.createQuery("FROM TipoItemPesquisa", TipoItemPesquisa.class);
		query.setMaxResults(1).setHint(QueryHints.HINT_READONLY, true);

		if (!query.getResultList().isEmpty()) {
			em.close();
			return true;
		}
		em.close();
		return false;
	}

	public HealthCheckJsonDTO veficacaoGeral() {
		HealthCheckDTO ldap = null;
		HealthCheckDTO db = null;
		HealthCheckDTO geral = null;
		try {
			if (checkLDAPConnection()) {
				ldap = new HealthCheckDTO("OK", "Funcionando corretamente.");
			}
		} catch (LdapException e) {
			ldap = new HealthCheckDTO("FAIL", e.getMessage());
		} catch (UsuarioInvalidoException e) {
			ldap = new HealthCheckDTO("FAIL",
					"Não foi possivel se conectar com o Active directory. Verifique o usuário e a senha do sistema.");
		} catch (Exception e) {
			ldap = new HealthCheckDTO("FAIL", e.getMessage());
		}

		try {
			if (checkDBConnection()) {
				db = new HealthCheckDTO("OK", "Funcionando corretamente.");
			}
		} catch (Exception e) {
			db = new HealthCheckDTO("OK", e.getMessage());
		}

		if (ldap != null && ldap.getStatus().equals("OK") && db != null && db.getStatus().equals("OK")) {
			geral = new HealthCheckDTO("OK", "Funcionando corretamente.");
		} else {
			geral = new HealthCheckDTO("FAIL", "Foram identificados problemas.");
		}

		return new HealthCheckJsonDTO(geral, ldap, db);
	}

}
