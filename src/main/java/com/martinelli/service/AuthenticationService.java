package com.martinelli.service;

import java.time.LocalDateTime;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import javax.ws.rs.core.NewCookie;

import com.martinelli.Application;
import com.martinelli.dao.PermissionamentoDAO;
import com.martinelli.dao.UsuarioDAO;
import com.martinelli.dto.LoginDTO;
import com.martinelli.dto.UsuarioAutenticadoDTO;
import com.martinelli.model.Permissionamento;
import com.martinelli.model.Usuario;
import com.martinelli.security.jwt.TokenUtil;
import com.martinelli.security.jwt.enums.RolesEnum;
import com.martinelli.security.jwt.model.AuthenticationToken;
import com.martinelli.utils.CookieUtil;
import com.martinelli.utils.ErroLDAP;
import com.martinelli.utils.LDAPConnector;
import com.martinelli.utils.LDAPService;
import com.martinelli.utils.StacktraceToString;
import com.martinelli.utils.Tuple;
import com.martinelli.utils.exceptions.LdapException;
import com.martinelli.utils.exceptions.LoginVazioException;
import com.martinelli.utils.exceptions.UsuarioInvalidoException;
import com.martinelli.utils.exceptions.ldap.BadArgumentException;

public class AuthenticationService {

	static final Logger LOGGER = Logger.getLogger(AuthenticationService.class.getName());

	static final PermissionamentoDAO PERMISSIONAMENTO_DAO = new PermissionamentoDAO();
	static final UsuarioDAO USUARIO_DAO = new UsuarioDAO();

	public static Tuple<UsuarioAutenticadoDTO, NewCookie> login(LoginDTO loginDto) throws Exception {
		if (loginDto.isEmpty()) {
			throw new LoginVazioException();
		}
		LdapContext ctx = connectToLDAP(loginDto);

		UsuarioAutenticadoDTO usuario = getLDAPUser(loginDto, ctx);

		Usuario usuarioDB = USUARIO_DAO.find(usuario.getLogin());
		if (usuarioDB == null) {
			usuarioDB = new Usuario()	.setLogin(usuario.getLogin())
										.setNome(usuario.getNome())
										.setEmail(usuario.getEmail().equals("") ? null : usuario.getEmail())
										.setTelefone(usuario.getTelefone().equals("") ? null : usuario.getTelefone());
			try {
				USUARIO_DAO.save(usuarioDB);
			} catch (Exception e) {
				LOGGER.severe(e.getMessage());
			}
		}

		usuarioDB.setUltimoAcesso(LocalDateTime.now());
		try {
			USUARIO_DAO.update(usuarioDB);
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
		}

		Permissionamento permissoes = PERMISSIONAMENTO_DAO.buscarPorLogin(usuario.getLogin());

		RolesEnum role = null;
		if (permissoes == null) {
			role = RolesEnum.fromString("Solicitante");
		} else {
			role = RolesEnum.fromString(permissoes.getRoleAlowed());
			if (permissoes.getSelecionarItens()) {
				usuario.setSelecionarItens(permissoes.getSelecionarItens());
			}
		}

		usuario.setRolesAllowed(role.getName());
		usuario.setUf(usuarioDB.getUf());

		AuthenticationToken token = TokenUtil.emiteToken(usuario);

		NewCookie cookie = CookieUtil.createCookie(token.getToken(), false);

		return new Tuple<>(usuario, cookie);
	}

	private static UsuarioAutenticadoDTO getLDAPUser(LoginDTO loginDto, LdapContext ctx)
			throws LdapException, BadArgumentException {
		UsuarioAutenticadoDTO usuario;
		try {
			usuario = LDAPService.getUserByAccountName(ctx, loginDto.getUser().split("@")[0],
					Application.getLdapSearchBaseProperty());
		} catch (NamingException e) {
			LOGGER.severe(StacktraceToString.convert(e));
			String erroLdap = ErroLDAP.descobrirErroNamingException(e.getMessage());
			LOGGER.info(erroLdap);
			throw new LdapException(erroLdap);
		} catch (BadArgumentException e) {
			LOGGER.severe(StacktraceToString.convert(e));
			throw e;
		}
		return usuario;
	}

	private static LdapContext connectToLDAP(LoginDTO loginDto) throws LdapException, UsuarioInvalidoException {
		LdapContext ctx = null;

		try {
			ctx = LDAPConnector.openConnection(Application.getLdapDomainProperty() + "\\" + loginDto.getUser(),
					loginDto.getPassword(), Application.getLdapUrlProperty());
		} catch (NamingException e) {
			e.printStackTrace();
			LOGGER.severe(StacktraceToString.convert(e));
			String erroLdap = ErroLDAP.descobrirErroNamingException(e.getMessage());
			LOGGER.info(erroLdap);
			throw new LdapException(erroLdap);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.severe(StacktraceToString.convert(e));
			throw new UsuarioInvalidoException();
		}
		return ctx;
	}

}
