package com.martinelli.dto;

public class ResultadoPaginado<T> {

	long totalItens;
	long qtdItens;
	long currentPage;
	long maxPages;
	T result;

	public ResultadoPaginado() {
	}

	public ResultadoPaginado(T result, long currentPage, long maxPages, long qtdItens, long totalItens) {
		super();
		this.qtdItens = qtdItens;
		this.currentPage = currentPage;
		this.maxPages = maxPages;
		this.result = result;
		this.totalItens = totalItens;
	}

	public long getCurrentPage() {
		return currentPage;
	}

	public ResultadoPaginado<T> setCurrentPage(long currentPage) {
		this.currentPage = currentPage;
		return this;
	}

	public long getMaxPages() {
		return maxPages;
	}

	public ResultadoPaginado<T> setMaxPages(long maxPages) {
		this.maxPages = maxPages;
		return this;
	}

	public T getResult() {
		return result;
	}

	public ResultadoPaginado<T> setResult(T result) {
		this.result = result;
		return this;
	}

	public long getQtdItens() {
		return qtdItens;
	}

	public ResultadoPaginado<T> setQtdItens(long qtdItens) {
		this.qtdItens = qtdItens;
		return this;
	}

	public long getTotalItens() {
		return totalItens;
	}

	public ResultadoPaginado<T> setTotalItens(long totalItens) {
		this.totalItens = totalItens;
		return this;
	}

}
