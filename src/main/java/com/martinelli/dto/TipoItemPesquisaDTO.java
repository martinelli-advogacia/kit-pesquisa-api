package com.martinelli.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.martinelli.model.TipoItemPesquisa;

@JsonInclude(Include.NON_NULL)
public class TipoItemPesquisaDTO {
	long id;

	String titulo;
	
	String url;
	
	String descricao;

	public TipoItemPesquisaDTO() {
		super();
	}

	public TipoItemPesquisaDTO(Long id, String titulo,  String url, String descricao) {
		this.id = id;
		this.titulo = titulo;
		this.url = url;
		this.descricao = descricao;
	}

	public TipoItemPesquisaDTO(TipoItemPesquisa ent) {
		this.id = ent.getId();
		this.titulo = ent.getTitulo();
		this.url = ent.getUrl();
		this.descricao = ent.getDescricao();
	}

	public long getId() {
		return id;
	}

	public TipoItemPesquisaDTO setId(long id) {
		this.id = id;
		return this;
	}

	public String getTitulo() {
		return titulo;
	}

	public TipoItemPesquisaDTO setTitulo(String titulo) {
		this.titulo = titulo;
		return this;
	}

	public String getUrl() {
		return url;
	}

	public TipoItemPesquisaDTO setUrl(String url) {
		this.url = url;
		return this;
	}

	public String getDescricao() {
		return descricao;
	}

	public TipoItemPesquisaDTO setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}
	
	

}
