package com.martinelli.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolicitacaoPorEmpresaDTO {
	EmpresaCNPJDTO empresaCNPJ;
	List<TipoItemPesquisaDTO> listaItems = new ArrayList<>();
	boolean urgente;
	String observacao;

	public SolicitacaoPorEmpresaDTO() {
		super();
	}

	public SolicitacaoPorEmpresaDTO(EmpresaCNPJDTO empresaCNPJ, List<TipoItemPesquisaDTO> listaItems, Boolean urgente,
			String observacao) {
		this.empresaCNPJ = empresaCNPJ;
		this.listaItems = listaItems;
		this.urgente = urgente;
		this.observacao = observacao;
	}

	public boolean isValid() {
		if (this.empresaCNPJ == null || this.empresaCNPJ.getCnpj().isEmpty())
			return false;
		if (this.listaItems == null || this.listaItems.isEmpty())
			return false;
		if (this.urgente && (this.observacao == null || this.observacao.isEmpty()))
			return false;
		return true;
	}

	public EmpresaCNPJDTO getEmpresaCNPJ() {
		return empresaCNPJ;
	}

	public SolicitacaoPorEmpresaDTO setEmpresaCNPJ(EmpresaCNPJDTO empresaCNPJ) {
		this.empresaCNPJ = empresaCNPJ;
		return this;
	}

	public List<TipoItemPesquisaDTO> getListaItems() {
		return listaItems;
	}

	public SolicitacaoPorEmpresaDTO setListaItems(List<TipoItemPesquisaDTO> listaItems) {
		this.listaItems = listaItems;
		return this;
	}

	public boolean getUrgente() {
		return urgente;
	}

	public SolicitacaoPorEmpresaDTO setUrgente(boolean urgente) {
		this.urgente = urgente;
		return this;
	}

	public String getObservacao() {
		return observacao;
	}

	public SolicitacaoPorEmpresaDTO setObservacao(String observacao) {
		this.observacao = observacao;
		return this;
	}

	@Override
	public String toString() {
		return "SolicitacaoPorEmpresaDTO{"
				+ "empresaCNPJ="
				+ empresaCNPJ
				+ ", listaItems="
				+ listaItems
				+ ", urgente="
				+ urgente
				+ ", observacao='"
				+ observacao
				+ '\''
				+ '}';
	}
}
