package com.martinelli.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.martinelli.model.SolicitacaoPesquisa;
import com.martinelli.utils.deserializers.LocalDateTimeSerializer;

public class SolicitacaoPesquisaConsultaDTO {

	long id;
	UsuarioDTO emAtendimentoPor;
	UsuarioDTO solicitadoPor;
	String cnpj;
	String empresa;
	String uf;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	LocalDateTime dataCriacao;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	LocalDateTime dataAtualizacao;
	double porcConclusao;
	boolean urgente;
	String observacao;
	boolean concluido;
	List<ItemPesquisaDTO> itens;

	public SolicitacaoPesquisaConsultaDTO() {
		super();
	}

	public SolicitacaoPesquisaConsultaDTO(SolicitacaoPesquisa pesquisa) {
		this.id = pesquisa.getId();
		if (pesquisa.getPesquisadorResponsavel() != null)
			this.emAtendimentoPor = new UsuarioDTO(pesquisa.getPesquisadorResponsavel());
		if (pesquisa.getSolicitante() != null)
			this.solicitadoPor = new UsuarioDTO(pesquisa.getSolicitante());
		this.cnpj = pesquisa.getEmpresa().getCnpj();
		this.empresa = pesquisa.getEmpresa().getNomeEmpresa();
		this.uf = pesquisa.getEmpresa().getUf();
		this.dataCriacao = pesquisa.getDataCriacao();
		this.dataAtualizacao = pesquisa.getDataAtualizacao();
		if (!pesquisa.getItens().isEmpty()) {
			Double contItenConcluido = (double) pesquisa.getItens()
														.parallelStream()
														.filter(i -> i.getConcluido())
														.count();
			this.porcConclusao = (contItenConcluido / pesquisa.getItens().stream().count()) * 100;
			this.itens = new ArrayList<>();
			this.itens = pesquisa	.getItens()
									.parallelStream()
									.map(item -> new ItemPesquisaDTO(item))
									.collect(Collectors.toList());
		}
		this.urgente = pesquisa.getUrgente();
		this.observacao = pesquisa.getObservacao();
		this.concluido = pesquisa.getConcluida();
	}

	public boolean isUrgente() {
		return urgente;
	}

	public SolicitacaoPesquisaConsultaDTO setUrgente(boolean urgente) {
		this.urgente = urgente;
		return this;
	}

	public String getObservacao() {
		return observacao;
	}

	public SolicitacaoPesquisaConsultaDTO setObservacao(String observacao) {
		this.observacao = observacao;
		return this;
	}

	public SolicitacaoPesquisaConsultaDTO setId(long id) {
		this.id = id;
		return this;
	}

	public Long getId() {
		return id;
	}

	public SolicitacaoPesquisaConsultaDTO setId(Long id) {
		this.id = id;
		return this;
	}

	public UsuarioDTO getEmAtendimentoPor() {
		return emAtendimentoPor;
	}

	public SolicitacaoPesquisaConsultaDTO setEmAtendimentoPor(UsuarioDTO emAtendimentoPor) {
		this.emAtendimentoPor = emAtendimentoPor;
		return this;
	}

	public UsuarioDTO getSolicitadoPor() {
		return solicitadoPor;
	}

	public SolicitacaoPesquisaConsultaDTO setSolicitadoPor(UsuarioDTO solicitadoPor) {
		this.solicitadoPor = solicitadoPor;
		return this;
	}

	public String getCnpj() {
		return cnpj;
	}

	public SolicitacaoPesquisaConsultaDTO setCnpj(String cnpj) {
		this.cnpj = cnpj;
		return this;
	}

	public String getEmpresa() {
		return empresa;
	}

	public SolicitacaoPesquisaConsultaDTO setEmpresa(String empresa) {
		this.empresa = empresa;
		return this;
	}

	public String getUf() {
		return uf;
	}

	public SolicitacaoPesquisaConsultaDTO setUf(String uf) {
		this.uf = uf;
		return this;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public SolicitacaoPesquisaConsultaDTO setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
		return this;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public SolicitacaoPesquisaConsultaDTO setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
		return this;
	}

	public double getPorcConclusao() {
		return porcConclusao;
	}

	public SolicitacaoPesquisaConsultaDTO setPorcConclusao(double porcConclusao) {
		this.porcConclusao = porcConclusao;
		return this;
	}

	public boolean getConcluido() {
		return concluido;
	}

	public SolicitacaoPesquisaConsultaDTO setConcluido(boolean concluido) {
		this.concluido = concluido;
		return this;
	}

	public List<ItemPesquisaDTO> getItens() {
		return itens;
	}

	public SolicitacaoPesquisaConsultaDTO setItens(List<ItemPesquisaDTO> itens) {
		this.itens = itens;
		return this;
	}

	@Override
	public String toString() {
		return "SolicitacaoPesquisaConsultaDTO [id="
				+ id
				+ ", emAtendimentoPor="
				+ emAtendimentoPor
				+ ", solicitadoPor="
				+ solicitadoPor
				+ ", cnpj="
				+ cnpj
				+ ", empresa="
				+ empresa
				+ ", uf="
				+ uf
				+ ", dataCriacao="
				+ dataCriacao
				+ ", dataAtualizacao="
				+ dataAtualizacao
				+ ", concluido="
				+ concluido
				+ "]";
	}

}
