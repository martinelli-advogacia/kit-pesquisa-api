package com.martinelli.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.martinelli.model.ItemPesquisa;
import com.martinelli.model.SolicitacaoPesquisa;
import com.martinelli.utils.deserializers.LocalDateTimeSerializer;

public class SolicitacaoPesquisaDTO {
	long id;
	UsuarioDTO solicitante;
	UsuarioDTO pesquisadorResponsavel;
	String observacao;
	boolean urgente;
	boolean concluida;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	LocalDateTime dataCriacao;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	LocalDateTime dataAtualizacao;
	EmpresaCNPJDTO empresa;
	List<ItemPesquisaDTO> itens;

	public SolicitacaoPesquisaDTO() {
		super();
	}

	public SolicitacaoPesquisaDTO(SolicitacaoPesquisa solicitacao) {
		super();
		this.id = solicitacao.getId();
		this.solicitante = new UsuarioDTO(solicitacao.getSolicitante());
		this.pesquisadorResponsavel = solicitacao.getPesquisadorResponsavel() != null
				? new UsuarioDTO(solicitacao.getPesquisadorResponsavel())
				: null;
		this.observacao = solicitacao.getObservacao();
		this.urgente = solicitacao.getUrgente();
		this.concluida = solicitacao.getConcluida();
		this.dataCriacao = solicitacao.getDataCriacao();
		this.dataAtualizacao = solicitacao.getDataAtualizacao();
		if (solicitacao.getEmpresa() != null)
			this.empresa = new EmpresaCNPJDTO(solicitacao.getEmpresa());
		this.itens = new ArrayList<>();
		if (solicitacao.getItens() != null) {
			for (ItemPesquisa ent : solicitacao.getItens()) {
				this.itens.add(new ItemPesquisaDTO(ent));
			}
		}
	}

	public long getId() {
		return id;
	}

	public SolicitacaoPesquisaDTO setId(long id) {
		this.id = id;
		return this;
	}

	public UsuarioDTO getSolicitante() {
		return solicitante;
	}

	public SolicitacaoPesquisaDTO setSolicitante(UsuarioDTO solicitante) {
		this.solicitante = solicitante;
		return this;
	}

	public UsuarioDTO getPesquisadorResponsavel() {
		return pesquisadorResponsavel;
	}

	public SolicitacaoPesquisaDTO setPesquisadorResponsavel(UsuarioDTO pesquisadorResponsavel) {
		this.pesquisadorResponsavel = pesquisadorResponsavel;
		return this;
	}

	public String getObservacao() {
		return observacao;
	}

	public SolicitacaoPesquisaDTO setObservacao(String observacao) {
		this.observacao = observacao;
		return this;
	}

	public Boolean getUrgente() {
		return urgente;
	}

	public SolicitacaoPesquisaDTO setUrgente(Boolean urgente) {
		this.urgente = urgente;
		return this;
	}

	public Boolean getConcluida() {
		return concluida;
	}

	public SolicitacaoPesquisaDTO setConcluida(Boolean concluida) {
		this.concluida = concluida;
		return this;
	}

	public LocalDateTime getDataCriacao() {
		return dataCriacao;
	}

	public SolicitacaoPesquisaDTO setDataCriacao(LocalDateTime dataCriacao) {
		this.dataCriacao = dataCriacao;
		return this;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public SolicitacaoPesquisaDTO setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
		return this;
	}

	public EmpresaCNPJDTO getEmpresa() {
		return empresa;
	}

	public SolicitacaoPesquisaDTO setEmpresa(EmpresaCNPJDTO empresa) {
		this.empresa = empresa;
		return this;
	}

	public List<ItemPesquisaDTO> getItens() {
		return itens;
	}

	public SolicitacaoPesquisaDTO setItens(List<ItemPesquisaDTO> itens) {
		this.itens = itens;
		return this;
	}

}
