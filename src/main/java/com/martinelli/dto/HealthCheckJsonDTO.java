package com.martinelli.dto;

public class HealthCheckJsonDTO {
	
	HealthCheckDTO geralStatus;
	HealthCheckDTO adTest;
	HealthCheckDTO dbTest;
	
	public HealthCheckJsonDTO() {
		super();
	}
	public HealthCheckJsonDTO(HealthCheckDTO geralStatus, HealthCheckDTO adTest, HealthCheckDTO dbTest) {
		super();
		this.geralStatus = geralStatus;
		this.adTest = adTest;
		this.dbTest = dbTest;
	}
	public HealthCheckDTO getGeralStatus() {
		return geralStatus;
	}
	public HealthCheckJsonDTO setGeralStatus(HealthCheckDTO geralStatus) {
		this.geralStatus = geralStatus;
		return this;
	}
	public HealthCheckDTO getAdTest() {
		return adTest;
	}
	public HealthCheckJsonDTO setAdTest(HealthCheckDTO ldapTest) {
		this.adTest = ldapTest;
		return this;
	}
	public HealthCheckDTO getDbTest() {
		return dbTest;
	}
	public HealthCheckJsonDTO setDbTest(HealthCheckDTO dbTest) {
		this.dbTest = dbTest;
		return this;
	}
	
	

}
