package com.martinelli.dto;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.martinelli.utils.deserializers.LocalDateTimeDeserializer;
import com.martinelli.utils.deserializers.LocalDateTimeSerializer;

public class SolicitacaoPesquisaFilterDTO {

	String solicitante;
	String atendente;
	List<String> estado;
	Long idEmpresa;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	LocalDateTime dataInicio;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	LocalDateTime dataFim;

	public SolicitacaoPesquisaFilterDTO(String solicitante, String atendente, List<String> estado, Long idEmpresa,
			long dataInicio, long dataFim) {
		this.solicitante = solicitante;
		this.atendente = atendente;
		this.estado = estado;
		if (dataInicio > 0 && dataFim > 0) {
			this.dataInicio = LocalDateTime	.ofInstant(Instant.ofEpochMilli(dataInicio), ZoneId.systemDefault())
											.withHour(0)
											.withMinute(0);
			this.dataFim = LocalDateTime.ofInstant(Instant.ofEpochMilli(dataFim), ZoneId.systemDefault())
										.withHour(23)
										.withMinute(59);
		}
		this.idEmpresa = idEmpresa;
	}

	public SolicitacaoPesquisaFilterDTO() {
		super();
	}

	public String getSolicitante() {
		return solicitante;
	}

	public SolicitacaoPesquisaFilterDTO setSolicitante(String solicitante) {
		this.solicitante = solicitante;
		return this;
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public SolicitacaoPesquisaFilterDTO setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
		return this;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public SolicitacaoPesquisaFilterDTO setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
		return this;
	}

	public List<String> getEstado() {
		return estado;
	}

	public SolicitacaoPesquisaFilterDTO setEstado(List<String> estado) {
		this.estado = estado;
		return this;
	}

	public Long getIdEmpresa() {
		return idEmpresa;
	}

	public SolicitacaoPesquisaFilterDTO setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
		return this;
	}

	public String getAtendente() {
		return atendente;
	}

	public SolicitacaoPesquisaFilterDTO setAtendente(String atendente) {
		this.atendente = atendente;
		return this;
	}

}
