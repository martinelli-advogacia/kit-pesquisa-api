package com.martinelli.dto;

public class LoginDTO {

	String user;
	String password;

	public LoginDTO() {
		super();
	}

	public LoginDTO(String user, String password) {
		super();
		this.user = user;
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public LoginDTO setUser(String user) {
		this.user = user;
		return this;
	}

	public String getPassword() {
		return password;
	}

	public LoginDTO setPassword(String password) {
		this.password = password;
		return this;
	}

	public boolean isEmpty() {
		return user == null || user.trim().isEmpty() || password == null || password.trim().isEmpty();
	}

}
