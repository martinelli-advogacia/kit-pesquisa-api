package com.martinelli.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.martinelli.utils.deserializers.LocalDateTimeDeserializer;
import com.martinelli.utils.deserializers.LocalDateTimeSerializer;

public class RelatorioFilterDTO {
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	LocalDateTime dataInicio;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	LocalDateTime dataFim;
	String uf;
	String solicitante;
	long page;
	long qtdItens;

	public RelatorioFilterDTO() {
		super();
	}

	public boolean hasSolicitante() {
		return solicitante != null && !solicitante.isEmpty();
	}

	public boolean hasData() {
		return dataInicio != null && dataFim != null && dataInicio.isBefore(dataFim);
	}

	public boolean hasUf() {
		return uf != null && !uf.isEmpty();
	}

	public LocalDateTime getDataInicio() {
		return dataInicio;
	}

	public RelatorioFilterDTO setDataInicio(LocalDateTime dataInicio) {
		this.dataInicio = dataInicio;
		return this;
	}

	public LocalDateTime getDataFim() {
		return dataFim;
	}

	public RelatorioFilterDTO setDataFim(LocalDateTime dataFim) {
		this.dataFim = dataFim;
		return this;
	}

	public String getUf() {
		return uf;
	}

	public RelatorioFilterDTO setUf(String uf) {
		this.uf = uf;
		return this;
	}

	public String getSolicitante() {
		return solicitante;
	}

	public RelatorioFilterDTO setSolicitante(String solicitante) {
		this.solicitante = solicitante;
		return this;
	}

	public long getPage() {
		return page;
	}

	public RelatorioFilterDTO setPage(long page) {
		this.page = page;
		return this;
	}

	public long getQtdItens() {
		return qtdItens;
	}

	public RelatorioFilterDTO setQtdItens(long qtdItens) {
		this.qtdItens = qtdItens;
		return this;
	}

}
