package com.martinelli.dto;


public class ErroResponseDTO {
	String msg;
	int status;
	String stackTrace;

	public ErroResponseDTO() {
		super();
	}

	public String getMsg() {
		return msg;
	}

	public ErroResponseDTO setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public int getStatus() {
		return status;
	}

	public ErroResponseDTO setStatus(int status) {
		this.status = status;
		return this;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public ErroResponseDTO setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
		return this;
	}

}
