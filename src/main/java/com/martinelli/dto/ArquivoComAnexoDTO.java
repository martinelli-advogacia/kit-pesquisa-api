package com.martinelli.dto;

import com.martinelli.model.Arquivo;

public class ArquivoComAnexoDTO {
	long id;
	String fileName;
	String contentType;
	byte[] anexo;

	public ArquivoComAnexoDTO() {
		super();
	}

	public ArquivoComAnexoDTO(Long id, String fileName, String contentType, byte[] anexo) {
		super();
		this.id = id;
		this.fileName = fileName;
		this.contentType = contentType;
		this.anexo = anexo;
	}

	public ArquivoComAnexoDTO(Arquivo arq) {
		super();
		this.id = arq.getId();
		this.fileName = arq.getNomeArquivo();
		this.contentType = arq.getTipoConteudo();
		this.anexo = arq.getAnexo();
	}

	public long getId() {
		return id;
	}

	public ArquivoComAnexoDTO setId(long id) {
		this.id = id;
		return this;
	}

	public String getFileName() {
		return fileName;
	}

	public ArquivoComAnexoDTO setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public String getContentType() {
		return contentType;
	}

	public ArquivoComAnexoDTO setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	public byte[] getAnexo() {
		return anexo;
	}

	public ArquivoComAnexoDTO setAnexo(byte[] anexo) {
		this.anexo = anexo;
		return this;
	}

}
