package com.martinelli.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.martinelli.model.Permissionamento;
import com.martinelli.model.Usuario;
import com.martinelli.utils.deserializers.LocalDateTimeDeserializer;
import com.martinelli.utils.deserializers.LocalDateTimeHoursMinutesSerializer;
import com.martinelli.utils.deserializers.TelefoneDeserializer;
import com.martinelli.utils.deserializers.TelefoneSerializer;

@JsonInclude(Include.NON_NULL)
public class UsuarioDTO {
	String login;
	String nome;
	String email;
	@JsonSerialize(using = TelefoneSerializer.class)
	@JsonDeserialize(using = TelefoneDeserializer.class)
	String telefone;
	String uf;
	String perfil;
	@JsonSerialize(using = LocalDateTimeHoursMinutesSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	LocalDateTime ultimoAcesso;
	boolean selecionarItens;
	boolean vip;

	public UsuarioDTO() {
		super();
	}

	public UsuarioDTO(Usuario usuario) {
		super();
		this.login = usuario.getLogin();
		this.nome = usuario.getNome();
		this.email = usuario.getEmail();
		this.telefone = usuario.getTelefone();
		this.uf = usuario.getUf();
		this.ultimoAcesso = usuario.getUltimoAcesso();
		this.vip = usuario.isVip();
	}

	public UsuarioDTO(Usuario usuario, Permissionamento perfil) {
		super();
		this.login = usuario.getLogin();
		this.nome = usuario.getNome();
		this.email = usuario.getEmail();
		this.telefone = usuario.getTelefone();
		this.uf = usuario.getUf();
		this.ultimoAcesso = usuario.getUltimoAcesso();
		this.perfil = perfil != null ? perfil.getRoleAlowed() : "Solicitante";
		this.selecionarItens = perfil != null ? perfil.getSelecionarItens() : false;
		this.vip = usuario.isVip();
	}

	public UsuarioDTO(String login) {
		super();
		this.login = login;
	}

	public String getLogin() {
		return login;
	}

	public UsuarioDTO setLogin(String login) {
		this.login = login;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public UsuarioDTO setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public UsuarioDTO setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public UsuarioDTO setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public String getPerfil() {
		return perfil;
	}

	public UsuarioDTO setPerfil(String perfil) {
		this.perfil = perfil;
		return this;
	}

	public boolean getSelecionarItens() {
		return selecionarItens;
	}

	public UsuarioDTO setSelecionarItens(boolean selecionarItens) {
		this.selecionarItens = selecionarItens;
		return this;
	}

	public String getUf() {
		return uf;
	}

	public UsuarioDTO setUf(String uf) {
		this.uf = uf;
		return this;
	}

	public LocalDateTime getUltimoAcesso() {
		return ultimoAcesso;
	}

	public UsuarioDTO setUltimoAcesso(LocalDateTime ultimoAcesso) {
		this.ultimoAcesso = ultimoAcesso;
		return this;
	}

	public boolean isVip() {
		return vip;
	}

	public UsuarioDTO setVip(boolean vip) {
		this.vip = vip;
		return this;
	}

}
