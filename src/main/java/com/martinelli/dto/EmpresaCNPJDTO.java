package com.martinelli.dto;

import com.martinelli.model.EmpresaCNPJ;

public class EmpresaCNPJDTO {
	long id;
	String cnpj;
	String nomeEmpresa;
	String uf;

	public EmpresaCNPJDTO(long id, String cnpj, String nomeEmpresa, String uf) {
		super();
		this.id = id;
		this.cnpj = cnpj;
		this.nomeEmpresa = nomeEmpresa;
		this.uf = uf;
	}

	public EmpresaCNPJDTO() {
		super();
	}

	public EmpresaCNPJDTO(EmpresaCNPJ empresa) {
		super();
		this.id = empresa.getId();
		this.cnpj = empresa.getCnpj();
		this.nomeEmpresa = empresa.getNomeEmpresa();
		this.uf = empresa.getUf();
	}

	public long getId() {
		return id;
	}

	public EmpresaCNPJDTO setId(long id) {
		this.id = id;
		return this;
	}

	public String getCnpj() {
		return cnpj;
	}

	public EmpresaCNPJDTO setCnpj(String cnpj) {
		this.cnpj = cnpj;
		return this;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public EmpresaCNPJDTO setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
		return this;
	}

	public String getUf() {
		return uf;
	}

	public EmpresaCNPJDTO setUf(String uf) {
		this.uf = uf;
		return this;
	}

}
