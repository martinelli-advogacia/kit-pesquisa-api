package com.martinelli.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.martinelli.model.ItemPesquisa;
import com.martinelli.utils.deserializers.LocalDateTimeSerializer;

public class ItemPesquisaDTO {

	private long id;
	private TipoItemPesquisaDTO tipo;
	private boolean semResultado;
	private boolean concluido;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	private LocalDateTime dataAtualizacao;
	private UsuarioDTO ultimoAtualizador;
	private ArquivoConsultaDTO arquivo;

	public ItemPesquisaDTO() {
		super();
	}

	public ItemPesquisaDTO(ItemPesquisa item) {
		this.id = item.getId();
		this.tipo = new TipoItemPesquisaDTO(item.getTipo());
		this.semResultado = item.getSemResultado();
		this.concluido = item.getConcluido();
		this.dataAtualizacao = item.getDataAtualizacao();
		if (item.getUltimoAtualizador() != null)
			this.ultimoAtualizador = new UsuarioDTO(item.getUltimoAtualizador());
		this.arquivo = item.getArquivo() != null ? new ArquivoConsultaDTO(item.getArquivo()) : null;
	}

	public long getId() {
		return id;
	}

	public ItemPesquisaDTO setId(long id) {
		this.id = id;
		return this;
	}

	public TipoItemPesquisaDTO getTipo() {
		return tipo;
	}

	public ItemPesquisaDTO setTipo(TipoItemPesquisaDTO tipo) {
		this.tipo = tipo;
		return this;
	}

	public boolean getSemResultado() {
		return semResultado;
	}

	public ItemPesquisaDTO setSemResultado(boolean semResultado) {
		this.semResultado = semResultado;
		return this;
	}

	public boolean getConcluido() {
		return concluido;
	}

	public ItemPesquisaDTO setConcluido(boolean concluido) {
		this.concluido = concluido;
		return this;
	}

	public LocalDateTime getDataAtualizacao() {
		return dataAtualizacao;
	}

	public ItemPesquisaDTO setDataAtualizacao(LocalDateTime dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
		return this;
	}

	public UsuarioDTO getUltimoAtualizador() {
		return ultimoAtualizador;
	}

	public ItemPesquisaDTO setUltimoAtualizador(UsuarioDTO ultimoAtualizador) {
		this.ultimoAtualizador = ultimoAtualizador;
		return this;
	}

	public ArquivoConsultaDTO getArquivo() {
		return arquivo;
	}

	public ItemPesquisaDTO setArquivo(ArquivoConsultaDTO arquivo) {
		this.arquivo = arquivo;
		return this;
	}

}
