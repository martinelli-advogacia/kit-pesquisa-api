package com.martinelli.dto;

public class HealthCheckDTO {
	
	String status;
	String message;
	
	public HealthCheckDTO() {
		super();
	}
	public HealthCheckDTO(String status, String message) {
		super();
		this.status = status;
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public HealthCheckDTO setStatus(String status) {
		this.status = status;
		return this;
	}
	public String getMessage() {
		return message;
	}
	public HealthCheckDTO setMessage(String message) {
		this.message = message;
		return this;
	}

}
