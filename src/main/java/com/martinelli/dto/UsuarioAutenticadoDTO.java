package com.martinelli.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class UsuarioAutenticadoDTO implements Serializable {

	static final long serialVersionUID = 1L;

	int id;
	String nome;
	String cargo;
	String email;
	String imagemBase64;
	String login;
	String cpf;
	String senha;
	String rolesAllowed;
	boolean selecionarItens;
	String uf;
	String distinguishedname;
	String escritorio;
	String telefone;
	String departamento;
	Date passwordChangeDate;
	Date dataNascimento;
	Date dataAdmissao;
	long idLong;

	public UsuarioAutenticadoDTO() {
		super();
	}

	public UsuarioAutenticadoDTO(UsuarioAutenticadoDTO user) {
		super();
		this.id = user.id;
		this.nome = user.nome;
		this.cargo = user.cargo;
		this.email = user.email;
		this.imagemBase64 = user.imagemBase64;
		this.login = user.login;
		this.senha = user.senha;
		this.rolesAllowed = user.rolesAllowed;
		this.distinguishedname = user.distinguishedname;
		this.escritorio = user.escritorio;
		this.telefone = user.telefone;
		this.departamento = user.departamento;
		this.passwordChangeDate = user.passwordChangeDate;
		this.idLong = user.idLong;
		this.dataAdmissao = user.dataAdmissao;
		this.cpf = user.cpf;
		this.dataNascimento = user.dataNascimento;
	}

	public String getRolesAllowed() {
		return rolesAllowed;
	}

	public UsuarioAutenticadoDTO setRolesAllowed(String rolesAlowed) {
		this.rolesAllowed = rolesAlowed;
		return this;
	}

	public long getIdLong() {
		return idLong;
	}

	public String getCpf() {
		return cpf;
	}

	public UsuarioAutenticadoDTO setCpf(String cpf) {
		this.cpf = cpf;
		return this;
	}

	public UsuarioAutenticadoDTO setIdLong(long idLong) {
		this.idLong = idLong;
		return this;
	}

	public int getId() {
		return id;
	}

	public UsuarioAutenticadoDTO setId(int id) {
		this.id = id;
		return this;
	}

	public String getNome() {
		return nome;
	}

	public UsuarioAutenticadoDTO setNome(String nome) {
		this.nome = nome;
		return this;
	}

	public String getCargo() {
		return cargo;
	}

	public UsuarioAutenticadoDTO setCargo(String cargo) {
		this.cargo = cargo;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public UsuarioAutenticadoDTO setEmail(String email) {
		this.email = email;
		return this;
	}

	public String getImagemBase64() {
		return imagemBase64;
	}

	public UsuarioAutenticadoDTO setImagemBase64(String imagemBase64) {
		this.imagemBase64 = imagemBase64;
		return this;
	}

	public String getLogin() {
		return login;
	}

	public UsuarioAutenticadoDTO setLogin(String login) {
		this.login = login;
		return this;
	}

	public String getSenha() {
		return senha;
	}

	public UsuarioAutenticadoDTO setSenha(String senha) {
		this.senha = senha;
		return this;
	}

	public String getDistinguishedname() {
		return distinguishedname;
	}

	public UsuarioAutenticadoDTO setDistinguishedname(String distinguishedname) {
		this.distinguishedname = distinguishedname;
		return this;
	}

	public String getEscritorio() {
		return escritorio;
	}

	public UsuarioAutenticadoDTO setEscritorio(String escritorio) {
		this.escritorio = escritorio;
		return this;
	}

	public String getTelefone() {
		return telefone;
	}

	public UsuarioAutenticadoDTO setTelefone(String telefone) {
		this.telefone = telefone;
		return this;
	}

	public String getDepartamento() {
		return departamento;
	}

	public UsuarioAutenticadoDTO setDepartamento(String departamento) {
		this.departamento = departamento;
		return this;
	}

	public Date getPasswordChangeDate() {
		return passwordChangeDate;
	}

	public UsuarioAutenticadoDTO setPasswordChangeDate(Date passwordChangeDate) {
		this.passwordChangeDate = passwordChangeDate;
		return this;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public UsuarioAutenticadoDTO setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
		return this;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public UsuarioAutenticadoDTO setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
		return this;
	}

	public boolean isSelecionarItens() {
		return selecionarItens;
	}

	public UsuarioAutenticadoDTO setSelecionarItens(boolean selecionarItens) {
		this.selecionarItens = selecionarItens;
		return this;
	}

	public String getUf() {
		return uf;
	}

	public UsuarioAutenticadoDTO setUf(String uf) {
		this.uf = uf;
		return this;
	}
	
	

}
