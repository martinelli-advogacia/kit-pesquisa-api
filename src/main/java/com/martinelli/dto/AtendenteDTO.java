package com.martinelli.dto;

public class AtendenteDTO {

	String atendente;

	public AtendenteDTO() {
		super();
	}

	public AtendenteDTO(String atendente) {
		super();
		this.atendente = atendente;
	}

	public String getAtendente() {
		return atendente;
	}

	public AtendenteDTO setAtendente(String atendente) {
		this.atendente = atendente;
		return this;
	}
}
