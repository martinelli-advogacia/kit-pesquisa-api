package com.martinelli.dto;

import com.martinelli.model.Arquivo;

public class ArquivoConsultaDTO {

	long id;
	String fileName;
	String contentType;
	UsuarioDTO atualizador;

	public ArquivoConsultaDTO() {
		super();
	}

	public ArquivoConsultaDTO(Arquivo arq) {
		super();
		this.id = arq.getId();
		this.fileName = arq.getNomeArquivo();
		this.contentType = arq.getTipoConteudo();
		if (arq.getUltimoAtualizador() != null)
			this.atualizador = new UsuarioDTO(arq.getUltimoAtualizador());
	}

	public long getId() {
		return id;
	}

	public ArquivoConsultaDTO setId(long id) {
		this.id = id;
		return this;
	}

	public String getFileName() {
		return fileName;
	}

	public ArquivoConsultaDTO setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public String getContentType() {
		return contentType;
	}

	public ArquivoConsultaDTO setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	public UsuarioDTO getAtualizador() {
		return atualizador;
	}

	public ArquivoConsultaDTO setAtualizador(UsuarioDTO atualizador) {
		this.atualizador = atualizador;
		return this;
	}

}
