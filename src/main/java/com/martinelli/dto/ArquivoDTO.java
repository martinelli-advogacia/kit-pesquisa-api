package com.martinelli.dto;

import java.util.ArrayList;
import java.util.List;

import com.martinelli.model.Arquivo;
import com.martinelli.model.ItemPesquisa;

public class ArquivoDTO {

	long id;
	String fileName;
	String contentType;
	UsuarioDTO atualizador;
	List<ItemPesquisaDTO> itens;

	public ArquivoDTO() {
		super();
	}

	public ArquivoDTO(Arquivo arq) {
		this.id = arq.getId();
		this.fileName = arq.getNomeArquivo();
		this.contentType = arq.getTipoConteudo();
		if (arq.getUltimoAtualizador() != null)
			this.atualizador = new UsuarioDTO(arq.getUltimoAtualizador());
		this.itens = new ArrayList<>();
		if (arq.getItemPesquisa() != null) {
			for (ItemPesquisa ent : arq.getItemPesquisa()) {
				this.itens.add(new ItemPesquisaDTO(ent));
			}
		}
	}

	public long getId() {
		return id;
	}

	public ArquivoDTO setId(long id) {
		this.id = id;
		return this;
	}

	public String getFileName() {
		return fileName;
	}

	public ArquivoDTO setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public String getContentType() {
		return contentType;
	}

	public ArquivoDTO setContentType(String contentType) {
		this.contentType = contentType;
		return this;
	}

	public UsuarioDTO getAtualizador() {
		return atualizador;
	}

	public ArquivoDTO setAtualizador(UsuarioDTO atualizador) {
		this.atualizador = atualizador;
		return this;
	}

	public List<ItemPesquisaDTO> getItens() {
		return itens;
	}

	public ArquivoDTO setItens(List<ItemPesquisaDTO> itens) {
		this.itens = itens;
		return this;
	}

}
