package com.martinelli.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolicitacaoPorVariasEmpresasDTO {
	List<EmpresaCNPJDTO> empresasCNPJ;
	List<TipoItemPesquisaDTO> listaItems = new ArrayList<>();
	boolean urgente;
	String observacao;

	public SolicitacaoPorVariasEmpresasDTO() {
	}

	public SolicitacaoPorVariasEmpresasDTO(List<EmpresaCNPJDTO> empresaCNPJ, List<TipoItemPesquisaDTO> listaItems,
			Boolean urgente, String observacao) {
		this.empresasCNPJ = empresaCNPJ;
		this.listaItems = listaItems;
		this.urgente = urgente;
		this.observacao = observacao;
	}

	public boolean isValid() {
		if (this.empresasCNPJ == null || this.empresasCNPJ.isEmpty())
			return false;
		if (this.listaItems == null || this.listaItems.isEmpty())
			return false;
		if (this.urgente && (this.observacao == null || this.observacao.isEmpty()))
			return false;
		// retorna verdadeiro apenas se todos os cpnjs possuirem tamanho > 0
		return this.empresasCNPJ.stream().allMatch(e -> !e.getCnpj().isEmpty());
	}

	public List<EmpresaCNPJDTO> getEmpresasCNPJ() {
		return empresasCNPJ;
	}

	public List<TipoItemPesquisaDTO> getListaItems() {
		return listaItems;
	}

	public SolicitacaoPorVariasEmpresasDTO setListaItems(List<TipoItemPesquisaDTO> listaItems) {
		this.listaItems = listaItems;
		return this;
	}

	public boolean getUrgente() {
		return urgente;
	}

	public SolicitacaoPorVariasEmpresasDTO setUrgente(boolean urgente) {
		this.urgente = urgente;
		return this;
	}

	public String getObservacao() {
		return observacao;
	}

	public SolicitacaoPorVariasEmpresasDTO setObservacao(String observacao) {
		this.observacao = observacao;
		return this;
	}

	public SolicitacaoPorVariasEmpresasDTO setEmpresasCNPJ(List<EmpresaCNPJDTO> empresasCNPJ) {
		this.empresasCNPJ = empresasCNPJ;
		return this;
	}

}
