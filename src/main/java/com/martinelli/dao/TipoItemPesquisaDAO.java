package com.martinelli.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.jpa.QueryHints;

import com.martinelli.dto.TipoItemPesquisaDTO;
import com.martinelli.model.TipoItemPesquisa;
import com.martinelli.utils.HibernateUtils;

public class TipoItemPesquisaDAO extends SuperDAO<TipoItemPesquisa, Long> {

	public TipoItemPesquisaDAO() {
		super(TipoItemPesquisa.class);
	}

	public List<TipoItemPesquisaDTO> findSemArquivo(Long idSolicitacao) {
		EntityManager em = HibernateUtils.getEntityManager();
		TypedQuery<TipoItemPesquisa> query = em.createNamedQuery("NQ_BUSCAR_SEM_ARQUIVO", TipoItemPesquisa.class);
		query.setParameter("idSolicitacao", idSolicitacao).setHint(QueryHints.HINT_READONLY, true);
		try {
			List<TipoItemPesquisa> itemPesquisaEnt = query.getResultList();
			List<TipoItemPesquisaDTO> itens = new ArrayList<>();
			for (TipoItemPesquisa itemEnt : itemPesquisaEnt) {
				itens.add(new TipoItemPesquisaDTO(itemEnt));
			}
			return itens;
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
	}

}
