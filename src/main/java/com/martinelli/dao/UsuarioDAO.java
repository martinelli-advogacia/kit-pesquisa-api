package com.martinelli.dao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.jpa.QueryHints;

import com.martinelli.dto.UsuarioDTO;
import com.martinelli.model.Usuario;
import com.martinelli.utils.HibernateUtils;

public class UsuarioDAO extends SuperDAO<Usuario, Long> {

	public UsuarioDAO() {
		super(Usuario.class);
	}

	public Usuario find(String login) {
		EntityManager em = HibernateUtils.getEntityManager();

		try {
			TypedQuery<Usuario> query = em.createNamedQuery("NQ_USUARIO_POR_LOGIN", Usuario.class);
			query.setHint(QueryHints.HINT_READONLY, true).setParameter("login", login);
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

	public UsuarioDTO findComPerfil(String login) {
		EntityManager em = HibernateUtils.getEntityManager();

		try {
			TypedQuery<UsuarioDTO> query = em.createNamedQuery("NQ_USUARIO_POR_LOGIN_PERFIL", UsuarioDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true).setParameter("login", login);
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

	public List<UsuarioDTO> getAllComPerfil() {
		EntityManager em = HibernateUtils.getEntityManager();

		try {
			TypedQuery<UsuarioDTO> query = em.createNamedQuery("NQ_USUARIO_PERMISSAO", UsuarioDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true);
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

	public List<UsuarioDTO> buscaSolicitante(String busca) {
		EntityManager em = HibernateUtils.getEntityManager();

		try {
			TypedQuery<UsuarioDTO> query = em.createNamedQuery("NQ_USUARIO_BUSCA_SOLICITANTE", UsuarioDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true).setParameter("busca", busca);
			List<UsuarioDTO> resultado = query.getResultList();
			resultado.sort(Comparator.comparing(UsuarioDTO::getNome));
			return resultado;
		} catch (NoResultException e) {
			return new ArrayList<>();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

	public List<UsuarioDTO> buscaAtendente() {
		EntityManager em = HibernateUtils.getEntityManager();

		try {
			TypedQuery<UsuarioDTO> query = em.createNamedQuery("NQ_USUARIO_BUSCA_ATENDENTE", UsuarioDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true);
			List<UsuarioDTO> resultado = query.getResultList();
			resultado.sort(Comparator.comparing(UsuarioDTO::getNome));
			return resultado;
		} catch (NoResultException e) {
			return new ArrayList<>();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

}
