package com.martinelli.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.jpa.QueryHints;

import com.martinelli.model.EmpresaCNPJ;
import com.martinelli.utils.HibernateUtils;

public class EmpresaCNPJDAO extends SuperDAO<EmpresaCNPJ, Long> {

	public EmpresaCNPJDAO() {
		super(EmpresaCNPJ.class);
	}

	public Optional<EmpresaCNPJ> findByCNPJ(String cnpj) {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			String sql = "SELECT e from EmpresaCNPJ e " + " WHERE e.cnpj = :cnpj ";

			TypedQuery<EmpresaCNPJ> query = em.createQuery(sql, EmpresaCNPJ.class).setParameter("cnpj", cnpj);
			List<EmpresaCNPJ> list = query.getResultList();
			if (list.isEmpty())
				return Optional.empty();
			else
				return Optional.of(list.get(0));

		} catch (NoResultException e) {
			return Optional.empty();
		} finally {
			em.close();
		}
	}

	public List<EmpresaCNPJ> findByNomeOrCNPJ(String busca) {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			TypedQuery<EmpresaCNPJ> query = em	.createNamedQuery("NQ_EMPRESA_POR_NOME_CNPJ", EmpresaCNPJ.class)
												.setParameter("busca", busca);
			query.setHint(QueryHints.HINT_READONLY, true);
			return query.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<>();
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}

	}
}
