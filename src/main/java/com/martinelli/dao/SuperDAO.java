package com.martinelli.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.validation.Valid;

import com.martinelli.utils.HibernateUtils;
import com.martinelli.utils.StacktraceToString;

//import com.hepta.vagas.util.HibernateUtil;

public class SuperDAO<T, I extends Serializable> {

	private Class<T> persistedClass;

	private Logger LOG;

	public SuperDAO() {

	}

	public SuperDAO(Class<T> persistedClass) {
		this();
		this.persistedClass = persistedClass;
		this.LOG = Logger.getLogger(persistedClass.getCanonicalName());
	}

	public T save(@Valid T entity) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.severe("Ocorreu um erro ao salvar dados: " + StacktraceToString.convert(e));
			throw new Exception(e);
		} finally {
			em.close();
		}
		return entity;
	}

	public T update(@Valid T newEntity) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		T entity = null;
		try {
			em.getTransaction().begin();
			entity = em.merge(newEntity);
			em.getTransaction().commit();
		} catch (Exception e) {
			LOG.severe("Ocorreu um erro ao atualizar os dados: " + StacktraceToString.convert(e));
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return entity;
	}

	public void delete(I id) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			T entity = find(id, em);
			T mergedEntity = em.merge(entity);
			em.remove(mergedEntity);
			em.getTransaction().commit();
		} catch (Exception e) {
			LOG.severe("Ocorreu um erro ao deletar os dados:" + StacktraceToString.convert(e));
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	private T find(I id, EntityManager em) throws Exception {
		return em.find(persistedClass, id);
	}

	public T find(I id) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		T obj = null;
		try {
			obj = em.find(persistedClass, id);
		} catch (Exception e) {
			LOG.severe("Ocorreu um erro ao buscar por id: " + StacktraceToString.convert(e));
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return obj;
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T> getAll() throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		List<T> list = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM " + persistedClass.getName());
			list = query.getResultList();
		} catch (Exception e) {
			LOG.severe("Ocorreu um erro ao buscar todos: " + StacktraceToString.convert(e));
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return list;
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T> listar(Integer initialPage, Integer maxValue) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		List<T> list = new ArrayList<>();

		try {
			StringBuilder sql = new StringBuilder("FROM ");
			sql.append(persistedClass.getName());
			Query query = em.createQuery(sql.toString());
			query.setFirstResult(initialPage - 1);
			query.setMaxResults(maxValue);
			list = query.getResultList();
		} catch (Exception e) {
			LOG.severe("Ocorreu um erro ao buscar dados: " + StacktraceToString.convert(e));
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return list;
	}

	public Long countRegistros() throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		Long result = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT COUNT(obj.id) FROM ");
			sql.append(persistedClass.getName());
			sql.append(" AS obj ");

			Query query = em.createQuery(sql.toString());
			result = (Long) query.getSingleResult();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return result;
	}

	protected Object createHQLQuery(String hql, boolean resultAsList) throws Exception {
		return createHQLQuery(hql, resultAsList, null);
	}

	protected Object createHQLQuery(String hql, boolean resultAsList, Map<String, Object> parameters) throws Exception {
		return createHQLQuery(hql, resultAsList, parameters, null, null);
	}

	protected Object createHQLQuery(String hql, boolean resultAsList, Map<String, Object> parameters, Integer page,
			Integer resultPerPage) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		Object result = new Object();

		try {
			Query query = em.createQuery(hql);

			if (page != null && resultPerPage != null) {
				query.setFirstResult(calculateOffset(page, resultPerPage));
				query.setMaxResults(resultPerPage);
			}

			if (parameters != null) {
				for (Map.Entry<String, Object> entry : parameters.entrySet()) {
					String key = entry.getKey();
					Object value = entry.getValue();

					query.setParameter(key, value);
				}
			}
			if (resultAsList) {
				result = query.getResultList();
			} else {
				try {
					result = query.getSingleResult();
				} catch (NoResultException e) {
					result = null;
				}
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return result;
	}

	private int calculateOffset(int page, int limit) {
		return ((limit * page) - limit);
	}

}
