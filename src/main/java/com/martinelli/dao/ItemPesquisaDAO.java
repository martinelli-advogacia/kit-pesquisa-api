package com.martinelli.dao;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import com.martinelli.model.ItemPesquisa;
import com.martinelli.model.Usuario;
import com.martinelli.utils.HibernateUtils;

public class ItemPesquisaDAO extends SuperDAO<ItemPesquisa, Long> {

	public ItemPesquisaDAO() {
		super(ItemPesquisa.class);
	}

	public boolean setSemResultado(List<ItemPesquisa> itens, Boolean flag, String atualizador) {

		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			Usuario atualizadorUsuario = em.find(Usuario.class, atualizador);
			itens.stream().forEach(item -> {
				ItemPesquisa mergeItem = em.find(ItemPesquisa.class, item.getId());
				mergeItem	.setArquivo(null)
							.setConcluido(flag)
							.setDataAtualizacao(LocalDateTime.now())
							.setSemResultado(flag)
							.setUltimoAtualizador(atualizadorUsuario);
				em.merge(mergeItem);
			});
			em.getTransaction().commit();
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

}
