package com.martinelli.dao;

import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;

import org.hibernate.jpa.QueryHints;

import com.martinelli.model.Permissionamento;
import com.martinelli.utils.HibernateUtils;

public class PermissionamentoDAO extends SuperDAO<Permissionamento, Long> {

	static final Logger LOGGER = Logger.getLogger(PermissionamentoDAO.class.getName());

	public PermissionamentoDAO() {
		super(Permissionamento.class);
	}

	public Permissionamento buscarPorLogin(String login) {
		EntityManager em = HibernateUtils.getEntityManager();
		TypedQuery<Permissionamento> query = em.createNamedQuery("NQ_BUSCAR_POR_LOGIN", Permissionamento.class);
		query.setParameter("login", login).setHint(QueryHints.HINT_READONLY, true);
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		} catch (NonUniqueResultException e) {
			LOGGER.info("login duplicado no banco de dados - " + login + " -");
			return null;
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
		} finally {
			em.close();
		}
		return null;

	}

}
