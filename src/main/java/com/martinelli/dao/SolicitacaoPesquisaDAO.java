package com.martinelli.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.validation.Valid;

import org.hibernate.jpa.QueryHints;

import com.martinelli.dto.RelatorioFilterDTO;
import com.martinelli.dto.ResultadoPaginado;
import com.martinelli.dto.SolicitacaoPesquisaConsultaDTO;
import com.martinelli.dto.SolicitacaoPesquisaFilterDTO;
import com.martinelli.dto.UsuarioDTO;
import com.martinelli.enums.StatusPesquisaEnum;
import com.martinelli.model.Arquivo;
import com.martinelli.model.EmpresaCNPJ;
import com.martinelli.model.ItemPesquisa;
import com.martinelli.model.SolicitacaoPesquisa;
import com.martinelli.model.Usuario;
import com.martinelli.utils.HibernateUtils;
import com.martinelli.utils.exceptions.SolicitacaoNaoConcluidaException;

public class SolicitacaoPesquisaDAO extends SuperDAO<SolicitacaoPesquisa, Long> {
	private Logger LOG;

	public SolicitacaoPesquisaDAO() {
		super(SolicitacaoPesquisa.class);
		this.LOG = Logger.getLogger(SolicitacaoPesquisaDAO.class.getCanonicalName());
	}

	public SolicitacaoPesquisa saveComItens(@Valid SolicitacaoPesquisa solicitacaoPesquisa, List<ItemPesquisa> itens)
			throws Exception {
		LOG.info("Abrindo EntityManeger");
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			// TODO existem formas mais elegantes de fazer isso, quando tivermos mais tempo
			// vale trocar para uma
			// versão que promova mais consistencia ->
			// http://meri-stuff.blogspot.com/2012/03/jpa-tutorial.html#RelationshipsBidirectionalOneToManyManyToOneConsistency
			if (solicitacaoPesquisa.getEmpresa().getId() != 0L) {
				Long empresaID = solicitacaoPesquisa.getEmpresa().getId();
				EmpresaCNPJ empresaSalva = em.find(EmpresaCNPJ.class, empresaID);
				solicitacaoPesquisa.setEmpresa(empresaSalva);
			}
			solicitacaoPesquisa.setStatus(StatusPesquisaEnum.ABERTO.getName());
			em.persist(solicitacaoPesquisa);
			itens.forEach(i -> {
				i.setSolicitacaoPesquisa(solicitacaoPesquisa);
				em.persist(i);
			});
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.severe("Ocorreu um erro ao salvar dados");
			throw e;
		} finally {
			LOG.info("Fechando EntityManeger");
			em.close();
		}
		return solicitacaoPesquisa;
	}

	public ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> buscarPesquisasSolicitante(
			SolicitacaoPesquisaFilterDTO filtros, Long qtdItens, long page) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		List<SolicitacaoPesquisaConsultaDTO> listaSolicitacoes = new ArrayList<>();
		ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> res = new ResultadoPaginado<>();
		try {
			// Query com os dados de resultado
			StringBuilder hql = new StringBuilder("SELECT new com.martinelli.dto.SolicitacaoPesquisaConsultaDTO(sp) ");

			// Corpo da query a ser gerado dinamicamente e utilizado tanto em contagem e na
			// extração de dados.
			StringBuilder hqlBody = new StringBuilder();
			hqlBody.append(" FROM SolicitacaoPesquisa sp ");
			hqlBody.append(" LEFT JOIN sp.solicitante so ");
			hqlBody.append(" WHERE so.login = :solicitante ");
			if (filtros.getDataFim() != null && filtros.getDataInicio() != null)
				hqlBody.append(" AND sp.dataCriacao BETWEEN :dtini AND :dtfim ");
			if (!filtros.getEstado().isEmpty()) {
				hqlBody.append(" AND sp.status IN ( ");
				List<String> estados = new ArrayList<>();
				filtros.getEstado().stream().forEach(s -> {
					StatusPesquisaEnum status = StatusPesquisaEnum.fromString(s);
					if (status.equals(StatusPesquisaEnum.ABERTO)) {
						estados.add(StatusPesquisaEnum.ABERTO.getName());
					}
					if (status.equals(StatusPesquisaEnum.EM_ATENDIMENTO)) {
						estados.add(StatusPesquisaEnum.EM_ATENDIMENTO.getName());
					}
					if (status.equals(StatusPesquisaEnum.CONCLUIDA))
						estados.add(StatusPesquisaEnum.CONCLUIDA.getName());
				});
				for (int i = 0; i < estados.size(); i++) {
					if (i == 0)
						hqlBody.append("\'" + estados.get(i) + "\'");
					else
						hqlBody.append(", \'" + estados.get(i) + "\'");
				}
				hqlBody.append(") ");
			}
			if (filtros.getIdEmpresa() != null)
				hqlBody.append(" AND sp.empresa.id = :empresa ");

			// Complemento da query principal
			hql.append(hqlBody);
			hql.append("ORDER BY sp.dataCriacao DESC");

			TypedQuery<SolicitacaoPesquisaConsultaDTO> query = em.createQuery(hql.toString(),
					SolicitacaoPesquisaConsultaDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true).setParameter("solicitante", filtros.getSolicitante());
			if (filtros.getDataFim() != null && filtros.getDataInicio() != null)
				query.setParameter("dtini", filtros.getDataInicio()).setParameter("dtfim", filtros.getDataFim());
			if (filtros.getIdEmpresa() != null)
				query.setParameter("empresa", filtros.getIdEmpresa());

			Double maxResult = 0D;
			Double maxPages = 0D;
			if (qtdItens > 0 && page > 0) {

				// Query para a contagem de itens
				StringBuilder hqlCount = new StringBuilder("SELECT COUNT(sp) ");
				hqlCount.append(hqlBody);

				TypedQuery<Long> queryCount = em.createQuery(hqlCount.toString(), Long.class);
				queryCount	.setHint(QueryHints.HINT_READONLY, true)
							.setParameter("solicitante", filtros.getSolicitante());
				if (filtros.getDataFim() != null && filtros.getDataInicio() != null)
					queryCount	.setParameter("dtini", filtros.getDataInicio())
								.setParameter("dtfim", filtros.getDataFim());
				if (filtros.getIdEmpresa() != null)
					queryCount.setParameter("empresa", filtros.getIdEmpresa());

				// Preparações da paginação
				maxResult = queryCount.getSingleResult().doubleValue();
				maxPages = (maxResult / qtdItens);
				maxPages = maxPages.doubleValue() > new Double(maxPages.toString()).longValue() ? maxPages + 1
						: maxPages;
				if (page > 1) {
					Long start = (page - 1) * qtdItens;
					query.setFirstResult(start.intValue());
				}
				if (page >= 1) {
					query.setMaxResults(qtdItens.intValue());
				}
			}
			listaSolicitacoes = query.getResultList();

			// preparação do resultado
			res	.setTotalItens(maxResult.longValue())
				.setMaxPages(maxPages.longValue())
				.setQtdItens(qtdItens)
				.setCurrentPage(page)
				.setResult(listaSolicitacoes);
			return res;
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}

	}

	public List<SolicitacaoPesquisaConsultaDTO> buscarPesquisasTodas() throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		List<SolicitacaoPesquisaConsultaDTO> listaSolicitacoes = new ArrayList<>();
		try {
			TypedQuery<SolicitacaoPesquisaConsultaDTO> query = em.createNamedQuery("NQ_BUSCA_PESQUISAS",
					SolicitacaoPesquisaConsultaDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true);
			listaSolicitacoes = query.getResultList();
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
		return listaSolicitacoes;
	}

	public SolicitacaoPesquisaConsultaDTO atualizaAtendente(Long id, String atendente) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			SolicitacaoPesquisa sol = em.find(SolicitacaoPesquisa.class, id);
			Usuario atendenteUsuario = em.find(Usuario.class, atendente);
			if (sol.getPesquisadorResponsavel() != null
					? sol.getPesquisadorResponsavel().getLogin().equalsIgnoreCase(atendente)
					: false) {
				sol	.setPesquisadorResponsavel(null)
					.setStatus(StatusPesquisaEnum.ABERTO.getName())
					.setDataAtualizacao(LocalDateTime.now());
			} else {
				sol	.setPesquisadorResponsavel(atendenteUsuario)
					.setStatus(StatusPesquisaEnum.EM_ATENDIMENTO.getName())
					.setDataAtualizacao(LocalDateTime.now());
			}
			sol = em.merge(sol);
			em.getTransaction().commit();
			return new SolicitacaoPesquisaConsultaDTO(sol);
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.severe("Ocorreu um erro ao atualizar dados");
			throw e;
		} finally {
			em.close();
		}
	}

	public SolicitacaoPesquisaConsultaDTO concluirSolicitacao(Long id, String atendente) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			SolicitacaoPesquisa sol = em.find(SolicitacaoPesquisa.class, id);
			Usuario atendenteUsuario = em.find(Usuario.class, atendente);
			sol	.setPesquisadorResponsavel(atendenteUsuario)
				.setStatus(StatusPesquisaEnum.CONCLUIDA.getName())
				.setDataAtualizacao(LocalDateTime.now())
				.setConcluida(true);
			sol = em.merge(sol);
			em.getTransaction().commit();
			return new SolicitacaoPesquisaConsultaDTO(sol);
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.severe("Ocorreu um erro ao atualizar dados");
			throw e;
		} finally {
			em.close();
		}
	}

	@Override
	public SolicitacaoPesquisa find(Long id) {
		EntityManager em = HibernateUtils.getEntityManager();
		SolicitacaoPesquisa solicitacao = null;
		try {
			TypedQuery<SolicitacaoPesquisa> query = em.createNamedQuery("NQ_FIND_PESQUISA_BY_ID",
					SolicitacaoPesquisa.class);
			query.setHint(QueryHints.HINT_READONLY, true).setParameter("id", id);
			solicitacao = query.getSingleResult();
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
		return solicitacao;
	}

	public List<SolicitacaoPesquisaConsultaDTO> buscarPesquisasNaoConcluidas() throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		List<SolicitacaoPesquisaConsultaDTO> listaSolicitacoes = new ArrayList<>();
		try {
			TypedQuery<SolicitacaoPesquisaConsultaDTO> query = em.createNamedQuery("NQ_BUSCA_PESQUISAS_NAO_CONCLUIDAS",
					SolicitacaoPesquisaConsultaDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true);
			listaSolicitacoes = query.getResultList();
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
		return listaSolicitacoes;
	}

	public ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> buscarPesquisasConcluidas(
			SolicitacaoPesquisaFilterDTO filtros, Long qtdItens, long page) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		List<SolicitacaoPesquisaConsultaDTO> listaSolicitacoes = new ArrayList<>();
		ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> res = new ResultadoPaginado<>();
		try {
			StringBuilder hql = new StringBuilder("SELECT new com.martinelli.dto.SolicitacaoPesquisaConsultaDTO(sp) ");

			StringBuilder hqlBody = new StringBuilder("FROM SolicitacaoPesquisa sp ");
			hqlBody.append(" LEFT JOIN sp.solicitante so ");
			hqlBody.append(" LEFT JOIN sp.pesquisadorResponsavel pr ");
			hqlBody.append(
					"WHERE (sp.concluida = true OR sp.status = \'" + StatusPesquisaEnum.CONCLUIDA.getName() + "\') ");
			if (filtros.getSolicitante() != null)
				hqlBody.append(" AND so.login = :solicitante ");
			if (filtros.getAtendente() != null)
				hqlBody.append(" AND pr.login = :atendente ");
			if (filtros.getDataFim() != null && filtros.getDataInicio() != null)
				hqlBody.append(" AND sp.dataAtualizacao BETWEEN :dtini AND :dtfim ");
			if (filtros.getIdEmpresa() != null)
				hqlBody.append(" AND sp.empresa.id = :empresa ");

			hql.append(hqlBody);
			hql.append("ORDER BY sp.dataAtualizacao DESC");

			TypedQuery<SolicitacaoPesquisaConsultaDTO> query = em.createQuery(hql.toString(),
					SolicitacaoPesquisaConsultaDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true);
			if (filtros.getSolicitante() != null)
				query.setParameter("solicitante", filtros.getSolicitante());
			if (filtros.getAtendente() != null)
				query.setParameter("atendente", filtros.getAtendente());
			if (filtros.getDataFim() != null && filtros.getDataInicio() != null)
				query.setParameter("dtini", filtros.getDataInicio()).setParameter("dtfim", filtros.getDataFim());
			if (filtros.getIdEmpresa() != null)
				query.setParameter("empresa", filtros.getIdEmpresa());

			Double maxResult = 0D;
			Double maxPages = 0D;
			if (qtdItens > 0 && page > 0) {

				// Query para a contagem de itens
				StringBuilder hqlCount = new StringBuilder("SELECT COUNT(sp) ");
				hqlCount.append(hqlBody);

				TypedQuery<Long> queryCount = em.createQuery(hqlCount.toString(), Long.class);
				queryCount.setHint(QueryHints.HINT_READONLY, true);
				if (filtros.getSolicitante() != null)
					queryCount.setParameter("solicitante", filtros.getSolicitante());
				if (filtros.getAtendente() != null)
					queryCount.setParameter("atendente", filtros.getAtendente());
				if (filtros.getDataFim() != null && filtros.getDataInicio() != null)
					queryCount	.setParameter("dtini", filtros.getDataInicio())
								.setParameter("dtfim", filtros.getDataFim());
				if (filtros.getIdEmpresa() != null)
					queryCount.setParameter("empresa", filtros.getIdEmpresa());

				// Preparações da paginação
				maxResult = queryCount.getSingleResult().doubleValue();
				maxPages = (maxResult / qtdItens);
				maxPages = maxPages.doubleValue() > new Double(maxPages.toString()).longValue() ? maxPages + 1
						: maxPages;
				if (page > 1) {
					Long start = (page - 1) * qtdItens;
					query.setFirstResult(start.intValue());
				}
				if (page >= 1) {
					query.setMaxResults(qtdItens.intValue());
				}
			}
			listaSolicitacoes = query.getResultList();

			// preparação do resultado
			res	.setTotalItens(maxResult.longValue())
				.setMaxPages(maxPages.longValue())
				.setQtdItens(qtdItens)
				.setCurrentPage(page)
				.setResult(listaSolicitacoes);
			return res;
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
	}

	@Override
	public void delete(Long id) {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			SolicitacaoPesquisa sol = em.find(SolicitacaoPesquisa.class, id);
			List<Arquivo> listaArquivos = sol	.getItens()
												.stream()
												.filter(i -> i.getArquivo() != null)
												.map(i -> i.getArquivo())
												.collect(Collectors.toList());

			Query queryItemPesquisa = em.createNamedQuery("NQ_DELETE_ITEM_PELA_SOLICITACAO");
			Query querySolicitacaoPesquisa = em.createNamedQuery("NQ_DELETAR_SOLICITACAO");

			queryItemPesquisa.setParameter("idSolicitacao", id);
			querySolicitacaoPesquisa.setParameter("id", id);

			em.getTransaction().begin();
			queryItemPesquisa.executeUpdate();
			listaArquivos.stream().forEach(a -> em.remove(a));
			querySolicitacaoPesquisa.executeUpdate();
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	public Set<UsuarioDTO> buscarSolicitantes() {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			TypedQuery<SolicitacaoPesquisa> query = em.createNamedQuery("NQ_BUSCA_PESQUISAS_CONCLUIDAS",
					SolicitacaoPesquisa.class);
			Set<UsuarioDTO> solicitantes = new HashSet<>();
			List<SolicitacaoPesquisa> solicitacoes = query.getResultList();
			for (SolicitacaoPesquisa sp : solicitacoes) {
				solicitantes.add(new UsuarioDTO(sp.getSolicitante()));
			}
			return solicitantes;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	public ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> buscarPesquisasSolicitante(
			RelatorioFilterDTO filtros) {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			StringBuilder hql = new StringBuilder("SELECT new com.martinelli.dto.SolicitacaoPesquisaConsultaDTO(sp) ");
			StringBuilder hqlBody = new StringBuilder(" FROM SolicitacaoPesquisa sp ");
			hqlBody.append(" LEFT JOIN sp.solicitante so ");
			hqlBody.append(
					" WHERE (sp.concluida = true OR sp.status = \'" + StatusPesquisaEnum.CONCLUIDA.getName() + "\') ");
			if (filtros.hasSolicitante())
				hqlBody.append(" AND so.login = :solicitante ");
			if (filtros.hasData())
				hqlBody.append(" AND sp.dataAtualizacao BETWEEN :dtini AND :dtfim ");
			if (filtros.hasUf())
				hqlBody.append(" AND so.uf = :uf ");

			hql.append(hqlBody);
			hql.append(" ORDER BY sp.dataAtualizacao DESC ");

			TypedQuery<SolicitacaoPesquisaConsultaDTO> query = em.createQuery(hql.toString(),
					SolicitacaoPesquisaConsultaDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true);
			if (filtros.hasSolicitante())
				query.setParameter("solicitante", filtros.getSolicitante());
			if (filtros.hasData())
				query.setParameter("dtini", filtros.getDataInicio()).setParameter("dtfim", filtros.getDataFim());
			if (filtros.hasUf())
				query.setParameter("uf", filtros.getUf());

			return paginacao(filtros, em, hqlBody, query);
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
	}

	protected ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> paginacao(RelatorioFilterDTO filtros,
			EntityManager em, StringBuilder hqlBody, TypedQuery<SolicitacaoPesquisaConsultaDTO> query) {
		List<SolicitacaoPesquisaConsultaDTO> listaSolicitacoes;
		ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> res = new ResultadoPaginado<>();
		Double maxResult = 0D;
		Double maxPages = 0D;
		long page = filtros.getPage();
		long qtdItens = filtros.getQtdItens();
		if (qtdItens > 0 && page > 0) {

			// Query para a contagem de itens
			StringBuilder hqlCount = new StringBuilder("SELECT COUNT(sp) ");
			hqlCount.append(hqlBody);

			TypedQuery<Long> queryCount = em.createQuery(hqlCount.toString(), Long.class);
			queryCount.setHint(QueryHints.HINT_READONLY, true);
			if (filtros.hasSolicitante())
				queryCount.setParameter("solicitante", filtros.getSolicitante());
			if (filtros.hasData())
				queryCount.setParameter("dtini", filtros.getDataInicio()).setParameter("dtfim", filtros.getDataFim());
			if (filtros.hasUf())
				queryCount.setParameter("uf", filtros.getUf());

			// Preparações da paginação
			maxResult = queryCount.getSingleResult().doubleValue();
			maxPages = (maxResult / qtdItens);
			maxPages = maxPages.doubleValue() > Double.valueOf(maxPages.toString()).longValue() ? maxPages + 1
					: maxPages;
			if (page > 1) {
				Long start = (page - 1) * qtdItens;
				query.setFirstResult(start.intValue());
			}
			if (page >= 1) {
				query.setMaxResults((int) qtdItens);
			}
		}
		listaSolicitacoes = query.getResultList();

		// preparação do resultado
		res	.setTotalItens(maxResult.longValue())
			.setMaxPages(maxPages.longValue())
			.setQtdItens(qtdItens)
			.setCurrentPage(page)
			.setResult(listaSolicitacoes);
		return res;
	}

	public List<SolicitacaoPesquisaConsultaDTO> buscaListaPesquisasConcluidasRelatorio(RelatorioFilterDTO filtros) {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			StringBuilder hql = new StringBuilder("SELECT new com.martinelli.dto.SolicitacaoPesquisaConsultaDTO(sp) ");

			StringBuilder hqlBody = new StringBuilder("FROM SolicitacaoPesquisa sp ");
			hqlBody.append(" LEFT JOIN sp.solicitante so ");
			hqlBody.append(
					"WHERE (sp.concluida = true OR sp.status = \'" + StatusPesquisaEnum.CONCLUIDA.getName() + "\') ");
			if (filtros.hasSolicitante())
				hqlBody.append(" AND so.login = :solicitante ");
			if (filtros.hasData())
				hqlBody.append(" AND sp.dataAtualizacao BETWEEN :dtini AND :dtfim ");
			if (filtros.hasUf())
				hqlBody.append(" AND so.uf = :uf ");

			hql.append(hqlBody);
			hql.append("ORDER BY sp.dataAtualizacao DESC");

			TypedQuery<SolicitacaoPesquisaConsultaDTO> query = em.createQuery(hql.toString(),
					SolicitacaoPesquisaConsultaDTO.class);
			query.setHint(QueryHints.HINT_READONLY, true);
			if (filtros.hasSolicitante())
				query.setParameter("solicitante", filtros.getSolicitante());
			if (filtros.hasData())
				query.setParameter("dtini", filtros.getDataInicio()).setParameter("dtfim", filtros.getDataFim());
			if (filtros.hasUf())
				query.setParameter("uf", filtros.getUf());

			return query.getResultList();

		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
	}

	public SolicitacaoPesquisaConsultaDTO reverterSolicitacao(long id) throws Exception{
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			SolicitacaoPesquisa sol = em.find(SolicitacaoPesquisa.class, id);
			if(!sol.getConcluida())
				throw new SolicitacaoNaoConcluidaException();
			sol	.setStatus(StatusPesquisaEnum.EM_ATENDIMENTO.getName())
				.setDataAtualizacao(LocalDateTime.now())
				.setConcluida(false);
			sol = em.merge(sol);
			em.getTransaction().commit();
			return new SolicitacaoPesquisaConsultaDTO(sol);
		} catch (Exception e) {
			em.getTransaction().rollback();
			LOG.severe("Ocorreu um erro ao atualizar dados");
			throw e;
		} finally {
			em.close();
		}
	}

}
