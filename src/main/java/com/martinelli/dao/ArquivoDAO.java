package com.martinelli.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.jpa.QueryHints;

import com.martinelli.dto.ArquivoDTO;
import com.martinelli.dto.ItemPesquisaDTO;
import com.martinelli.model.Arquivo;
import com.martinelli.model.ItemPesquisa;
import com.martinelli.model.Usuario;
import com.martinelli.utils.HibernateUtils;

public class ArquivoDAO extends SuperDAO<Arquivo, Long> {

	public ArquivoDAO() {
		super(Arquivo.class);
	}

	public ArquivoDTO saveComItens(Arquivo arquivo, List<ItemPesquisa> itens) {
		EntityManager em = HibernateUtils.getEntityManager();
		ArquivoDTO result = null;
		try {
			em.getTransaction().begin();
			em.persist(arquivo);
			List<ItemPesquisaDTO> itensDTO = new ArrayList<>();
			itens.stream().forEach(item -> {
				item.setConcluido(true)
					.setArquivo(arquivo)
					.setDataAtualizacao(LocalDateTime.now())
					.setUltimoAtualizador(arquivo.getUltimoAtualizador());
				em.merge(item);
				itensDTO.add(new ItemPesquisaDTO(item));
			});
			em.getTransaction().commit();
			result = new ArquivoDTO(arquivo);
			result.setItens(itensDTO);
			return result;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}

	}

	public List<Arquivo> findTodosSolicitacao(Long idSolicitacao) throws Exception {
		EntityManager em = HibernateUtils.getEntityManager();
		TypedQuery<Arquivo> query = em.createNamedQuery("NQ_BUSCAR_POR_SOLICITACAO", Arquivo.class);
		query.setParameter("idSolicitacao", idSolicitacao).setHint(QueryHints.HINT_READONLY, true);
		try {
			return query.getResultList();
		} catch (Exception e) {
			throw e;
		} finally {
			em.close();
		}
	}

	public void delete(Long id, String atualizador) {
		EntityManager em = HibernateUtils.getEntityManager();
		try {
			em.getTransaction().begin();
			Arquivo arquivo = em.find(Arquivo.class, id);
			Usuario atualizadorUsuario = em.find(Usuario.class, atualizador);
			arquivo.getItemPesquisa().stream().forEach(item -> {
				item.setConcluido(false)
					.setArquivo(null)
					.setDataAtualizacao(LocalDateTime.now())
					.setUltimoAtualizador(atualizadorUsuario);
				em.merge(item);
			});
			em.remove(arquivo);
			em.getTransaction().commit();

		} catch (Exception e) {
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

}
