package com.martinelli;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.martinelli.filter.CorsFilter;
import com.martinelli.security.jwt.filter.AuthenticationFilter;
import com.martinelli.security.jwt.filter.AuthorizationFilter;
import com.martinelli.security.jwt.filter.ResponseFilter;

@ApplicationPath("/")
public class APIConfig extends ResourceConfig {
	
	public APIConfig() {
		packages("com.martinelli.rest");
		register(CorsFilter.class);
		register(AuthenticationFilter.class);
		register(AuthorizationFilter.class);
		register(ResponseFilter.class);
		register(MultiPartFeature.class);
	}

}
