package com.martinelli.utils;

import java.io.Serializable;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class LDAPConnector implements Serializable {
	static final long serialVersionUID = 1L;

	public static LdapContext openConnection(String username, String password, String server, String porta)
			throws NamingException {
		Hashtable<String, Object> env = new Hashtable<>();
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		if (username != null) {
			env.put(Context.SECURITY_PRINCIPAL, username);
		}
		if (password != null) {
			env.put(Context.SECURITY_CREDENTIALS, password);
		}
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		
		if(porta == null)
			env.put(Context.PROVIDER_URL, server);
		else
			env.put(Context.PROVIDER_URL, server + ":" + porta);
		
		env.put("java.naming.ldap.attributes.binary", "objectSID");
		env.put("java.naming.ldap.attributes.binary", "objectGUID");
		return new InitialLdapContext(env, null);
	}
	
	public static LdapContext openConnection(String username, String password, String server) throws NamingException {
		return openConnection(username,password,server,null);
	}

	public static void closeConnection(LdapContext ctx) throws NamingException {
		ctx.close();
	}

}
