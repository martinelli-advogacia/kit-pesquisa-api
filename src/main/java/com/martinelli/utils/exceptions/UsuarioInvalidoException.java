package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class UsuarioInvalidoException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public UsuarioInvalidoException() {
		super("Usuário Invalido.", Status.INTERNAL_SERVER_ERROR);
	}
}
