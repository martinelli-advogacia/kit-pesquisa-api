package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class HeptaException extends Exception {

	static final long serialVersionUID = 1L;
	Status status = Status.BAD_REQUEST;

	public HeptaException(String msg, Status status) {
		super(msg);
		this.status = status;
	}

	public HeptaException() {
		super();
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
