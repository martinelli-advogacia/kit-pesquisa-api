package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class FalhaAtualizarItemPesquisa extends HeptaException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1096183959087030480L;

	public FalhaAtualizarItemPesquisa() {
		super("Falha ao atualizar item da pesquisa", Status.BAD_REQUEST);
	}

}
