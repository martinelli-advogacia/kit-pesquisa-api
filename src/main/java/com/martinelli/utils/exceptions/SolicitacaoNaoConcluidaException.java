package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class SolicitacaoNaoConcluidaException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public SolicitacaoNaoConcluidaException() {
		super("Solicitação não foi concluida ainda.", Status.UNAUTHORIZED);
	}
}
