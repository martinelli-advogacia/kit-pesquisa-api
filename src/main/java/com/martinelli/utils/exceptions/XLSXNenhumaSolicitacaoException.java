package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class XLSXNenhumaSolicitacaoException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public XLSXNenhumaSolicitacaoException() {
		super("Nenhuma solicitacao existente nesse periodo escolhido.", Status.EXPECTATION_FAILED);
	}
}
