package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class SolicitacaoInexistenteException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public SolicitacaoInexistenteException() {
		super("Solicitação não existe.", Status.NOT_FOUND);
	}
}