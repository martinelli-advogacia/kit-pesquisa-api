package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class ArquivoInexistenteException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public ArquivoInexistenteException() {
		super("Arquivo não existe.", Status.NOT_FOUND);
	}
}
