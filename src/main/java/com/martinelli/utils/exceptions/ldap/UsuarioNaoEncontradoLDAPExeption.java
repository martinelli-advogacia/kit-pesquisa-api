package com.martinelli.utils.exceptions.ldap;

public class UsuarioNaoEncontradoLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public UsuarioNaoEncontradoLDAPExeption() {
		super("Erro 525: Usuário não encontrado.");
	}


}
