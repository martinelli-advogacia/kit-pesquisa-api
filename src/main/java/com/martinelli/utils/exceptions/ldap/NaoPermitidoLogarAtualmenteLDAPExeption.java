package com.martinelli.utils.exceptions.ldap;

public class NaoPermitidoLogarAtualmenteLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public NaoPermitidoLogarAtualmenteLDAPExeption() {
		super("Erro 530: Não é permitido logar nesse momento.");
	}


}
