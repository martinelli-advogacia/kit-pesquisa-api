package com.martinelli.utils.exceptions.ldap;

public class ContaTrancadaLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public ContaTrancadaLDAPExeption() {
		super("Erro 775: Conta de usuário trancada.");
	}


}
