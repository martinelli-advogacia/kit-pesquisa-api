package com.martinelli.utils.exceptions.ldap;

public class NaoPermitidoLogarWorkstationLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public NaoPermitidoLogarWorkstationLDAPExeption() {
		super("Erro 532: Senha expirada.");
	}


}
