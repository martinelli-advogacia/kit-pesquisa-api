package com.martinelli.utils.exceptions.ldap;

public class CredenciaisInvalidasLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public CredenciaisInvalidasLDAPExeption() {
		super("Erro 52e: Credenciais inválidas.");
	}


}
