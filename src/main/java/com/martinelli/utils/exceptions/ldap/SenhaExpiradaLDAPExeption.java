package com.martinelli.utils.exceptions.ldap;

public class SenhaExpiradaLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public SenhaExpiradaLDAPExeption() {
		super("Erro 532: Senha expirada.");
	}


}
