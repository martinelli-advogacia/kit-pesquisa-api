package com.martinelli.utils.exceptions.ldap;

import java.util.Collection;

public class BadArgumentException extends Exception {

	Collection<String> messages;

	static final long serialVersionUID = 1L;

	public BadArgumentException(Collection<String> message) {
		super();
		this.messages = message;
	}

	public BadArgumentException(String msg) {
		super(msg);
	}

	@Override
	public String getMessage() {
		String msg;

		if (this.messages != null && !this.messages.isEmpty()) {
			msg = "[";

			for (String message : this.messages) {
				msg += message + ",";
			}

			msg.substring(0, msg.length() - 1);
			msg += ']';

		} else {
			msg = super.getMessage();
		}

		return msg;
	}

}
