package com.martinelli.utils.exceptions.ldap;

public class ContaDesativadaLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public ContaDesativadaLDAPExeption() {
		super("Erro 533: Conta desativada");
	}


}
