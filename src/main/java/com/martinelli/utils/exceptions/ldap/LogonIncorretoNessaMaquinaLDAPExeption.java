package com.martinelli.utils.exceptions.ldap;

public class LogonIncorretoNessaMaquinaLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public LogonIncorretoNessaMaquinaLDAPExeption() {
		super("Erro 534: O usuário não tem o tipo de logon correto nessa maquina.");
	}


}
