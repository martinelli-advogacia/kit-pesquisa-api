package com.martinelli.utils.exceptions.ldap;

public class PrecisaResetarSenhaLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public PrecisaResetarSenhaLDAPExeption() {
		super("Erro 773: Usuário precisa resetar a senha.");
	}


}
