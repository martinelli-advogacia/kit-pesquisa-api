package com.martinelli.utils.exceptions.ldap;

public class ErroDesconhecidoLDAPException extends Exception {
	static final long serialVersionUID = 1L;

	public ErroDesconhecidoLDAPException() {
		super("Erro desconhecido, endereço, usuário ou senha inválidos");
	}

}
