package com.martinelli.utils.exceptions.ldap;

public class ContaExpiradaLDAPExeption extends Exception {
	static final long serialVersionUID = 1L;

	public ContaExpiradaLDAPExeption() {
		super("Erro 701: Conta expirada.");
	}


}
