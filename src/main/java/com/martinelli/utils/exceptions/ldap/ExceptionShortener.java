package com.martinelli.utils.exceptions.ldap;

import java.util.Stack;

public class ExceptionShortener {
	public static Exception truncate(Exception rt) {
		Stack<Throwable> causeStack = new Stack<>();

		Throwable currentEx = rt;
		while (currentEx != null) {
			causeStack.push(currentEx);
			currentEx = currentEx.getCause();
		}

		Exception newEx = null;
		while (!causeStack.isEmpty()) {
			Throwable t = causeStack.pop();
			newEx = new RuntimeException(truncateMessage(t.getMessage()), newEx);
			newEx.setStackTrace(t.getStackTrace());
		}

		return newEx;
	}

	public static String truncateMessage(String message) {
		if (message.length() < 25) {
			return message;
		}

		return message.substring(0, 25) + "...";
	}
}
