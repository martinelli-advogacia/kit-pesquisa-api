package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class SolicitacaoConcluidaException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public SolicitacaoConcluidaException() {
		super("Solicitação já foi concluida, não é possível editar seus dados.", Status.UNAUTHORIZED);
	}
}
