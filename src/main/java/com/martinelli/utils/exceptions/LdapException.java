package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class LdapException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public LdapException(String msg) {
		super(msg, Status.BAD_REQUEST);
	}
}
