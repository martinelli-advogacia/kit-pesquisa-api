package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class SolicitacaoPesquisaInvalidaException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public SolicitacaoPesquisaInvalidaException() {
		super("Solicitação inválida, verifique se os dados da empresa, items de pesquisa e observação estão corretos",
				Status.BAD_REQUEST);
	}
}
