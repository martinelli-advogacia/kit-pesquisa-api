package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class TipoNaoImplementadoException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public TipoNaoImplementadoException() {
		super("Esse tipo ainda não foi implementado", Status.EXPECTATION_FAILED);
	}
}
