package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class ArquivoOrfaoException extends HeptaException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5594880485912783190L;

	public ArquivoOrfaoException() {
		super("Arquivo de ser ligado a um ou mais itens solicitados.", Status.PRECONDITION_REQUIRED);
	}

}
