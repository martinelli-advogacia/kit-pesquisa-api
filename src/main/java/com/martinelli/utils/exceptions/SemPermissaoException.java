package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class SemPermissaoException  extends HeptaException {
	private static final long serialVersionUID = 1L;

	public SemPermissaoException() {
		super("Sem permissão.", Status.UNAUTHORIZED);
	}
}
