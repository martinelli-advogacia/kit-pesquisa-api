package com.martinelli.utils.exceptions;

import javax.ws.rs.core.Response.Status;

public class LoginVazioException extends HeptaException {
	private static final long serialVersionUID = 1L;

	public LoginVazioException() {
		super("Usuário/Senha não foi preenchido.", Status.EXPECTATION_FAILED);
	}
}
