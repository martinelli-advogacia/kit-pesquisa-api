package com.martinelli.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.martinelli.Application;

@WebListener
public class HibernateUtils implements ServletContextListener {
	
	static EntityManagerFactory factory;
	
	
	
	public static EntityManager getEntityManager() {
		if(factory == null) {
			factory = Persistence.createEntityManagerFactory(Application.getDbPersistenceUnitProperty());			
		}
		
		return factory.createEntityManager();
	}
	
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		getEntityManager().close();
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		if (factory != null) {
			factory.close();
		}
	}

}
