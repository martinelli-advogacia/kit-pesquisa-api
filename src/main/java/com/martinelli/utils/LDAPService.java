package com.martinelli.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.PagedResultsControl;
import javax.naming.ldap.PagedResultsResponseControl;
import javax.naming.ldap.SortControl;

import com.martinelli.dto.UsuarioAutenticadoDTO;
import com.martinelli.dto.UsuarioDTO;
import com.martinelli.utils.exceptions.ldap.BadArgumentException;

public class LDAPService {

	static final String ARGUMENTO_NULO = "Argumento nulo";
	static final String ARGUMENTO_VAZIO = "Argumento vazio";

	public static NamingEnumeration<SearchResult> busca(LdapContext ctx, String ldapSearchBase,
			String[] returningAttributes, String searchFilter) throws NamingException, BadArgumentException {
		if (ctx == null || searchFilter == null || ldapSearchBase == null)
			throw new BadArgumentException(ARGUMENTO_NULO);
		if (searchFilter.isEmpty() || ldapSearchBase.isEmpty())
			throw new BadArgumentException(ARGUMENTO_VAZIO);

		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		if (returningAttributes != null && returningAttributes.length > 0)
			searchControls.setReturningAttributes(returningAttributes);
		return ctx.search(ldapSearchBase, searchFilter, searchControls);
	}

	public static NamingEnumeration<SearchResult> busca(LdapContext ctx, String ldapSearchBase, String searchFilter)
			throws NamingException, BadArgumentException {
		return busca(ctx, ldapSearchBase, null, searchFilter);
	}

	public static NamingEnumeration<SearchResult> buscaSort(LdapContext ctx, String searchFilter, String ldapSearchBase,
			String[] returningAttributes, String sortKey, boolean verbose)
			throws NamingException, IOException, BadArgumentException {
		if (sortKey == null)
			throw new BadArgumentException(ARGUMENTO_NULO);
		if (sortKey.isEmpty())
			throw new BadArgumentException(ARGUMENTO_VAZIO);

		ctx.setRequestControls(new Control[] { new SortControl(sortKey, Control.CRITICAL) });

		return busca(ctx, ldapSearchBase, returningAttributes, searchFilter);
	}

	public static List<NamingEnumeration<SearchResult>> buscaPaginada(LdapContext ctx, String searchFilter,
			String ldapSearchBase, String[] returningAttributes, int pageSize, boolean verbose)
			throws NamingException, IOException, BadArgumentException {
		if (ctx == null || searchFilter == null || ldapSearchBase == null || returningAttributes == null)
			throw new BadArgumentException(ARGUMENTO_NULO);
		if (searchFilter.isEmpty() || ldapSearchBase.isEmpty() || returningAttributes.length == 0 || pageSize == 0)
			throw new BadArgumentException(ARGUMENTO_VAZIO);

		byte[] cookie = null;
		int total = 0;

		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchControls.setReturningAttributes(returningAttributes);
		ctx.setRequestControls(new Control[] { new PagedResultsControl(pageSize, Control.NONCRITICAL) });

		List<NamingEnumeration<SearchResult>> resultado = new ArrayList<>();
		do {
			// ctx.setRequestControls(new Control[] { new SortControl(sortKey,
			// Control.CRITICAL) });
			resultado.add(ctx.search(ldapSearchBase, searchFilter, searchControls));

			// Examine the paged results control response
			Control[] controls = ctx.getResponseControls();
			if (controls != null) {
				for (Control control : controls) {
					if (control instanceof PagedResultsResponseControl) {
						PagedResultsResponseControl prrc = (PagedResultsResponseControl) control;
						total = prrc.getResultSize();
						if (verbose) {
							if (total != 0) {
								System.out.println("***************** END-OF-PAGE "
										+ "(total : "
										+ total
										+ ") *****************\n");
							} else {
								System.out.println(
										"***************** END-OF-PAGE " + "(total: unknown)***************\n");
							}
						}
						cookie = prrc.getCookie();
					}
				}
			} else {
				System.out.println("No controls were sent from the server");
			}
			// Re-activate paged results
			ctx.setRequestControls(new Control[] { new PagedResultsControl(pageSize, cookie, Control.CRITICAL) });

		} while (cookie != null);

		ctx.close();
		return resultado;
	}

	public static String decodeSID(byte[] sid) {

		final StringBuilder strSid = new StringBuilder("S-");

		// get version
		final int revision = sid[0];
		strSid.append(Integer.toString(revision));

		// next byte is the count of sub-authorities
		final int countSubAuths = sid[1] & 0xFF;

		// get the authority
		long authority = 0;
		// String rid = "";
		for (int i = 2; i <= 7; i++) {
			authority |= ((long) sid[i]) << (8 * (5 - (i - 2)));
		}
		strSid.append("-");
		strSid.append(Long.toHexString(authority));

		// iterate all the sub-auths
		int offset = 8;
		int size = 4; // 4 bytes for each sub auth
		for (int j = 0; j < countSubAuths; j++) {
			long subAuthority = 0;
			for (int k = 0; k < size; k++) {
				subAuthority |= (long) (sid[offset + k] & 0xFF) << (8 * k);
			}

			strSid.append("-");
			strSid.append(subAuthority);

			offset += size;
		}

		return strSid.toString();
	}

	public static String convertBinaryToDashedString(byte[] objectGUID) {
		StringBuilder displayStr = new StringBuilder();

		displayStr.append(prefixZeros(objectGUID[3] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[2] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[1] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[0] & 0xFF));
		displayStr.append("-");
		displayStr.append(prefixZeros(objectGUID[5] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[4] & 0xFF));
		displayStr.append("-");
		displayStr.append(prefixZeros(objectGUID[7] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[6] & 0xFF));
		displayStr.append("-");
		displayStr.append(prefixZeros(objectGUID[8] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[9] & 0xFF));
		displayStr.append("-");
		displayStr.append(prefixZeros(objectGUID[10] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[11] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[12] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[13] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[14] & 0xFF));
		displayStr.append(prefixZeros(objectGUID[15] & 0xFF));

		return displayStr.toString();
	}

	private static String prefixZeros(int value) {
		if (value <= 0xF) {
			StringBuilder sb = new StringBuilder("0");
			sb.append(Integer.toHexString(value));

			return sb.toString();

		} else {
			return Integer.toHexString(value);
		}
	}

	private static String safeAttribute(Attributes attr, String atributo) throws NamingException {
		if (attr.get(atributo) != null)
			return attr.get(atributo).get().toString();
		else
			return "";
	}

	public static UsuarioAutenticadoDTO getUserByAccountName(LdapContext ctx, String accountName, String ldapSearchBase)
			throws NamingException, BadArgumentException {

		UsuarioAutenticadoDTO funcionario = new UsuarioAutenticadoDTO();
		String searchFilter = "(&(objectClass=user)(sAMAccountName=" + accountName + "))";
		NamingEnumeration<SearchResult> results = busca(ctx, ldapSearchBase, null, searchFilter);

		SearchResult searchResult = null;
		if (results.hasMoreElements()) {
			searchResult = results.nextElement();

			Attributes attributes = searchResult.getAttributes();
			funcionario	.setLogin(safeAttribute(attributes, "sAMAccountName"))
						.setNome(safeAttribute(attributes, "displayName"))
						.setEmail(safeAttribute(attributes, "mail"))
						.setTelefone(safeAttribute(attributes, "mobile"));

			if (results.hasMoreElements()) {
				System.err.println("Matched multiple users for the accountName: " + accountName);
				return null;
			}
		}

		return funcionario;
	}
	
	public static List<UsuarioDTO> getUserByName(LdapContext ctx, String accountName, String ldapSearchBase)
			throws NamingException, BadArgumentException {

		List<UsuarioDTO> lista = new ArrayList<>();
		String searchFilter = "(&(objectClass=user)(displayName=*" + accountName + "*))";
		NamingEnumeration<SearchResult> results = busca(ctx, ldapSearchBase, null, searchFilter);

		SearchResult searchResult = null;
		while (results.hasMoreElements()) {
			searchResult = results.nextElement();

			Attributes attributes = searchResult.getAttributes();
			UsuarioDTO usuario = new UsuarioDTO();
			usuario.setLogin(safeAttribute(attributes, "sAMAccountName"))
					.setNome(safeAttribute(attributes, "displayName")).setEmail(safeAttribute(attributes, "mail"))
					.setTelefone(safeAttribute(attributes, "mobile"));
			lista.add(usuario);
			
		}

		return lista;
	}

}
