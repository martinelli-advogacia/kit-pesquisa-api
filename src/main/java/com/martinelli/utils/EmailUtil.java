package com.martinelli.utils;

import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.martinelli.Application;

public class EmailUtil {

	static final Logger LOG = Logger.getLogger(EmailUtil.class.getName());

	public static void enviarEmailHTML(String remetente, String destinatario, String assunto, String corpoEmail) {

		Properties properties = System.getProperties();

		properties.setProperty("mail.smtp.host", Application.getMailSmtpHost());
		properties.setProperty("mail.smtp.port", Application.getMailSmtpPort());
		properties.setProperty("mail.user", Application.getMailUser());
		properties.setProperty("mail.password", Application.getMailPassword());
		
		LOG.info("Enviando email.");
		LOG.info("host " + Application.getMailSmtpHost());
		LOG.info("conteudo " + corpoEmail);

		try {

			Session session = Session.getDefaultInstance(properties);

			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(remetente));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));

			message.setSubject(assunto, "UTF-8");
			message.setContent(corpoEmail, "text/html; charset=UTF-8;");

			Transport.send(message);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
//	public static void main(String[] args) {
//		String rem = "douglas.oliveira@hepta.com.br";
//		String dest = "fernandalourenco@martinelli.adv.br";
//		String assunto =  "Teste smtp";
//		String corpo = "<h1>TESTE SMTP FUNCIONADO!!!</h1>";
//				
//		EmailUtil.enviarEmailHTML(rem, dest, assunto, corpo);
//		
//	}

}
