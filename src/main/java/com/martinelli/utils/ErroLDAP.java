package com.martinelli.utils;

public class ErroLDAP {
	//525	user not found
	//52e	invalid credentials
	//530	not permitted to logon at this time
	//531	not permitted to logon at this workstation
	//532	password expired
	//533	account disabled 
	//534	The user has not been granted the requested logon type at this machine
	//701	account expired
	//773	user must reset password
	//775	user account locked
	public static String escolherErro(String s) {
		String erroDescoberto = "";
		if (s.contains("525")) {
			erroDescoberto = "Erro 525: Usuário não encontrado.";
		} else if (s.contains("52e")) {
			erroDescoberto = "Erro 52e: Credenciais inválidas.";
		} else if (s.contains("530")) {
			erroDescoberto = "Erro 530: Não é permitido logar nesse momento.";
		} else if (s.contains("531")) {
			erroDescoberto = "Erro 531: Não é permitido logar nessa workstation.";
		} else if (s.contains("532")) {
			erroDescoberto = "Erro 532: Senha expirada.";
		} else if (s.contains("533")) {
			erroDescoberto = "Erro 533: Conta desativada";
		} else if (s.contains("534")) {
			erroDescoberto = "Erro 534: O usuário não tem o tipo de logon correto nessa maquina.";
		} else if (s.contains("701")) {
			erroDescoberto = "Erro 701: Conta expirada.";
		} else if (s.contains("773")) {
			erroDescoberto = "Erro 773: Usuário precisa resetar a senha.";
		} else if (s.contains("775")) {
			erroDescoberto = "Erro 775: Conta de usuário trancada.";
		}
		return erroDescoberto;
	}

	public static String descobrirErroNamingException(String erro) {
		String erroDescoberto = "";
		if (erro.contains("data")) {
			String[] dados = erro.split(",");
			for (String s : dados) {
				if (s.contains("data")) {
					erroDescoberto = escolherErro(s);
				}
			}
		}
		if (erroDescoberto.isEmpty())
			erroDescoberto = "Erro desconhecido. Endereço, usuário ou senha inválidos";
		return erroDescoberto;
	}

}
