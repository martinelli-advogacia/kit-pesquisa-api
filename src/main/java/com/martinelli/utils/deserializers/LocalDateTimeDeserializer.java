package com.martinelli.utils.deserializers;

import java.io.IOException;
import java.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;

public class LocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

	@Override
	public LocalDateTime deserialize(JsonParser jp, DeserializationContext dc)
			throws IOException, JsonProcessingException {
		ObjectCodec codec = jp.getCodec();
		TextNode node = (TextNode) codec.readTree(jp);
		String dateString = node.textValue();
		try {
			return LocalDateTime.parse(dateString);
		} catch (Exception e) {
			try {
				return LocalDateTime.parse(dateString + "T00:00:00");
			} catch (Exception e1) {
				try {
					return LocalDateTime.parse(dateString.substring(0, 19));
				} catch (Exception e2) {
					return null;
				}
			}
		}
	}

}
