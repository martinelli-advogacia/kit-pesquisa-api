package com.martinelli.utils;

import javax.ws.rs.core.NewCookie;

import com.martinelli.Application;

public class CookieUtil {
	static final int DURACAO_TOKEN_LOGOUT = -1;
	static final String CONTEXT_PATH_APPLICATION = Application.getApplicationContextPathProperty();
	static final String APPLICATION_HTTP_PROTOCOL = Application.getApplicationHttpProtocoloProperty();
	static final int EXPIRATION_COOKIE_DEFAULT = Application.getJwtMinutosToken() * 60; 

	public static NewCookie createCookie(String token, boolean isLogout) {
		
		int tempoExpiracao = isLogout ? DURACAO_TOKEN_LOGOUT : EXPIRATION_COOKIE_DEFAULT;
		String tokenString = isLogout ? token : "Bearer " + token;
		boolean secure = APPLICATION_HTTP_PROTOCOL.equalsIgnoreCase("https") ? true : false;
		
		return new NewCookie("Authentication",    	// nome do cookie
						tokenString, 				// valor do cookie
						CONTEXT_PATH_APPLICATION, 	// path do cookie
						null,						// dominio do cookie
						NewCookie.DEFAULT_VERSION, 	// versão do cookie
						null, 						// comentario do cookie ( ? )
						tempoExpiracao,				// tempo de duração do cookie 
						null, 						// tempo de expiração do cookie
						secure, 					// cookie trafega em https
						true);						// cookie é http only
		
	}

}
