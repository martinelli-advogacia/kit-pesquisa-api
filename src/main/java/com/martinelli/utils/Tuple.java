package com.martinelli.utils;

public class Tuple<S1, S2> {
	S1 first;
	S2 second;

	public Tuple(S1 first, S2 second) {
		super();
		this.first = first;
		this.second = second;
	}

	public Tuple() {
		super();
	}

	public S1 getFirst() {
		return first;
	}

	public void setFirst(S1 first) {
		this.first = first;
	}

	public S2 getSecond() {
		return second;
	}

	public void setSecond(S2 second) {
		this.second = second;
	}

}
