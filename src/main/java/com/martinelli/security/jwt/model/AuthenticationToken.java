package com.martinelli.security.jwt.model;

import java.util.Calendar;

public class AuthenticationToken {

	String token;
	Calendar expira;

	public AuthenticationToken() {
		super();
	}

	public AuthenticationToken(String token) {
		this.token = token;
	}

	public AuthenticationToken(String compact, Calendar expira) {
		this.token = compact;
		this.expira = expira;
	}

	public String getToken() {
		return token;
	}

	public Calendar getExpiration() {
		return expira;
	}
	
}
