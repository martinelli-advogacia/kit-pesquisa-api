package com.martinelli.security.jwt.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.martinelli.security.jwt.enums.RolesEnum;

@Retention(RUNTIME)
@Target({ METHOD, TYPE })
public @interface RolesAllowed {
	RolesEnum[] value();
}
