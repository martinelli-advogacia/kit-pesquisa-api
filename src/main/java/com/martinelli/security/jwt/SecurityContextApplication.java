package com.martinelli.security.jwt;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;

import io.jsonwebtoken.Claims;

public class SecurityContextApplication implements SecurityContext {

	ContainerRequestContext requestContext;
	Claims claims;

	public SecurityContextApplication(ContainerRequestContext requestContext, Claims claims) {
		this.requestContext = requestContext;
		this.claims = claims;
	}

	@Override
	public Principal getUserPrincipal() {
		String user = claims.getIssuer();
		return () -> user;
	}

	@Override
	public boolean isUserInRole(String role) {
		List<String> niveisAcesso = extractAuthoritiesFromClaims(claims);
		return niveisAcesso.stream().anyMatch(nivel -> nivel.equalsIgnoreCase(role));
	}

	@Override
	public boolean isSecure() {
		SecurityContext currentSecurityContext = requestContext.getSecurityContext();
		return currentSecurityContext.isSecure();
	}

	@Override
	public String getAuthenticationScheme() {
		return TokenUtil.AUTHENTICATION_SCHEME;
	}

	private List<String> extractAuthoritiesFromClaims(Claims claims) {
		String jsonNivelAcesso = claims.get("niveis-acesso", String.class);
		return Arrays.asList(jsonNivelAcesso.split(","));
	}

}
