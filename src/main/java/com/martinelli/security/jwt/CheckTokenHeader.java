package com.martinelli.security.jwt;

import io.jsonwebtoken.Claims;

public class CheckTokenHeader {

	static final String AUTHENTICATION_SCHEME = "BEARER";
	
	public CheckTokenHeader() {
	}

	public boolean isTokenBasedAuthentication(String token) {
		return token != null
				&& token.toLowerCase().startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ");
	}
	
	public Claims extractClaim(String token) throws Exception {
		String tokenSemBaerer = token.substring(AUTHENTICATION_SCHEME.length()).trim();
		Claims claims = TokenUtil.extractClaimsFromToken(tokenSemBaerer);
		if (claims == null) {
			throw new Exception("Token invalido");
		}
		return claims;
	}
}
