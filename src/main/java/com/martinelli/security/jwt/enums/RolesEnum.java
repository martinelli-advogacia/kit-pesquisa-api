package com.martinelli.security.jwt.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum RolesEnum {

	SOLICITANTE(0, "Solicitante"),
	PESQUISADOR(1, "Pesquisador"),
	ADMINISTADOR(2, "Administrador");

	int value;
	String name;

	private RolesEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public int getValue() {
		return this.value;
	}

	public static List<RolesEnum> getEnumList() {
		List<RolesEnum> le = new ArrayList<>();
		le.addAll(Arrays.asList(RolesEnum.values()));
		return le;
	}

	public static RolesEnum fromString(String name) {
		Optional<RolesEnum> res = getEnumList().stream().filter(e -> e.getName().equals(name)).findFirst();
		if (res.isPresent()) {
			return res.get();
		}
		return null;
	}

	public static RolesEnum fromInt(int value) {
		Optional<RolesEnum> res = getEnumList().stream().filter(e -> e.getValue() == value).findFirst();
		if (res.isPresent()) {
			return res.get();
		}
		return null;
	}

}
