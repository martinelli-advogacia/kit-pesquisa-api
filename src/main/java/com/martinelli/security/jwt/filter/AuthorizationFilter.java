package com.martinelli.security.jwt.filter;

import java.lang.reflect.Method;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import com.martinelli.security.jwt.annotation.RolesAllowed;
import com.martinelli.security.jwt.enums.RolesEnum;
import com.martinelli.security.jwt.exception.AccessDeniedException;


@Provider
@Priority(Priorities.AUTHORIZATION)
public class AuthorizationFilter implements ContainerRequestFilter {

	@Context
	ResourceInfo resourceInfo;

	@Override
	public void filter(ContainerRequestContext requestContext) {
		Method method = resourceInfo.getResourceMethod();
		if (method.isAnnotationPresent(DenyAll.class)) {
			refuseRequest(requestContext);
		}
		RolesAllowed rolesAllowed = method.getAnnotation(RolesAllowed.class);
		if (rolesAllowed != null) {
			performAuthorization(rolesAllowed.value(), requestContext);
			return;
		}
		if (method.isAnnotationPresent(PermitAll.class)) {
			return;
		}
		rolesAllowed = resourceInfo.getResourceClass().getAnnotation(RolesAllowed.class);
		if (rolesAllowed != null) {
			performAuthorization(rolesAllowed.value(), requestContext);
		}
		if (resourceInfo.getResourceClass().isAnnotationPresent(PermitAll.class)) {
			return;
		}
		if (!isAuthenticated(requestContext)) {
			refuseRequest(requestContext);
		}
	}

	private boolean isAuthenticated(ContainerRequestContext requestContext) {
		return requestContext.getSecurityContext().getUserPrincipal() != null;
	}

	private void performAuthorization(RolesEnum[] rolesAllowed, ContainerRequestContext requestContext)
			throws AccessDeniedException {
		if (rolesAllowed.length > 0 && !isAuthenticated(requestContext)) {
			refuseRequest(requestContext);
		}
		for (final RolesEnum role : rolesAllowed) {
			if (requestContext.getSecurityContext().isUserInRole(role.getName())) {
				return;
			}
		}
		refuseRequest(requestContext);
	}

	private void refuseRequest(ContainerRequestContext requestContext) throws AccessDeniedException {
		requestContext.abortWith(Response.status(Status.FORBIDDEN).entity("Você não tem permissão para acessar este recurso.").build());
	}
}
