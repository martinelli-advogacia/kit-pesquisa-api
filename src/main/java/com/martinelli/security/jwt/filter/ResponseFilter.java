package com.martinelli.security.jwt.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.ext.Provider;

import com.martinelli.Application;
import com.martinelli.security.jwt.TokenUtil;
import com.martinelli.security.jwt.model.AuthenticationToken;
import com.martinelli.utils.CookieUtil;

import io.jsonwebtoken.Claims;

@Provider
public class ResponseFilter implements ContainerResponseFilter {
	static final Logger LOGGER = Logger.getLogger(ResponseFilter.class.getName());

	public void filter(ContainerRequestContext req, ContainerResponseContext res) throws IOException {
		Cookie cookie = req.getCookies().get("Authentication"); // se existe um token no response, da um refresh no
																// token
		res.getHeaders().add("versao-app", Application.getApplicationDomainFrontVersion());
		String authorizationToken = "";
		try {
			// caso seja um request de logout ou sem credencial o cookie esta nulo
			if (cookie != null && !cookie.getValue().isEmpty() && !req.getMethod().equals("OPTIONS")
					&& !req.getUriInfo().getPath().equalsIgnoreCase("auth/logout")) {
				authorizationToken = cookie.getValue();
				String tokenWithoutBearer = TokenUtil.removeBearerFromToken(authorizationToken);
				Claims claims = TokenUtil.extractClaimsFromToken(tokenWithoutBearer);
				AuthenticationToken refreshToken = TokenUtil.refreshToken(claims);
				NewCookie cookieAuthorization = CookieUtil.createCookie(refreshToken.getToken(), false);
				res.getHeaders().add("set-cookie", cookieAuthorization.toString());
			}
		} catch (Exception e) {
			LOGGER.severe(e.getMessage());
		}
	}
	
}