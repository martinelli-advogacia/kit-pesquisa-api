package com.martinelli.security.jwt.filter;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.Priority;
import javax.annotation.security.PermitAll;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.martinelli.security.jwt.CheckTokenHeader;
import com.martinelli.security.jwt.SecurityContextApplication;

import io.jsonwebtoken.Claims;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
	
	static final CheckTokenHeader CHECK_TOKEN = new CheckTokenHeader();
	
	@Context
	ResourceInfo resourceInfo;
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		
		Method method = resourceInfo.getResourceMethod();
		if (method.isAnnotationPresent(PermitAll.class)) {
			return;
		}
		if (resourceInfo.getResourceClass().isAnnotationPresent(PermitAll.class)) {
			return;
		}

		Cookie cookie = requestContext.getCookies().get("Authentication");
		String authorizationToken = "";
	
		if(cookie != null) {
			authorizationToken = cookie.getValue();
		}
		
		if (CHECK_TOKEN.isTokenBasedAuthentication(authorizationToken)) {
			try {
				Claims claims = CHECK_TOKEN.extractClaim(authorizationToken);
				modificarRequestContext(requestContext, claims);
			} catch (Exception e) {
				abortWithUnauthorized(requestContext);
			}
		}
	}

	private void abortWithUnauthorized(ContainerRequestContext requestContext) {
		requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
	}
	
	private void modificarRequestContext(ContainerRequestContext requestContext, Claims claims) {
		requestContext.setSecurityContext(new SecurityContextApplication(requestContext, claims));
	}
	
}
