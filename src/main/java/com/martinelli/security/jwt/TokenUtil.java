package com.martinelli.security.jwt;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import com.martinelli.Application;
import com.martinelli.dto.UsuarioAutenticadoDTO;
import com.martinelli.security.jwt.model.AuthenticationToken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenUtil {
	static final String FRASE_SEGREDO = Application.getJwtFraseSegredo();
	static final int TEMPO_TOKEN = Application.getJwtMinutosToken();
	static final SignatureAlgorithm algoritimoAssinatura = SignatureAlgorithm.HS512;
	public static final String AUTHENTICATION_SCHEME = "Bearer";

	public static AuthenticationToken emiteToken(UsuarioAutenticadoDTO pessoaDTO) {
		Date agora = new Date();

		Calendar expira = Calendar.getInstance();
		expira.add(Calendar.MINUTE, TEMPO_TOKEN);
		
		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(FRASE_SEGREDO);
		SecretKeySpec key = new SecretKeySpec(apiKeySecretBytes, algoritimoAssinatura.getJcaName());

		JwtBuilder construtor = Jwts.builder()
			.setIssuedAt(agora)
			.setIssuer(pessoaDTO.getLogin())
			.signWith(key, algoritimoAssinatura)
			.setExpiration(expira.getTime())
			.claim("niveis-acesso", pessoaDTO.getRolesAllowed())
			.claim("email", pessoaDTO.getEmail())
			.claim("login", pessoaDTO.getLogin())
			.claim("Nome", pessoaDTO.getNome());
		
		return new AuthenticationToken(construtor.compact(), expira);
	}

	public static AuthenticationToken refreshToken(Claims claims) {

		Date agora = new Date();

		Calendar expira = Calendar.getInstance();
		expira.add(Calendar.MINUTE, TEMPO_TOKEN);

		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(FRASE_SEGREDO);
		SecretKeySpec key = new SecretKeySpec(apiKeySecretBytes, algoritimoAssinatura.getJcaName());

		JwtBuilder construtor = Jwts.builder()
			.setIssuedAt(agora)
			.setIssuer(claims.getIssuer())
			.signWith(key, algoritimoAssinatura)
			.setExpiration(expira.getTime())
			.claim("niveis-acesso",	claims.get("niveis-acesso", String.class))
			.claim("email",claims.get("email", String.class))
			.claim("login", claims.get("login", String.class))
			.claim("Nome", claims.get("nome", String.class));

		return new AuthenticationToken(construtor.compact(), expira);
	}

	public static Claims extractClaimsFromToken(String token) {
		return Jwts.parserBuilder()
				.setSigningKey(DatatypeConverter.parseBase64Binary(FRASE_SEGREDO)).build()
				.parseClaimsJws(token)
				.getBody();
	}
	
	public static String extractToken(HttpServletRequest request) {
		String cookiesString = request.getHeader("Cookie");
		String cookieHeader = cookiesString.replace("\"", "");
		String[] split = cookieHeader.split("[=;]");
		String authorizantionCookie = Arrays.stream(split).filter(s -> s.contains("Bearer")).findFirst().orElse("");

		return authorizantionCookie.substring(TokenUtil.AUTHENTICATION_SCHEME.length()).trim();
	}
	
	public static String removeBearerFromToken(String authorizantionCookie) {
		if(authorizantionCookie.contains(TokenUtil.AUTHENTICATION_SCHEME))
			return authorizantionCookie.substring(TokenUtil.AUTHENTICATION_SCHEME.length()).trim();
		else return authorizantionCookie;
	}

	public static Claims extractClaimsFromRequest(HttpServletRequest request) {
		return extractClaimsFromToken(extractToken(request));
	}

}
