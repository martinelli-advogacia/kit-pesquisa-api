package com.martinelli.security.jwt.exception;

public class AccessDeniedException extends RuntimeException {
	static final long serialVersionUID = 1L;

	public AccessDeniedException() {
		super();
	}

	public AccessDeniedException(String message, Throwable cause) {
		super(message, cause);
	}

	public AccessDeniedException(String message) {
		super(message);
	}
}
