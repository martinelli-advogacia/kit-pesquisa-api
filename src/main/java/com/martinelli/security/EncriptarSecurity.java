package com.martinelli.security;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class EncriptarSecurity {
	Cipher cipher;
	PublicKey key;
	byte[] keyBytes = { 48, -127, -97, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -127, -115, 0, 48,
			-127, -119, 2, -127, -127, 0, -93, -80, -62, -63, 100, -18, 35, 10, 102, -79, -83, -69, 67, 108, -117, 44,
			-69, 25, -15, 36, 96, 92, 102, -2, 87, -124, -3, -55, 18, 86, 57, -5, 16, -18, 43, -89, -71, -66, 80, -53,
			-68, -77, -2, 86, -127, 86, -27, -2, -14, -62, -122, -73, -69, -63, 123, -114, -89, 100, 34, -115, -38, -18,
			62, -42, 17, -101, -50, -125, -121, 76, 51, -5, -13, 109, 86, 83, 99, 88, -40, 103, -2, 83, -53, 90, 69, 38,
			125, 38, 11, 30, 35, 119, -3, 105, 88, 32, 20, 96, 33, 113, 5, -83, -119, -116, 49, 51, -6, -14, -49, 105,
			37, 11, 64, -56, 67, 46, -126, 111, -57, -97, 6, -124, 105, -29, -103, -42, 111, 83, 2, 3, 1, 0, 1 };

	public EncriptarSecurity() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException,
			IOException, URISyntaxException {
		init();
	}

	public EncriptarSecurity(byte[] keyBytes) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeySpecException, IOException, URISyntaxException {
		this.keyBytes = keyBytes;
		init();
	}

	public EncriptarSecurity(URL file) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException,
			IOException, URISyntaxException {
		this.keyBytes = Files.readAllBytes(new File(file.toURI().getPath()).toPath());
		init();
	}

	public void init() throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, IOException,
			URISyntaxException {
		this.cipher = Cipher.getInstance("RSA");
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		this.key = kf.generatePublic(spec);
	}

	public String encriptarString(String msg) throws NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
		this.cipher.init(Cipher.ENCRYPT_MODE, this.key);
		return Base64.getEncoder().encodeToString(cipher.doFinal(msg.getBytes("UTF-8")));
	}
}
