package com.martinelli.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import com.martinelli.Application;

@Provider
@PreMatching
public class CorsFilter implements ContainerRequestFilter, ContainerResponseFilter {

	static final List<String> ORIGIN = Arrays.asList(Application.getApplicationDomainFrontProperty().split(";"));

	@Override
	public void filter(ContainerRequestContext request) throws IOException {

		if (isPreflightRequest(request)) {
			request.abortWith(Response.ok().build());
		}
	}

	private static boolean isPreflightRequest(ContainerRequestContext request) {
		return request.getHeaderString("Origin") != null && request.getMethod().equalsIgnoreCase("OPTIONS");
	}

	@Override
	public void filter(ContainerRequestContext request, ContainerResponseContext response) throws IOException {

		if (request.getHeaderString("Origin") == null) {
			return;
		}

		if (isPreflightRequest(request)) {
			response.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
			response.getHeaders()
					.add("Access-Control-Allow-Headers",
							"X-Requested-With, Authorization, Accept-Version, Content-MD5, CSRF-Token, Content-Type, versao-app");
		}

		Optional<String> origem = ORIGIN.stream()
										.filter(o -> request.getHeaderString("Origin").contains(o))
										.findFirst();

		if (origem.isPresent()) {
			response.getHeaders().add("Access-Control-Allow-Credentials", "true");
			response.getHeaders().add("Access-Control-Allow-Origin", origem.get());
			response.getHeaders()
					.add("Access-Control-Expose-Headers", "Content-type, Content-Disposition, X-Suggested-Filename, versao-app");
		}
	}

}
