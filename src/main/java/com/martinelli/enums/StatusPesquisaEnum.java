package com.martinelli.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum StatusPesquisaEnum {
	
	ABERTO(0, "Aberto"),
	EM_ATENDIMENTO(1, "Em Atendimento"),
	CONCLUIDA(2, "Concluida");
	
	int value;
	String name;
	private StatusPesquisaEnum(int value, String name) {
		this.value = value;
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public String getName() {
		return name;
	}
	
	public static List<StatusPesquisaEnum> getEnumList() {
		List<StatusPesquisaEnum> le = new ArrayList<>();
		le.addAll(Arrays.asList(StatusPesquisaEnum.values()));
		return le;
	}

	public static StatusPesquisaEnum fromString(String name) {
		Optional<StatusPesquisaEnum> res = getEnumList().stream().filter(e -> e.getName().equalsIgnoreCase(name)).findFirst();
		if (res.isPresent()) {
			return res.get();
		}
		return null;
	}

	public static StatusPesquisaEnum fromInt(int value) {
		Optional<StatusPesquisaEnum> res = getEnumList().stream().filter(e -> e.getValue() == value).findFirst();
		if (res.isPresent()) {
			return res.get();
		}
		return null;
	}

}
