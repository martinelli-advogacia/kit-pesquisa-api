package com.martinelli.rest;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.martinelli.dto.HealthCheckJsonDTO;
import com.martinelli.service.HealthCheckService;

@Path("health")
public class HealthCheckREST extends SuperREST {
	
	private static final HealthCheckService healthCheckService = new HealthCheckService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public Response healthCheck() {
		HealthCheckJsonDTO status = healthCheckService.veficacaoGeral();
		return Response.ok(status).build();
	}

}
