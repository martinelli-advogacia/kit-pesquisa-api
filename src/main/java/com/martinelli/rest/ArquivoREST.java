package com.martinelli.rest;

import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.martinelli.dto.ArquivoComAnexoDTO;
import com.martinelli.dto.ArquivoDTO;
import com.martinelli.dto.UsuarioDTO;
import com.martinelli.security.jwt.annotation.RolesAllowed;
import com.martinelli.security.jwt.enums.RolesEnum;
import com.martinelli.service.ArquivoService;
import com.martinelli.utils.StacktraceToString;
import com.martinelli.utils.exceptions.HeptaException;

@Path("arquivo")
public class ArquivoREST extends SuperREST {

	static final Logger LOGGER = Logger.getLogger(ArquivoREST.class.getName());

	static final ArquivoService ARQUIVO_SERVICE = new ArquivoService();

	@POST
	@Path("{idSolicitacao}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response salvarArquivo(@PathParam("idSolicitacao") Long idSolicitacao,
			@FormDataParam("dados") FormDataBodyPart json, @FormDataParam("arquivo") InputStream arquivo) {
		try {
			json.setMediaType(MediaType.APPLICATION_JSON_TYPE);
			ArquivoDTO newArquivo = json.getValueAs(ArquivoDTO.class);
			newArquivo.setAtualizador(new UsuarioDTO(getLogin()));
			ArquivoDTO arquivoSalvo = ARQUIVO_SERVICE.salvar(idSolicitacao, newArquivo, arquivo);

			return Response.ok(arquivoSalvo).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@DELETE
	@Path("{idArquivo}")
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	@Produces(MediaType.APPLICATION_JSON)
	public Response deletar(@PathParam("idArquivo") long idArquivo, @PathParam("idSolicitacao") long idSolicitacao) {
		try {
			ARQUIVO_SERVICE.deletar(idArquivo, idSolicitacao, getLogin());
			return Response.ok().build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@GET
	@Path("baixar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response baixar(@QueryParam("idSolicitacao") Long idSolicitacao, @QueryParam("idArquivo") Long idArquivo) {
		try {
			ArquivoComAnexoDTO arq = ARQUIVO_SERVICE.getFile(idSolicitacao, idArquivo, getLogin(), isPesquisador());
			return Response.ok(arq).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@GET
	@Path("baixarTodos/{idSolicitacao}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response baixarTodos(@PathParam("idSolicitacao") Long idSolicitacao) {
		try {
			List<ArquivoComAnexoDTO> arq = ARQUIVO_SERVICE.getAllFiles(idSolicitacao, getLogin(), isPesquisador());
			return Response.ok(arq).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@GET
	@Path("/{idSolicitacao}")
	public Response buscarArquivosSolicitacao(@PathParam("idSolicitacao") Long idSolicitacao) {
		try {
			List<ArquivoDTO> arqs = ARQUIVO_SERVICE.findTodos(idSolicitacao, getLogin(), isPesquisador());
			return Response.ok(arqs).build();
		} catch (HeptaException e) {
			e.printStackTrace();
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

}
