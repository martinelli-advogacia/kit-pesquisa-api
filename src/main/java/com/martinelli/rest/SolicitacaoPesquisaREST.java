package com.martinelli.rest;

import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.martinelli.dto.RelatorioFilterDTO;
import com.martinelli.dto.ResultadoPaginado;
import com.martinelli.dto.SolicitacaoPesquisaConsultaDTO;
import com.martinelli.dto.SolicitacaoPesquisaDTO;
import com.martinelli.dto.SolicitacaoPesquisaFilterDTO;
import com.martinelli.dto.SolicitacaoPorVariasEmpresasDTO;
import com.martinelli.dto.UsuarioDTO;
import com.martinelli.excel.RelatorioSolicitacoes;
import com.martinelli.model.SolicitacaoPesquisa;
import com.martinelli.security.jwt.annotation.RolesAllowed;
import com.martinelli.security.jwt.enums.RolesEnum;
import com.martinelli.service.SolicitacaoPesquisaService;
import com.martinelli.utils.StacktraceToString;
import com.martinelli.utils.exceptions.HeptaException;

@Path("/solicitacao")
public class SolicitacaoPesquisaREST extends SuperREST {

	static final Logger LOGGER = Logger.getLogger(SolicitacaoPesquisaREST.class.getName());

	static final SolicitacaoPesquisaService solicitacaoService = new SolicitacaoPesquisaService();

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.SOLICITANTE, RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response buscaSolicitacao(@PathParam("id") Long id) {
		try {
			SolicitacaoPesquisa solicitacao = solicitacaoService.buscaSolicitacao(id, getLogin(), isPesquisador());
			return Response.ok(new SolicitacaoPesquisaConsultaDTO(solicitacao)).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@GET
	@Path("/solicitante")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.SOLICITANTE, RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response buscaSolicitacoesSolicitante(@QueryParam("data-inicio") long dataInicio,
			@QueryParam("data-fim") long dataFim, @QueryParam("empresa") Long idEmpresa,
			@QueryParam("status") List<String> status, @QueryParam("page") long page,
			@QueryParam("qtd-itens") long qtdItens) {
		try {
			ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> lista = null;
			SolicitacaoPesquisaFilterDTO filtros = new SolicitacaoPesquisaFilterDTO(getLogin(), null, status, idEmpresa,
					dataInicio, dataFim);

			lista = solicitacaoService.buscaListaPesquisasSolicitante(filtros, qtdItens, page);
			return Response.ok(lista).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@GET
	@Path("/buscarSolicitantes")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.SOLICITANTE, RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response buscarSolicitantes() {
		try {
			Set<UsuarioDTO> solicitantes = solicitacaoService.buscarSolicitantes();
			return Response.ok(solicitantes).build();
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response criaSolicitacaoPorEmpresa(SolicitacaoPorVariasEmpresasDTO solicitacoes) {
		String solicitante = getLogin();

		try {
			List<SolicitacaoPesquisaDTO> solicitacaoPesquisa = solicitacaoService.criaSolicitacao(solicitacoes,
					solicitante);
			return Response.ok(solicitacaoPesquisa).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@PUT
	public Response atualizaSolicitacao(SolicitacaoPesquisa solicitacao) {
		return Response.ok(solicitacao).build();
	}

	@DELETE
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR, RolesEnum.SOLICITANTE })
	@Path("/minha/{id}")
	public Response excluiSolicitacaoPropria(@PathParam("id") Long id) {

		try {
			if (solicitacaoService.excluiSolicitacaoPropria(id, getLogin()))
				return Response.ok("excluido" + id).build();
			else {
				return Response.serverError().build();
			}
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@DELETE
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	@Path("/{id}")
	public Response excluiSolicitacao(@PathParam("id") Long id) {
		try {
			if (solicitacaoService.excluiSolicitacao(id))
				return Response.ok("excluido" + id).build();
			else {
				return Response.serverError().build();
			}
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@GET
	@Path("/todas")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response buscarPesquisasTodas() {
		try {
			List<SolicitacaoPesquisaConsultaDTO> lista = solicitacaoService.buscaListaPesquisasTodas();
			return Response.ok(lista).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@GET
	@Path("/nao-concluidas")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response buscarPesquisasNaoConcluidas() {
		try {
			List<SolicitacaoPesquisaConsultaDTO> lista = solicitacaoService.buscaListaPesquisasNaoConcluidas();
			return Response.ok(lista).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@GET
	@Path("/concluidas")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response buscarPesquisasConcluidas(@QueryParam("data-inicio") long dataInicio,
			@QueryParam("data-fim") long dataFim, @QueryParam("empresa") Long idEmpresa,
			@QueryParam("solicitante") String loginSolicitante, @QueryParam("atendente") String loginAtendente,
			@QueryParam("page") long page, @QueryParam("qtd-itens") long qtdItens) {
		try {
			ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> lista = null;
			SolicitacaoPesquisaFilterDTO filtros = new SolicitacaoPesquisaFilterDTO(loginSolicitante, loginAtendente,
					null, idEmpresa, dataInicio, dataFim);

			lista = solicitacaoService.buscaListaPesquisasConcluidas(filtros, qtdItens, page);
			return Response.ok(lista).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@GET
	@Path("/atender/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response atender(@PathParam("id") Long id) {

		try {
			SolicitacaoPesquisaConsultaDTO item = solicitacaoService.atualizarAtendente(id, getLogin());
			return Response.ok(item).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@GET
	@Path("/concluir/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response concluir(@PathParam("id") Long id) {
		try {
			SolicitacaoPesquisaConsultaDTO item = solicitacaoService.concuirSolicitacao(id, getLogin());
			return Response.ok(item).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@GET
	@Path("/reverter/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response reverter(@PathParam("id") Long id) {
		try {
			SolicitacaoPesquisaConsultaDTO item = solicitacaoService.reverterSolicitacao(id);
			return Response.ok(item).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@POST
	@Path("/listaRelatorio")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({ RolesEnum.SOLICITANTE, RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response buscaSolicitacoesFiltradoRelatorio(RelatorioFilterDTO filtros) {
		try {
			ResultadoPaginado<List<SolicitacaoPesquisaConsultaDTO>> lista = solicitacaoService.buscaListaPesquisas(
					filtros);

			return Response.ok(lista).build();
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

	@POST
	@Path("/relatorio")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@RolesAllowed({ RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR })
	public Response relatorio(RelatorioFilterDTO filtros) {
		try {
			RelatorioSolicitacoes relatorio = new RelatorioSolicitacoes();
			List<SolicitacaoPesquisaConsultaDTO> solicitacoes = solicitacaoService.buscaListaPesquisasConcluidasRelatorio(
					filtros);
			InputStream input = relatorio.gerarXLSX(solicitacoes);
			return Response	.ok(input, MediaType.APPLICATION_OCTET_STREAM)
							.header("Content-Disposition", "attachment; filename=\"relatorio.xlsx\"")
							.build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}
}
