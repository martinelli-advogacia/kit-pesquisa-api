package com.martinelli.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.martinelli.dto.TipoItemPesquisaDTO;
import com.martinelli.service.TipoItemPesquisaService;
import com.martinelli.utils.StacktraceToString;

@Path("/tipo-item")
public class TipoItemPesquisaREST extends SuperREST {

	static final Logger LOGGER = Logger.getLogger(TipoItemPesquisaREST.class.getName());

	static final TipoItemPesquisaService TIPO_ITEM_SERVICE = new TipoItemPesquisaService();

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscaTiposItens() {
		try {
			List<TipoItemPesquisaDTO> tipos = TIPO_ITEM_SERVICE.getAllTipoItens();
			return Response.ok(tipos).build();
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@GET
	@Path("semArquivo/{idSolicitacao}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarSemArquivo(@PathParam("idSolicitacao") Long idSolicitacao) {
		try {
			List<TipoItemPesquisaDTO> tipos = TIPO_ITEM_SERVICE.buscarSemArquivo(idSolicitacao, getLogin());
			return Response.ok(tipos).build();
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

}
