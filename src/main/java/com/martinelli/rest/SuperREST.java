package com.martinelli.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import com.martinelli.dto.ErroResponseDTO;
import com.martinelli.security.jwt.TokenUtil;
import com.martinelli.utils.StacktraceToString;
import com.martinelli.utils.exceptions.HeptaException;

import io.jsonwebtoken.Claims;

public class SuperREST {
	@Context
	HttpServletRequest request;
	@Context
	HttpServletResponse response;

	public Response erroPadrao(Exception e) {
		ErroResponseDTO erro = new ErroResponseDTO();
		erro.setMsg("Erro desconhecido, envie o erro abaixo para o suporte:")
			.setStatus(777)
			.setStackTrace(StacktraceToString.convert(e));
		return Response.status(777).entity(erro).build();
	}

	public String getLogin() {
		String token = TokenUtil.extractToken(request);
		Claims claims = TokenUtil.extractClaimsFromToken(token);
		return claims.get("login", String.class);
	}

	public String getPermissao() {
		String token = TokenUtil.extractToken(request);
		Claims claims = TokenUtil.extractClaimsFromToken(token);
		return claims.get("niveis-acesso", String.class);
	}

	public boolean isPesquisador() {
		return getPermissao().equals("Pesquisador") || getPermissao().equals("Administrador");
	}

	public Response heptaError(HeptaException e) {
		return Response.status(e.getStatus()).entity(e.getMessage()).build();
	}
}
