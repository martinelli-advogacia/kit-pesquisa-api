package com.martinelli.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.martinelli.model.ItemPesquisa;
import com.martinelli.security.jwt.annotation.RolesAllowed;
import com.martinelli.security.jwt.enums.RolesEnum;
import com.martinelli.service.ItemPesquisaService;
import com.martinelli.utils.StacktraceToString;
import com.martinelli.utils.exceptions.FalhaAtualizarItemPesquisa;
import com.martinelli.utils.exceptions.HeptaException;

@Path("item-pesquisa")
public class ItemPesquisaREST extends SuperREST {

	static final Logger LOGGER = Logger.getLogger(ItemPesquisaREST.class.getName());

	static final ItemPesquisaService ITEM_PESQUISA_SERVICE = new ItemPesquisaService();

	@POST
	@Path("sem-resultado")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed({RolesEnum.PESQUISADOR, RolesEnum.ADMINISTADOR})
	public Response semResultado(List<ItemPesquisa> itens, @QueryParam("flag") String flagFront) {
		Boolean flag = null;
		if (flagFront != null) {
			flag = Boolean.valueOf(flagFront);
		} else {
			flag = true;
		}

		try {
			if (ITEM_PESQUISA_SERVICE.setSemResultado(itens, flag, getLogin())) {
				return Response.ok().build();
			} else {
				throw new FalhaAtualizarItemPesquisa();
			}
		} catch (HeptaException e) {
			e.printStackTrace();
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}

	}

}
