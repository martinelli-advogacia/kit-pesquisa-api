package com.martinelli.rest;

import java.util.logging.Logger;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import com.martinelli.dto.LoginDTO;
import com.martinelli.dto.UsuarioAutenticadoDTO;
import com.martinelli.service.AuthenticationService;
import com.martinelli.utils.CookieUtil;
import com.martinelli.utils.StacktraceToString;
import com.martinelli.utils.Tuple;
import com.martinelli.utils.exceptions.HeptaException;

@Path("/auth")
public class AuthenticationREST extends SuperREST {
	
	static final Logger LOGGER = Logger.getLogger(UsuarioREST.class.getName());

	@Path("/login")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@PermitAll
	public Response login(LoginDTO loginDto) {
		try {
			Tuple<UsuarioAutenticadoDTO, NewCookie> res = AuthenticationService.login(loginDto);
			return Response.ok(res.getFirst()).cookie(res.getSecond()).build();
		} catch (HeptaException e) {
			return heptaError(e);
		} catch (Exception e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
			return erroPadrao(e);
		}
	}

	@Path("/logout")
	@GET
	@PermitAll
	public Response logout() {
		return Response.ok().cookie(CookieUtil.createCookie("", true)).build();
	}

	@Path("/ping")
	@GET
	@PermitAll
	public Response teste() {
		return Response.ok().build();
	}

}
