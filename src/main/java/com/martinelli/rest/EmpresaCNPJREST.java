package com.martinelli.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.martinelli.model.EmpresaCNPJ;
import com.martinelli.service.EmpresaCNPJService;

@Path("empresa")
public class EmpresaCNPJREST extends SuperREST{
	
	static final EmpresaCNPJService EMPRESA_CNPJ_SERVICE = new EmpresaCNPJService();
	
	@GET
	@Path("buscar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscaEmpresasCNPJOuNome(@QueryParam("param") String busca) {
		
		List<EmpresaCNPJ> empresas = EMPRESA_CNPJ_SERVICE.buscaEmpresa(busca);
		return Response.ok(empresas).build();
		
	}

}
