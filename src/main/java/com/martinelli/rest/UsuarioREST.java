package com.martinelli.rest;

import java.util.List;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.martinelli.dto.UsuarioDTO;
import com.martinelli.security.jwt.annotation.RolesAllowed;
import com.martinelli.security.jwt.enums.RolesEnum;
import com.martinelli.service.UsuarioService;
import com.martinelli.utils.StacktraceToString;
import com.martinelli.utils.exceptions.LdapException;
import com.martinelli.utils.exceptions.UsuarioInvalidoException;
import com.martinelli.utils.exceptions.ldap.BadArgumentException;

@Path("usuario")
public class UsuarioREST extends SuperREST{
	
	static final Logger LOGGER = Logger.getLogger(UsuarioREST.class.getName());
	
	static final UsuarioService USUARIO_SERVICE = new UsuarioService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(RolesEnum.ADMINISTADOR)
	public Response getAll() {
		List<UsuarioDTO> usuarios = USUARIO_SERVICE.getAll();
		return Response.ok(usuarios).build();
	}
	
	@GET
	@Path("{login}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(RolesEnum.ADMINISTADOR)
	public Response getUsuario(@PathParam("login") String login) {
		UsuarioDTO usuario = USUARIO_SERVICE.findByLoginComPerfil(login);
		return Response.ok(usuario).build();
	}
	
	@GET
	@Path("buscar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscaUsuario(@QueryParam("param") String busca) {
		List<UsuarioDTO> lista = USUARIO_SERVICE.buscaSolicitante(busca);
		return Response.ok(lista).build();
	}
			
	@GET
	@Path("atendentes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscaAtendentes() {
		List<UsuarioDTO> lista = USUARIO_SERVICE.buscaAtendente();
		return Response.ok(lista).build();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(RolesEnum.ADMINISTADOR)
	public Response updateUsuario(UsuarioDTO usuario) {
		USUARIO_SERVICE.updateUsuario(usuario);
		return Response.ok(usuario).build();
	}
	
	@PUT
	@Path("uf")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUfUsuario(UsuarioDTO usuario) {
		usuario = USUARIO_SERVICE.updateUfUsuario(usuario);
		return Response.ok(usuario).build();
	}
	
	@GET
	@Path("ad")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed(RolesEnum.ADMINISTADOR)
	public Response buscaUsuarioAd(@QueryParam("nome") String nome) {
		List<UsuarioDTO> lista = null;
		try {
			lista = USUARIO_SERVICE.buscaUsuarioAD(nome);
		} catch (LdapException e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
		} catch (UsuarioInvalidoException e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
		} catch (NamingException e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
		} catch (BadArgumentException e) {
			LOGGER.severe(StacktraceToString.convert(e));
			e.printStackTrace();
		}
		return Response.ok(lista).build();
	}
	
}
